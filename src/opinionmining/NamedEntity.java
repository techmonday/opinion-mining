package opinionmining;

import java.util.*;
import java.util.regex.*;

import opinionmining.data.*;

public class NamedEntity {
	Map<String,String> dic;
	int sentiment; //ner: 0, positive: 1, negative: -1
	
	public NamedEntity(Map<String,String> nerDic){
		dic = new  HashMap<String,String>();
		dic = nerDic;
		//sentiment = cType;
	}
	
	/*public ArrayList<Entity> Recognizer(Sentence sent){
		return LRMM(dic,sent);
	}*/
	
	public ArrayList<Entity> SentiWordDetector(Sentence sent){
		ArrayList<Entity> arrEnt = new ArrayList<Entity>();
		String strMatch = "";
		int matchLen = 0, prevLen = 0;
		String strPrev = "";
		ArrayList<Word> arrWord = sent.getWords();
		//ArrayList<Entity> arrEnt = new ArrayList<Entity>();
		int i=0;
		
		while(i < arrWord.size())	{
			if(matchLen > 0) {
				strMatch += " " + arrWord.get(i).getContent();
			}else {
				strMatch += arrWord.get(i).getContent(); 
			}
			matchLen ++;		
			if(dic.containsKey(strMatch) ==true){
				strPrev = strMatch; prevLen = matchLen;
			} else{
				//strMatch of four words doesn't exist in the dic or strMatch covers the end word of the sentence
				if(matchLen > 3 || i == arrWord.size() - 1){ 
					if(strPrev.isEmpty()){ //if strPrev is empty, goes back to the second word in the strMatch
						i = i - matchLen + 1;						
					} else{ //store strPrev as an entity						
						Entity ent = new Entity();
						ent.setContent(strPrev);
						ent.setCategory(dic.get(strPrev));
						ent.setStart(i-matchLen+1); ent.setEnd(ent.getStart() -1 + prevLen);
						//ent.setSentiment2();
						//check the POS of the entity
						int position = ent.getStart();
						do{
							String strPos = arrWord.get(position).getPOS(); 
							if(strPos.equals("N") || strPos.equals("Np") || strPos.equals("Nc") || strPos.equals("Nu") || strPos.equals("V")
									|| strPos.equals("A"))
								position ++;
							else break;
						}while (position < ent.getEnd());
						
						if(position == ent.getEnd() + 1) {
							ent.setSentiment2();
							arrEnt.add(ent);				
						}
						
						i = i - matchLen + prevLen; //goes back to the word next to the detected entity
					}
					strMatch = ""; strPrev = ""; matchLen = 0;
				}
			}
			i++;
		}
		
		if(strPrev.isEmpty()==false){
			Entity ent = new Entity();
			ent.setContent(strPrev);
			ent.setCategory(dic.get(strPrev));
			ent.setStart(i-1-matchLen+1); ent.setEnd(ent.getStart() -1 + prevLen);
			ent.setSentiment2();
			arrEnt.add(ent);
		}
		
		return arrEnt;
	}
	
	/**
	 * Left right maximum matching algorithm to detect named entities based on a dictionary
	 * @param a sentence that has been word segmented
	 * @return an array of entities mentioned in a sentence
	 */
	
	public  ArrayList<Entity> NERecognizer(Sentence sent, ArrayList<Entity> arrEnt){
		//System.out.println("Recognizing product names ...");
		String strMatch = "";
		int matchLen = 0, prevLen = 0;
		String strPrev = "";
		ArrayList<Word> arrWord = sent.getWords();
		//ArrayList<Entity> arrEnt = new ArrayList<Entity>();
		int i=0;
		
		while(i < arrWord.size())	{
			if(matchLen > 0) {
				strMatch += " " + arrWord.get(i).getContent();
			}else {
				strMatch += arrWord.get(i).getContent(); 
			}
			matchLen ++;
			if(dic.containsKey(strMatch) ==true){
				strPrev = strMatch; prevLen = matchLen;
			} else{
				//strMatch of four words doesn't exist in the dic or strMatch covers the end word of the sentence
				if(matchLen > 3 || i == arrWord.size() - 1){ 
					if(strPrev.isEmpty()){ //if strPrev is empty, goes back to the second word in the strMatch
						i = i - matchLen + 1;						
					} else{ //store strPrev as an entity						
						Entity ent = new Entity();
						ent.setContent(strPrev);
						ent.setCategory(dic.get(strPrev));
						ent.setStart(i-matchLen+1); ent.setEnd(ent.getStart() -1 + prevLen);
						//ent.setSentiment2();
						if(arrEnt.contains(ent)==false){
							arrEnt.add(ent);
						}
						else{
							int temp = arrEnt.indexOf(ent);
							arrEnt.get(temp).setCategory(ent.getCategory());							
						}
						
						i = i - matchLen + prevLen; //goes back to the word next to the detected entity
					}
					strMatch = ""; strPrev = ""; matchLen = 0;
				}
			}
			i++;
		}
		
		if(strPrev.isEmpty()==false){
			Entity ent = new Entity();
			ent.setContent(strPrev);
			ent.setCategory(dic.get(strPrev));
			ent.setStart(i-1-matchLen+1); ent.setEnd(ent.getStart() -1 + prevLen);
			if(arrEnt.contains(ent)==false){
				arrEnt.add(ent);
			}
			else{
				int temp = arrEnt.indexOf(ent);
				arrEnt.get(temp).setCategory(ent.getCategory());							
			}			
		}
			
		//sorting the array of entities based on their start position
		Collections.sort(arrEnt, new EntityComparator());
		
		//combining continuous entities into one entity
		ArrayList<Entity> arrRes = new ArrayList<Entity>();
		i = 0;
		while(i< arrEnt.size()){
			Entity newEnt = arrEnt.get(i);			
			int j = i;
			while(j< arrEnt.size()-1){
				Entity ent1 = arrEnt.get(j), ent2 = arrEnt.get(j+1);
				String cat1 = ent1.getCategory(), cat2 = ent2.getCategory();
				if(ent1.getEnd() + 1 == ent2.getStart()) {// if the two entities are continuous
					//newEnt.setStart(arrEnt.get(j).getStart());
					if (cat1.length()==2 && cat2.length()==8)  //only combining product names
						//	|| ((cat1.length()==7|| cat1.length()==3) && cat2.length()==7) //only combining the feature and its value
						//	|| (cat2.length()==7 && cat1.length()==7) )
					{
						newEnt.setEnd(ent2.getEnd());
						newEnt.setCategory(ent2.getCategory()); //the new entity's category is the later entity's
						newEnt.setContent(newEnt.getContent() + " " + ent2.getContent());
						j++;
					} else break;
				} 
				else break;
			}
			i = j + 1;
			arrRes.add(newEnt);
		}
		
		return arrRes;
	}
	
	/**
	 * Detecting entities by using regular expressions
	 * @param a sentence
	 * @return an array of entities
	 */
	public ArrayList<Entity> regExNER(Sentence sent){
		ArrayList<Entity> arrEnt = new ArrayList<Entity>();
		Pattern p1 = Pattern.compile("[0-9]+[.,0-9]*(megapixel|mp|ppi|pixel|inch|gb|mah|ghz|mb|g|mm|cm)");
		Pattern p2 = Pattern.compile("[0-9]+[.,0-9]*");
		Pattern p3 = Pattern.compile("megapixel|mp|ppi|pixel|inch|gb|mah|ghz|mb|mm|cm|giờ");
				
		ArrayList<Word> arrWord = sent.getWords();
		for(int i=0; i< arrWord.size(); i++){
			String word = arrWord.get(i).getContent();
			if(p1.matcher(word).matches()){
				Entity ent = new Entity(arrWord.get(i).getContent(), i, i, "REGEXEN");
				arrEnt.add(ent);
			} else if(p2.matcher(word).matches() && i < arrWord.size() - 1 && p3.matcher(arrWord.get(i+1).getContent()).matches()){
				Entity ent = new Entity (word + " " + arrWord.get(i+1).getContent(), i, i+1, "REGEXEN");
				arrEnt.add(ent);
			}
		}
		
		return arrEnt;
	}
}
