package opinionmining;
import opinionmining.data.*;
import opinionmining.data.Dictionary;

import java.io.FileNotFoundException;
import java.util.*;
import java.util.Map.Entry;


/**
 * This class is used for extracting sentiments of a documents
 * @author nthnhung
 *
 */

public class SentimentExtractor {
	BasicNLP bnlp;
	Dictionary entityDic;
	Dictionary sentiDic;
	//Dictionary positiveDic;
	//Dictionary negativeDic;
	public SentimentExtractor(Dictionary entDic, Dictionary sentimentDic, BasicNLP basic){
		entityDic = new Dictionary();
		entityDic = entDic;
		/*positiveDic = new Dictionary();
		positiveDic = posDic;
		negativeDic = new Dictionary();
		negativeDic = negDic;*/
		sentiDic = new Dictionary();
		sentiDic = sentimentDic;
		bnlp = new BasicNLP();
		bnlp = basic;
	}
	public ArrayList<String> extractKeyword(ArrayList<Entity> allFea){
		
		//extracting keyword
		ArrayList<String> keywords = new ArrayList<String>();
		Map<String,Integer> dicFea = new HashMap<String,Integer>();
		for(Entity ent:allFea){
			if(ent.getCategory().length()!=8)						
				continue;
			String strEnt = ent.getContent();
			if(dicFea.containsKey(strEnt)){
				int value = dicFea.get(strEnt) + 1;
				dicFea.put(strEnt, value);
				}
			else {						
				dicFea.put(strEnt, 1);
				
			}
		}
		//sorting the map by value 
		//- See more at: http://java2novice.com/java-interview-programs/sort-a-map-by-value/
		Set<Entry<String, Integer>> set = dicFea.entrySet();
		List<Entry<String, Integer>> list = new ArrayList<Entry<String, Integer>>(set);
		Collections.sort( list, new Comparator<Map.Entry<String, Integer>>(){
			public int compare( Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2 )   {
				return (o2.getValue()).compareTo( o1.getValue() );
				}
			} );
        int count = 0;
        for(Map.Entry<String, Integer> entry:list){
        	if( count ++ > 2)
        		break;
        	if(entry.getValue() > 1) {
        		System.out.println(entry.getKey()+" ==== "+entry.getValue());
        		keywords.add(entry.getKey());
        	}
        }
        return keywords;
}
	
	public ArrayList<Sentence> extractor(String fileIn) throws FileNotFoundException, Exception{		
		
		//System.out.println("Sentiment Analysis ...") ;
		
		//sentence segmentation, word segmentation, POS
		//BasicNLP bnlp = new BasicNLP();
		ArrayList<Sentence> arrSent = bnlp.NLP(fileIn);
		
		if(arrSent == null){
			System.out.println("No sentence to process!");
			return null;
		}
		
		//bnlp.printSent(); //for testing the wrap function
		
		NamedEntity ner = new NamedEntity(entityDic.getDic()); //named entity recognition
		NamedEntity sentiWord  = new NamedEntity(sentiDic.getDic()); //positive sentiment word detection
		//NamedEntity negSenti = new NamedEntity(negativeDic.getDic(),-1); //negative sentiment word detection
		//SentimentWord sw = new SentimentWord(); //sentiment word detection
		SentimentDetector sd = new SentimentDetector(); // sentiment extraction
		
		ArrayList<Entity> allFea = new ArrayList<Entity>();
		ArrayList<Entity> allSenti = new ArrayList<Entity>();
		
		for(int i=0; i< arrSent.size(); i++){
			Sentence sent = arrSent.get(i);
			sent.detectNeg();
			
			//detecting entities by using regular expressions
			ArrayList<Entity> arrEnt2 = ner.regExNER(sent);			
			
			//detecting entities by using the dictionary
			ArrayList<Entity> arrEnt = ner.NERecognizer(sent, arrEnt2);
			//arrEnt.addAll(arrEnt2);
			sent.setEntities(arrEnt);			
			
			//locating all sentiment words
			ArrayList<Entity> arrSenti = sentiWord.SentiWordDetector(sent);
			sent.setSentiments(arrSenti);
			allSenti.addAll(arrSenti);
			
			//assigning sentiment for each feature
			ArrayList<Entity> arrFea = sd.featureAnalyze(sent);
			sent.setEntities(arrFea);
			
			//assigning sentiment for the whole sentence
			sent.setSenti(sd.assignSentiment(sent));
			
			allFea.addAll(arrFea);
			
			sent = sd.comparativeSent(sent);
			
			arrSent.set(i, sent);			
		}

		//extracting keywords
		ArrayList<String> keywords = extractKeyword(allFea);
        
		//extracting the sentiment for the whole document
        

		
		return arrSent;
	}
}
