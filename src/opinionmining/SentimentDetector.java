package opinionmining;

import java.util.ArrayList;
import java.util.Collections;

import opinionmining.data.EntityComparator;
import opinionmining.data.Sentence;
import opinionmining.data.Entity;
import opinionmining.data.Word;
/**
 * This class is used to extract sentiment from a sentence by using a set of rules
 * @author nthnhung
 * - Partly addressing comparative sentences
 * 
 * - Conditional sentences: ignore!
 * 
 * - Pay attention to some reverse adverbs: hết, chưa, hiếm khi, chỉ có, hơi, khá, không, ... 
 * 
 * Part-whole relation of features: "Màn hình Nokia" , "iPhone có những tiện ích ..." --> not addressed yet!
 */

public class SentimentDetector {
	public ArrayList<Entity> Detector(Sentence sent){
		return null;
	}
	
	public static final int MAX_DIS = 4; //the maximum distance between an entity and its sentiment word 
	public int fSenti = 0;
	
	public int checkStringBetween(ArrayList<Word> arrWord, int start, int end){
		/** Currently, the distance between the feature and the sentiment word is limited to 4 words. 
		 * However, there are cases that the distance is longer. For example:
		 * SS Galaxy N4 là/C một/M chiếc/Nc điện_thoại/N rất/R tuyệt/A
		 * Galaxy Note Edge là/C một/M trong/E những/L smartphone/N độc_đáo/A 
		 * We should deal with such cases. 
		 * 
		 */
		if(end-start < MAX_DIS)
			return 1;		
		
		for(int z = start; z <end ; z++){
			Word w = arrWord.get(z);
			String strPos = w.getPOS();
			String wordContent = w.getContent();
			if(strPos.equals(",") ) {
				return -1; // if there is punctuation, return -1
			}
			
			
			//check if there is any reverse words between the entity and the sentiment word
			if(wordContent.contains("không") || wordContent.contains("ko") || wordContent.contains("kg") || wordContent.contains("chẳng")
					|| wordContent.contains("khá") || wordContent.contains("hết")|| wordContent.contains("chưa") 
					|| wordContent.contains("thua") || wordContent.contains("chỉ") ){ //wordContent.contains("hơi")||
				fSenti++;
			}
			//check if there is a conjunction or punctuation between them
			//String wordPos = arrWord.get(z).getPOS();
			//if(wordPos.equals("C") ||wordPos.equals(":") || wordPos.equals(",") || wordPos.equals("...")){
			//	minIndx = -1;
			//	fSenti = 0;
			//	break;
			//}			
		}		
		
		return 1;
	}
	
	public int assignSentiment(Sentence sent){
		//-2 unspecified, -1 negative, 0 neutral, 1 positive
		ArrayList<Entity> arrSenti = sent.getSentiments();
		if(arrSenti.size()==0)
			return -2; //unspecified
		
		int positive = 0, negative = 0, neutral = 0;		
		for(int j=0; j < arrSenti.size(); j++)
		{
			switch(arrSenti.get(j).getSentiment()){
			case 1: positive ++; break;
			case -1: negative ++; break;
			case 2: neutral++; break;
			}
			
		}
		if(positive > negative) return 1; //positive
		if(positive < negative) return -1; //negative
		
		return 0; //neutral
	}
	
	public Sentence comparativeSent(Sentence sent){
		/** Patterns: [ ] means optional
		 * 
		 * [Entity 1] + [thì] + [KHÔNG, KHÔNG THỂ] + sentiment word/adj/verb phrase? + {hơn, hơn hẳn, hơn so với, hơn hẳn so với} + Entity 2
		 * M8 chắc chắn hơn so với M7
		 * iPhone 6 đáng chọn hơn Galaxy 6
		 * trải nghiệm M8 thì sung sướng hơn M7 50%
		 * 
		 * [Entity 1] + [KHÔNG, KHÔNG THỂ, CHƯA, CHƯA THỂ] + sentiment word/adj/phrase + như + Entity 2
		 * Tốc độ lấy nét của M8 chưa nhanh như những gì mình được trải nghiệm trên Galaxy S5.
		 * m8 không bị lag như s5
		 * 
		 * So E1 với E2 thì E1 + sentiment word + hơn --> not address
		 * 
		 * So sánh nhất: tốt nhất, tuyệt nhất, ... --> not necessary since the sentiment can be detected by the normal process 
		 *    
		 * So sánh tương đương: 
		 *  Feature 1 của E1 và E2 tương đương ...
		 *  E1 tương đương với E2 ...
		 *  E1 có F1 tương đương E2 ...
		 *  [Entity 1] + * + [KHÔNG] + {giống, giống như, y chang, giống y chang} + [Entity 2]
		 *  Nắp lưng giống BB, cạnh giống iP5 , mặt trước sau vẫn SS
		 * 

		 *  
		 * Noted: limit the distance between components in patterns
		 */
		
		ArrayList<Entity> arrEnt = sent.getEntities();
		ArrayList<Entity> arrSenti = sent.getSentiments();
		ArrayList<Word> arrWord = sent.getWords();
		
		//String[] negWord = {"không", "chưa"};
		//String[] comparativeWord = {"hơn", "như", "giống"};
				
		if(arrEnt.size()==0)
			return sent;
		
		/*if(arrSenti.size()==0)
			return sent;
		*/
		
		if(arrWord.size()< 2)
			return sent;
		
		int startPos = -1, endPos = -1;
		for(int i=-1; i < arrEnt.size(); i++){
			if(i == -1){
				startPos = 0; //in case the entity one is not present.
			}else{
				if(arrEnt.get(i).getCategory().length()!=8) 
					continue; //focus on comparison between product names
				startPos = arrEnt.get(i).getEnd() + 1;
			}
			//pairing two entities
			for(int j=i+1; j< arrEnt.size(); j++){ 
				endPos = arrEnt.get(j).getStart();
				int pNeg = -1, pSenti = -1, pComWord = -1;
				if(arrEnt.get(j).getCategory().length() !=8 || endPos == -1 || startPos > endPos || endPos - startPos > 15) //only consider 
					//the product names
					continue;
				if(i != -1 && arrEnt.get(i).getContent().equals(arrEnt.get(j).getContent())) 
					continue; // avoid pairing the two identical product names

				//checking words between E1 and E2				
				for(int pos = startPos; pos < endPos; pos++){
					String wstr = arrWord.get(pos).getContent();
					String posstr = arrWord.get(pos).getPOS();
					if(wstr.contains("không") || wstr.contains("chưa") ) { //negation word
						pNeg = pos;
					}
					else if(wstr.contains("hơn") || wstr.contains("như") || wstr.contains("giống")
							||(pos < arrWord.size()-1 && wstr.contains("y") && arrWord.get(pos+1).getContent().equals("chang"))){
						pComWord = pos; //comparative word
					} else if(i!=-1 && (wstr.contains("tương_đương") || wstr.contains("tương_tự"))){//comparative sentence but fairly
						if(pNeg!=-1) {
							sent.setType(-2); //negation comparison
						} else{							
							sent.setType(2); //neutral comparison
						}
						
						System.out.print("Neutral compare: ");
						if(i!=-1)
							System.out.println(arrEnt.get(i).getContent() + " vs. " + arrEnt.get(j).getContent());
						else
							System.out.println(arrEnt.get(j).getContent());
						break;
						
					} else if(posstr.equals("A")){ //adjective
						pSenti = pos;
					} 
				}				
				if(pComWord != -1 && pSenti != -1){
					int t = 0;
					//check if there is a product between the two compared entities
					for(t=i+1; t < j; t++){
						if(arrEnt.get(t).getCategory().length()==8) // there is a product, break
							break;
							
					}
					if(t==j){
						if(pNeg!=-1) {
							sent.setType(-1); //negation comparison
						} else{							
							sent.setType(1); //comparative sentence
						}
						
						System.out.print("Comparing:");
						if(i!=-1)
							System.out.println(arrEnt.get(i).getContent() + " vs. " + arrEnt.get(j).getContent());
						else
							System.out.println(arrEnt.get(j).getContent());
					}
				}
			}
			
			
		}
		
		return sent;
	}
	
	public ArrayList<Entity> featureAnalyze(Sentence sent){
		/**Some patterns:
			 * - Negative: 
			 * + negation words not preceded by a negation within 3 words in the same sentence
			 * + positive words preceded by a negation within 3 words in the same sentence
			 *- Positive:
			 * + negative words after a negation within 3 words in the same sentence
			 * + positive words not after a negation within 3 words in the same sentence
		*/
		
		ArrayList<Entity> arrEnt = sent.getEntities();
		ArrayList<Entity> arrSenti = sent.getSentiments();
		ArrayList<Word> arrWord = sent.getWords();		
		
		//sorting the array of sentiment words based on their start position
		//Collections.sort(arrSenti, new EntityComparator());	
	
		
		for(int i =0; i< arrEnt.size(); i++){
			Entity curEnt = arrEnt.get(i);
			fSenti = 0;
			int minDis = Integer.MAX_VALUE , minIndx = -1;
			for(int j=0; j < arrSenti.size(); j++)
			{				
				Entity senti = arrSenti.get(j);				
				//distance between a named entity and a sentiment word is less than 4
				//finding the closet sentiment word of an entity
				//ex: việc sử_dụng [m8] rất [nhanh_chóng], [thiết_kế] [thuôn gọn] , rất [vừa_tay], [q10] thật_sự rất [mạnh_mẽ] 
				//[q10] KHÔNG thật_sự [thoải_mái]
				if(senti.getStart() - curEnt.getEnd() > 0 && senti.getStart() - curEnt.getEnd() < minDis){					
						
				}
			}
			
			//if there are reversed words between an entity and its sentiment word, reverse the sentiment of the entity
			if(minIndx!=-1){
				String sentiID = arrSenti.get(minIndx).getCategory();
				
				String strID = arrEnt.get(i).getCategory();
				if(strID.length()>3)
					strID = strID.substring(0, 3);
				
				if(sentiID.contains(strID)){ //matching the correct sentiment ID with the appropriate category of the entity
					int pos1 = sentiID.indexOf(strID);
					int pos2 = sentiID.indexOf(",", pos1+1);
					if(pos2==-1) pos2 = sentiID.length();
					sentiID = sentiID.substring(pos1, pos2);
					if(sentiID.charAt(sentiID.length()-1)== 'N') {
						arrEnt.get(i).setSentiment(-1); //negative sentiment
						//if the current sentiment is neutral --> change it
						if(arrSenti.get(minIndx).getSentiment() == 2) 
							arrSenti.get(minIndx).setSentiment(-1);
					}
					else{
						arrEnt.get(i).setSentiment(1); //positive
						//if the current sentiment is neutral --> change it
						if(arrSenti.get(minIndx).getSentiment() == 2) 
							arrSenti.get(minIndx).setSentiment(1);
					}
				} else {
					int orgSenti = arrSenti.get(minIndx).getSentiment();
					if(orgSenti == 2 && fSenti > 0){
						if(fSenti%2 ==0) // double negation
							arrEnt.get(i).setSentiment(1); //positive
						else arrEnt.get(i).setSentiment(-1); //negative
					}
					else if(orgSenti == 1){
						if(fSenti % 2 ==0) 	arrEnt.get(i).setSentiment(1);
						else if(fSenti % 2 == 1) arrEnt.get(i).setSentiment(-1);
					} 
					else if(orgSenti == -1){
						if(fSenti % 2 ==0) 	arrEnt.get(i).setSentiment(-1);
						else if(fSenti % 2 == 1) arrEnt.get(i).setSentiment(1);
					}
					else arrEnt.get(i).setSentiment(0); //neutral
				}
				
				arrEnt.get(i).setStrSenti(arrSenti.get(minIndx).getContent());
			}
			
		}
		
		//grouping synonyms of features/products
		
		
		return arrEnt;
	
	
	}
}
