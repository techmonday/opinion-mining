package opinionmining;

import opinionmining.data.*;
import opinionmining.data.Dictionary;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.*;

import opinionmining.*;

public class Document {
	String filePath;
	ArrayList<Sentence> arrSent;
	
	public Document(String path){
		filePath = path;
	}
	public Document(){
	
	}
	public void preprocessInput(String fileOut) {
		System.out.println("Preprocessing the input ...");
		return;
	}
	
	public void sentimentExtractor(Dictionary entDic, Dictionary sentiDic,BasicNLP bnlp) throws FileNotFoundException, Exception{		
		SentimentExtractor ext=new SentimentExtractor(entDic, sentiDic,bnlp);		
		arrSent = ext.extractor(filePath);
		return;
	}
	
	public void csvWriter(String fileOut) throws IOException{
		System.out.println("Database writing ...");
		fileOut = fileOut + ".csv";
		OutputStream os = new FileOutputStream(fileOut);		
		Writer bw = new OutputStreamWriter(os, "UTF8");
		bw.write("\uFEFF"); //add BOW of UTF-8		
		
		bw.write("ID,Content 1,Content 2,Content 3,TRUE/FALSE,Notes\n");
		for(int i=0; i< arrSent.size(); i++){			
			Sentence sent = arrSent.get(i);
			ArrayList <Word> arrWord = sent.getWords();
			if(arrWord.size() < 3)
				continue;
			String strSent = ""; 
			strSent += Integer.toString(i) + ",\"**"; //to escape commas in csv, we have to enclose the whole string within double quotes
			for(int j=0; j < arrWord.size(); j++){
				//bw.write(arrWord.get(j).getContent()+ " ");
				if(arrWord.get(j).getContent().equals("\"")){ //escape character in csv
					strSent += "\"";
				}
				strSent += arrWord.get(j).getContent()+ " ";
			}
			strSent += "\",,";
			bw.write(strSent + "\n");
//			bw.write(",,\n");
			bw.write(Integer.toString(i) + ",Overall sentiment,");
			int sentiment = sent.getSenti();
			switch(sentiment){
			case 1: bw.write("Positive,\n"); break;
			case -1: bw.write("Negative,\n"); break;
			case 0: bw.write("Neutral,\n"); break;
			case -2: bw.write("Unspecified,\n"); break;
			}
			
			ArrayList<Entity> arrEnt = sent.getEntities();
			if(arrEnt.size() > 0) {				
				for(int j=0; j < arrEnt.size(); j++ ){					
					Entity curEnt = arrEnt.get(j);
					if(curEnt.getStrSenti()==null)
						continue;
					bw.write(Integer.toString(i) + ",");
					bw.write(curEnt.getContent() + ",");
					if(curEnt.getSentiment() ==1){
						bw.write(curEnt.getStrSenti() + ",Positive\n");
					} else if (curEnt.getSentiment() ==-1){
						bw.write(curEnt.getStrSenti() + ",Negative\n");
					} else
						bw.write(curEnt.getStrSenti() + ",Neutral\n");				
				}
			}
			bw.write("\n");
		}
		bw.close();
	}
	public void databaseWriter(String fileOut) throws IOException{
		System.out.println("Database writing ...");		
		OutputStream os = new FileOutputStream(fileOut);
		Writer bw = new OutputStreamWriter(os, "UTF8");
		
		for(int i=0; i< arrSent.size(); i++){
			Sentence sent = arrSent.get(i);
			ArrayList <Word> arrWord = sent.getWords();
			bw.write("**");
			for(int j=0; j < arrWord.size(); j++){
				bw.write(arrWord.get(j).getContent()+ " ");
			}
			bw.write("\n");
			
			ArrayList<Entity> arrEnt = sent.getEntities();
			if(arrEnt.size() > 0) {
				bw.write("+++ Named Entities +++\n");
				for(int j=0; j < arrEnt.size(); j++ ){
					Entity curEnt = arrEnt.get(j);
					bw.write(curEnt.getContent() + "\t" + curEnt.getCategory());
					if(curEnt.getSentiment() ==1){
						bw.write("\tPositive: " + curEnt.getStrSenti());
					} else if (curEnt.getSentiment() ==-1){
						bw.write("\tNegative: " + curEnt.getStrSenti());
					} 
					
					bw.write("\n");
				}
			}
			
			ArrayList<Entity> arrSenti = sent.getSentiments();
			if(arrSenti.size() > 0) {
				bw.write("+++ Sentiment Words +++\n");				
				for(int j=0; j < arrSenti.size(); j++ ){
					bw.write(arrSenti.get(j).getContent() + "\t" + arrSenti.get(j).getCategory()) ;
					bw.write("\n");
				}
				
			}
			
			bw.write("++Overall sentiment: ");
			int sentiment = sent.getSenti();
			switch(sentiment){
			case 1: bw.write("Positive\n"); break;
			case -1: bw.write("Negative\n"); break;
			case 0: bw.write("Neutral\n"); break;
			case -2: bw.write("Unspecified\n"); break;
			}
			
			bw.write("\n");
			
			
		}
		
						
		
		bw.close();
		return;
	}
	public void extractForFolder(File folder, Dictionary entDic, Dictionary sentiDic, BasicNLP bnlp) {
	    for (final File fileEntry : folder.listFiles()) {
	        if (fileEntry.isDirectory()) {
	            extractForFolder(fileEntry, entDic, sentiDic,bnlp);
	        } else {
	        	System.out.println(fileEntry.getName());
	        	String fileName = fileEntry.getAbsolutePath();
	        	Document doc = new Document(fileName);
	        	try {
			doc.sentimentExtractor(entDic, sentiDic,bnlp);
			fileName = "data\\out\\";
		        	fileName = fileName + fileEntry.getName();
		    	doc.databaseWriter(fileName);
		        	//doc.csvWriter(fileName);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					
				}
	        	
	            //System.out.println(fileEntry.getName());
	        }
	    }
	}

	public static void main(String [] args) throws FileNotFoundException, Exception{
		/*if(args.length < 2 )
		{
			System.out.println("Command line: SentimentAnalysis [inputFile] [outputFile]");
			return;
		}*/
		
		//String fileTemp = "temp.txt";
		
		//doc.PreprocessInput(fileTemp);
		
		final long startTime = System.nanoTime();
		
		Dictionary nerDic = new Dictionary();
		nerDic.loadDictionary("data\\dictionary\\dic_entity_ws.txt");
		
		/*Dictionary positiveDic = new Dictionary();
		positiveDic.loadDictionary("data\\dictionary\\PositiveDic.txt");
		
		Dictionary negativeDic = new Dictionary();
		negativeDic.loadDictionary("data\\dictionary\\NegativeDic.txt");*/
		
		Dictionary sentiDic = new Dictionary();
		sentiDic.loadDictionary("data\\dictionary\\dic_sentiment_word.txt");
		
		BasicNLP bnlp = new BasicNLP();
		bnlp.initialize();
		
		//final File folder = new File(args[0]);
                                           final File folder = new File("data\\txt");
		Document doc = new Document();		
		doc.extractForFolder(folder, nerDic, sentiDic,bnlp);
		
		final long duration = System.nanoTime() - startTime;
		
		System.out.print("Running time: " + duration/1000000000 + " second(s).");
		//System.out.println();
		
		//doc.SentimentExtractor(args[0], nerDic, positiveDic, negativeDic);
		
		//doc.DatabaseWriter(args[1]);
	}
}
