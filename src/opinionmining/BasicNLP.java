package opinionmining;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.*;

import jvntextpro.*;
import jvntextpro.conversion.CompositeUnicode2Unicode;
import opinionmining.data.*;

public class BasicNLP {
	
	/**
	 *  an array of sentences */	
	/** POS tag by JVnTextPro
	 * 1. N: Noun (danh từ) 
	 * 2. Np: Personal Noun (danh từ riêng)
	 * 3. Nc: Classification Noun (danh từ chỉ loại)
	 * 4. Nu: Unit Noun (danh từ đơn vị)
	 * 5. V: verb (động từ)
	 * 6. A: Adjective (tính từ)
	 * 7. P: Pronoun (đại từ)
	 * 8: L: attribute (định từ)
	 * 9. M: Numeral (số từ) 
	 * 10. R: Adjunct (phụ từ) 
	 * 11. E: Preposition (giới từ)	
	 * 12. C: conjunction (liên từ)
	 * 13. I: Interjection (thán từ)
	 * 14. T: Particle, modal particle (trợ từ, tiểu từ)
	 * 15. B: Words from foreign countries (Từ mượn tiếng nước ngoài ví dụ Internet, ...)
	 * 16. Y: abbreviation (từ viết tắt)
	 * 17. X: un-known (các từ không phân loại được)
	 * 18. Mrk: punctuations (các dấu câu)
	 */
	//public ArrayList<Sentence> arrSent;
	public JVnTextPro textPro=null;
	public BasicNLP(){
		//arrSent = new ArrayList<Sentence>();		
	}
	public void initialize(){
		textPro = new JVnTextPro();		
		textPro.initSenSegmenter("models\\jvnsensegmenter"); 
		textPro.initSenTokenization(); 
		textPro.initSegmenter("models\\jvnsegmenter"); 
		textPro.initPosTagger("models\\jvnpostag\\maxent");
	}
	
	public void WrapJVnTextPro(String fileIn, String fileOut) throws Exception, FileNotFoundException{
		/**
		 * Process the text and return the processed text with word segmentation and POS
		 * pipeline : wrapping JVnTextPro
		 *
		 * @param fileIn: text file to process, fileOut: resulted file
		 * @return null
		 */	
		if(textPro==null){
			System.err.println("JVnTextPro hasn't initialized yet!");
			return;
		}
		
		CompositeUnicode2Unicode conversion = new CompositeUnicode2Unicode();
		
		
		//read file, process and write file
		BufferedReader reader = new BufferedReader(new InputStreamReader(	new FileInputStream( fileIn), "UTF-8"));
		String line, ret = "";
		
		while ((line = reader.readLine()) != null){
			line = conversion.convert(line);				
			ret += textPro.process(line).trim() + "\n";
		
		}
		
		reader.close();
		//fileOut = fileIn + ".pos.txt";
		//write file
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream(fileOut), "UTF-8"));
		writer.write(ret);
		writer.close();	
		
		return;
	}
	/**
	 * Parsing the output of JVnTextPro and storing all results in an array of sentences
	 * @param fileIn
	 * @throws IOException 
	 */
	
	public ArrayList<Sentence> ParseOutput(String fileIn) throws IOException{
		ArrayList<Sentence> arrSent = new ArrayList<Sentence>();
		BufferedReader reader = new BufferedReader(new InputStreamReader(	new FileInputStream( fileIn), "UTF-8"));
		String line;
		
		CompositeUnicode2Unicode conversion = new CompositeUnicode2Unicode();
		
		while ((line = reader.readLine()) != null){
			String txtSent = "";
			line = conversion.convert(line);
			if(line.length() < 2) 	continue;
			ArrayList<Word> arrWord = new ArrayList<Word>();
			for(String word: line.split(" ")){
				Word tword = new Word();
				//String[] term = word.split("/");
				int pos = word.lastIndexOf("/");
				if(pos == -1){
					System.err.print("The output word is not formatted correctly:");
					System.err.println(word);
					break;
				}
				if(word.substring(0,pos).isEmpty() || word.substring(0,pos).equals("\uFEFF")){
					continue;
				}
				tword.setContent(word.substring(0,pos).toLowerCase(new Locale("vi")));
				tword.setPOS(word.substring(pos+1, word.length()));
				arrWord.add(tword);
				txtSent = tword.getContent() + " ";
			}
			Sentence sent = new Sentence(txtSent);
			sent.setWords(arrWord);
			arrSent.add(sent);			
		
		}
		
		reader.close();
		
		return arrSent;
	}
	/**
	 * natural processing with word segmentation and POS by using JVnTextPro, and then parsing the output for the next steps
	 * @param fileIn: plain text without HTML/XML/JSON tags
	 * @throws FileNotFoundException
	 * @throws Exception
	 */
	public ArrayList<Sentence> NLP(String fileIn) throws FileNotFoundException, Exception{
		System.out.println("Segmenting the document by JVnTextPro ...");
		String fileOut = "temp.txt";
		WrapJVnTextPro(fileIn, fileOut);
		
		//parse the output and save in an array of sentences
		return ParseOutput(fileOut);
	}
	
	/*public ArrayList<Sentence> getSent(){
		return arrSent;
	}
	
	
	public void printSent(){
		for(int i=0; i< arrSent.size(); i++){
			arrSent.get(i).printSentence();
			System.out.println();
		}
	}*/
	
}
