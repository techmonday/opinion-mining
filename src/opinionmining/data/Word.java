package opinionmining.data;

public class Word {
	/**
	 * @author nthnhung
	 * This class represents a word in a sentence. A word contains: 
	 * 		- textual content, 
	 * 		- part-of-speech, 
	 * 		- start and end position (word index), and
	 * 		- sentiment value
	 */
	
	String content;
	String POS;
	int start, end;
	int sentimentValue; //1: positive, 0: neutral, -1: negative, 2: ambiguous
	
	public Word(){
		
	}
	
	public Word(int s, int e, String word){
		content = word.toLowerCase();
		start = s; 
		end = e;
	}
		
	public void setPOS(String pos){
		POS = pos;
	}
	
	public String getPOS(){
		return POS;
	}
	
	public void setSentiVal(int val){
		sentimentValue = val;
	}
	
	public int getSentiVal(){
		return sentimentValue;
	}
	
	public String getContent(){
		return content;
	}
	
	public void setContent(String word){
		content = word;
	}
	public void printWord() {
		System.out.print(content);
	}
}
