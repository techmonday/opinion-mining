package opinionmining.data;

public class Entity{
	String content;
	int start, end;
	String category; //Entity: category = category ID
	String strSenti; // the string that conveys the sentiment
	int senti; //Sentiment: positive, negative, neutral
	public Entity(){
		
	}
	
	@Override
	public boolean equals(Object obj){
		if(obj==null) 
			return false;
		if(getClass()!=obj.getClass())
			return false;
		
		final Entity other = (Entity) obj;
		if(this.start == other.start && this.end == other.end)
			return true;
		
		return false;
	}
	
	@Override
	public int hashCode(){
		int hash = 3;
		hash = 53 * hash + this.start + this.end;
		
		return hash;
	}
	
	public Entity(String c, int s, int e, String cat){
		content = c;
		start = s; end = e; category = cat;
	}
	
	public String getContent(){
		return content;
	}
	
	public void setContent(String entity){
		content = entity;
	}
	
	public void setCategory(String cat){
		category = cat;
	}
	
	public String getCategory(){
		return category;
	}
	
	public int getStart(){
		return start;
	}
	
	public void setStart(int s){
		start = s;
	}
	
	public int getEnd(){
		return end;
	}
	
	public void setEnd(int e){
		end = e;
	}
	
	public void setSentiment(int s){
		senti = s;
	}
	
	public void setSentiment2(){
		/*if(category.contains(",")) {
			int flag = 0;
			String[] arrId = category.split(",");
			for (int i=0; i< arrId.length; i++){
				if(arrId[i].charAt(arrId[i].length()-1)=='N'){					
					if(flag==1)
					{
						senti = 2; return;  //ambiguous sentiment
					}
					if(flag==0) flag = -1;
				} else if(arrId[i].charAt(arrId[i].length()-1)=='P'){	
					if(flag == -1){
						senti = 2; return;
					}
					if(flag == 0) flag = 1;
				}
				
			}
			senti = flag;
			return; 
		}*/
		
		if(category.contains(",")) {
			int positive = 0, negative = 0;
			String[] arrId = category.split(",");
			for (int i=0; i< arrId.length; i++){
				if(arrId[i].charAt(arrId[i].length()-1)=='N'){					
					negative ++;
				} else if(arrId[i].charAt(arrId[i].length()-1)=='P'){	
					positive ++;
				}
				
			}
			if(positive > negative) senti = 1;
			else if(positive < negative) senti = -1;
			else senti = 0;			
			return; 
		}
		
		if(category.charAt(category.length() -1) == 'N'){
			senti = -1;
		} else if(category.charAt(category.length() -1) == 'P'){
			senti = 1;
		} else senti = 0; //named entity
	}
	
	public int getSentiment(){
		return senti;
	}
	
	public void setStrSenti(String txtSenti){
		strSenti = txtSenti;
	}
	
	public String getStrSenti(){
		return strSenti;
	}

	public void printEntity(){
		System.out.print(content + "\t" + category);
		
	}
}

