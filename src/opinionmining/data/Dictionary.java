package opinionmining.data;
import java.util.HashMap;
import java.util.Map;
import java.util.Locale;
import java.io.*;

public class Dictionary {
	public Map<String, String> dic;
	public Dictionary(){
		dic = new HashMap<String,String>();
	}
	
	public Map<String,String> getDic(){
		return dic;
	}
	
	public void setDic(Map<String,String> input){
		dic = new HashMap<String,String>();
		dic = input;
	}
	
	public void loadDictionary(String fileName) throws IOException{
		BufferedReader br = new BufferedReader(
				   new InputStreamReader(
		                      new FileInputStream(fileName), "UTF8"));
		String line;
		while((line=br.readLine()) != null){
			if(line.isEmpty())
				continue;
			if(line.charAt(0)=='#')
				continue;
			if(line.indexOf('\t') == -1)
				continue;			
			
			String strKey = line.substring(0, line.indexOf('\t')).toLowerCase(new Locale("vi")); //vi or vi_VN?;
			String strValue = line.substring(line.indexOf('\t')+1, line.length());
			if(dic.containsKey(strKey)){
				strValue = dic.get(strKey) + "," + strValue;
				dic.put(strKey, strValue);
			} 
			dic.put(strKey, strValue);
		}
		br.close();
		
		/*OutputStream os = new FileOutputStream("test.txt");
		Writer bw = new OutputStreamWriter(os, "UTF8");
		
		for(Map.Entry<String, String> entry : dic.entrySet()){
			bw.write(entry.getKey());
			bw.write("\t");
			bw.write(entry.getValue());
			bw.write("\n");
		}
		
		bw.close();*/
		
	}
}
