package opinionmining.data;

import java.util.ArrayList;

public class Sentence {
	String sent;
	int type; //0: normal 1: comparative sentence, 2: fairly comparative sentence, 3: conditional sentence
	ArrayList<Word> arrWord;
	ArrayList<Entity> arrEnt;
	ArrayList<Entity> arrSentiment;
	int genSenti; //-2 unspecified, -1 negative, 0 neutral, 1 positive
	
	public Sentence(String strSent){
		sent = strSent;
		arrWord = new ArrayList<Word>();
		arrEnt = new ArrayList<Entity>();
		arrSentiment = new ArrayList<Entity>();		
		genSenti = -2;
	}
	
	public void detectNeg(){
		//this function is to detect negation words that are not included in the pre-defined dictionaries
		//không, thiếu, tuy_nhiên, nhưng, mặc_dù, không_phải, không_thể, không_hề, không_cần, không_có, không_đáng, 
		//không_biết, không_còn, không_ngại, không_được
		for(int i=0; i< arrWord.size(); i++){
			if(arrWord.get(i).getContent().contains("không") || arrWord.get(i).getContent().equals("nhưng") 
					|| arrWord.get(i).getContent().equals("thiếu") || arrWord.get(i).getContent().equals("tuy_nhiên") 
					|| arrWord.get(i).getContent().equals("mặc_dù") )
				arrWord.get(i).setSentiVal(-1); //reverse words
		}
				
	}
	
	public void printSentence(){
		for(int i=0; i< arrWord.size(); i++){
			arrWord.get(i).printWord();
			System.out.print(" ");
		}
	}
	
	public void printEntities(){
		for(int i=0; i< arrEnt.size(); i++){
			arrEnt.get(i).printEntity();
			System.out.println();
		}
	}
	
	public ArrayList<Word> getWords(){
		return arrWord;
	}
	
	public void setWords(ArrayList<Word> words){
		arrWord = words;
	}
	
	public void setEntities(ArrayList<Entity> entities){
		arrEnt = entities;
	}
	
	public ArrayList<Entity> getEntities(){
		return arrEnt;
	}
	
	public void setSentiments(ArrayList<Entity> sentiments){
		arrSentiment = sentiments;
	}
	
	public ArrayList<Entity> getSentiments (){
		return arrSentiment;
	}
	
	public String getSent(){
		return sent;
	}
	
	public void setSent(String strSent){
		sent = strSent;
	}
	
	/*public boolean checkConditionalSent(){
		
		return false;
	}*/
	
	public void setSenti(int value){
		genSenti = value;
	}
	
	public int getSenti(){
		return genSenti;
	}

	public void setType(int t){
		type = t;
	}
	
	public int getType(){
		return type;
	}
}


