package opinionmining.data;

import java.util.Comparator;

public class EntityComparator implements Comparator<Entity>{	
	public int compare(Entity e1, Entity e2){
		if(e1.getStart()==e2.getStart())
			return 0;
		if(e1.getStart() < e2.getStart())
			return -1;
		
		return 1;
	}

}