CES 2015 đã khép lại với khá ít điểm nhấn với làng smartphone ngoại trừ chiếc LG G Flex 2. Tuy nhiên, mọi chuyện chắc chắn sẽ rất khác tại MWC được diễn ra vào tháng 2 tới đây. Theo nhiều nguồn tin, sẽ có tất cả 6 chiếc điện thoại cao cấp đến từ Samsung, HTC , LG, Sony. 6 sản phẩm này sẽ tạo nên một cuộc đua hết sức khốc liệt và thiết bị nào ấn tượng hơn sẽ là người chiến thắng, ít nhất là trong mắt các nhà báo công nghệ từ khắp nơi trên thế giới.

LG G4

Gần như chắc chắn LG G4 sẽ góp mặt tại MWC 2015. Theo nhiều nguồn tin rò rỉ, mẫu điện thoại cao cấp tiếp theo của LG sẽ có cấu hình phần cứng tương tự như chiếc G Flex 2 ra mắt cách đây không lâu, ngoại trừ độ phân giải màn hình là 2K.

a6076c0d442030f744070614ba736416.jpg

Concept LG G4. Ảnh: Internet

G4 sẽ được trang bị SoC Qualcomm Snapdragon 810, dung lượng RAM 3GB (không ngoại trừ khả năng LG sẽ đẩy lên mức 4GB để có thể cạnh tranh tốt hơn với các sản phẩm khác).

Mới đây, LG đã đăng ký sở hữu “LG G Pen”, điều đó có nghĩa rằng G4 cũng sẽ được trang bị bút cảm ứng tương tự Note 4, hứa hẹn cuộc chạy đua giữa các mẫu phablet sở hữu bút S Pen sẽ càng gay cấng hơn nữa trong thời gian tới.

SONY XPERIA Z4

Trong số 6 smartphone sẽ xuất hiện tại MWC lần này, Xperia Z4 là sản phẩm sở hữu những thông tin rò rỉ vô cùng ít ỏi. Nhiều người cho rằng thiết bị này sẽ có một chút cải tiến về thiết kế khi không còn dùng nắp che cho các chi tiết như loa hay cổng microUSB. Ngoài ra, máy cũng có th��� hỗ trợ một camera sau có độ phân giải cao hơn mức 20.7 MP trên các mẫu smartphone tiền nhiệm.

3d97c419ed4026c3fc0adfa89571d247.jpg

Concept Xperia Z4. Ảnh: Internet

Về cấu hình, Xperia Z4 được cho là sở hữu màn hình kích thước 5.5 inch với độ phân giải 2K, sử dụng chip Snapdragon 810, có RAM 4GB cùng bộ nhớ trong 32GB. Bên cạnh đó, Z4 cũng được đồn đoán sẽ tích hợp chuẩn sạc không dây Qi cùng khả năng chống nước theo tiêu chuẩn IP68.

SAMSUNG GALAXY S6 VÀ S6 EDGE

Khả năng ra mắt của Galaxy S6 Edge vẫn còn bỏ ngỏ nhưng với S6 lại là một chuyện khác. Sự kiện Samsung Unpacked đầu tiên trong năm luôn chứng kiến sự xuất hiện của một mẫu flag-ship của dòng Galaxy, đồng thời, mở ra một kỳ MWC thành công. Năm ngoái, Galaxy S5 đã không thành công như mong đợi, bởi vậy, sự ra mắt của S6 sẽ được Samsung chau chuốt hơn và chứa đựng đầy sự bất ngờ đối với người tham gia cũng như nhà báo công nghệ toàn thế giới.

36ba2be36a6988065ee31725bd721ddf.jpg

Bản concept Samsung Galaxy S6. Ảnh: Internet

Thông số cấu hình Samsung Galaxy S6 bao gồm, màn hình được nâng cấp lên kích thước 5.5 inch, độ phân giải Quad HD (1440x2560), camera phụ 5MP và camera chính lên tới 20MP. Bên cạnh đó, máy sẽ chạy trên vi xử lý thế hệ mới nhất của Samsung mang tên Exynos 7420 8 nhân, hoạt động dựa trên cấu trúc 64-bit, đồ họa ARM Mali-T760 tích hợp. Kế đến, máy sẽ vận hành trên nền tảng Android 5.0 Lollipop mới nhất của Google, 3G RAM, 32GB bộ nhớ trong.

BỘ ĐÔI HTC ONE M9 VÀ ONE M9 PLUS

Dòng One của HTC luôn là một điểm nhấn vô cùng quan trọng của HTC trong vài năm trở lại đây và ngay cả khi hãng điện thoại Đài Loan rơi vào tình thế khó khăn, dòng One lại đóng một vai trò là vị cứu tinh. Bởi vậy, sự hiện diện của One M9 tại MWC sẽ quyết định được sự thành bại của HTC trong năm 2015.

bc83c5dedc4ff882ce6de9bdd25dfd13.jpg

Concept tuyệt đẹp về HTC One M9. Ảnh: Internet

HTC được dự đoán sẽ nâng cấp khá mạnh mẽ phần cứng của HTC One(M9), máy sẽ được trang bị màn hình 5.2 inch có độ phân giải 1440x2560 pixel (2K), mật độ điểm ảnh đạt được 564ppi, cao hơn hẳn so với One (M8) là 441ppi. Không những thế, HTC còn cung cấp cho One (M9) bộ vi xử lý Snapdragon 805, 3G RAM, bộ nhớ trong 64GB và 128GB, cài sẵn hệ điều hành Android 5.0. Dung lượng pin đồng thời cũng cao hơn để phù hợp với độ phân giải màn hình của máy. Ngoài ra, máy còn được hỗ trợ khả năng chống nước, không rõ đây là loại chứng chỉ nào, nhưng sẽ cao hơn loại IPx3 đang được trang bị trên One (M9). Camera sau cũng sẽ có độ phân giải cao hơn, tuy nhiên vẫn chưa rõ đây có phải là công nghệ Ultrapixel hay không.

Ngoài ra, One M9 sẽ có thêm một phiên bản lớn hơn với cấu hình tương tự và không có nhiều sự khác biệt ngoài kích thước màn hình lớn hơn một chút.

 

Theo Techz