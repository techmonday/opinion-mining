﻿Đánh giá chi tiết Galaxy Note 4: chiếc Note hoàn thiện nhất

Samsung Galaxy Note 4 có rất nhiều những thay đổi, trong đó thay đổi về thiết kế bên ngoài là đáng nói đến nhất. Sự khác biệt này đã làm cho Note 4 trở nên hấp dẫn hơn hẳn so với năm ngoái, nó là smartphone khung nhôm đầu tiên của Samsung được sản xuất đại trà sau Galaxy Alpha mang tính thử nghiệm. Trước khi iPhone 6 Plus xuất hiện, tất cả các đối thủ của dòng Galaxy Note đều đã thất bại. Liệu với mức giá khoảng 18 triệu đồng ở Việt Nam, Note 4 có giành được nhiều thành công như những đàn anh đi trước hay không?

Thiết kế:
Không phải S, mà Note mới chính là dòng sản phẩm được người dùng Samsung tự hào nhất. Sự lựa chọn đó chủ yếu đến từ sức mạnh, từ màn hình lớn và đặc biệt là thời lượng pin thật sự xuất sắc hơn là đến từ thiết kế và vẻ ngoài của máy. Nói một cách nôm na, người dùng các dòng Note đi trước phải hy sinh, chọn hoặc tính năng hoặc thiết kế và điều đó đã được cải tiến một cách đáng kể trên Note 4. Chiếc máy này không thể nói là quá đẹp, quá xuất sắc nhưng rõ ràng nó tốt và nhìn cao cấp hơn hẳn so với Note 3 hay S5 liền kề.

Samsung_Galaxy_Note_4_review-2.
​
Trên Galaxy Note 4, Samsung tiếp tục kế thừa thiết kế hơi vuông kiểu Note 3, từ bỏ thiết kế bo tròn lấy cảm hứng từ thiên nhiên mà họ khởi đầu từ S3 và Note 2. Đây là một động thái đi ngược lại hoàn toàn với các đối thủ khác, LG làm máy nhẹ nhàng hơn, Apple cũng bo tròn hơn, Sony và HTC lại càng cong mềm mại trong khi Samsung vẫn làm cho máy cứng và khác biệt hẳn. Điều này khá lạ vì rõ ràng là máy càng cong thì sẽ càng bớt khó chịu khi cầm, nhất là đối với các thiết bị có màn hình lớn.

Samsung_Galaxy_Note_4_review-17.
Với cách thiết kế vuông vức kiểu này thì chúng ta sẽ không cảm thấy dễ chịu khi cầm Note 4, nó hơi khô và sắc
​
Với các máy có màn hình nhỏ như Galaxy Alpha, thiết kế vuông vức của Note 4 không gây quá nhiều khó chịu nhưng đối với máy lớn thì mình nghĩ nó là một nhược điểm. Samsung vẫn chấp nhận làm máy hơi cong đều nhẹ ở nắp lưng tạo điểm tựa chứ không cắt hoàn toàn và dựa vào những đường diamond cut như mặt trước nên cảm giác cầm có phần nào dễ chịu hơn. Tuy nhiên, bạn cũng cần lưu ý là phần diamond cut chạy dọc phía sau nắp lưng không tiệp hoàn toàn với viền, sẽ hơi khó chịu một chút. Nokia Lumia 930 cũng sử dụng cách thiết kế tương tự cho phía sau nhưng tiết diện của đường cắt lớn hơn và điện thoại cũng nhỏ hơn nên bạn sẽ không bị cấn nhiều như Note 4.

Samsung_Galaxy_Note_4_review-14.
​
Nói đi cũng phải nói lại, chính thiết kế này làm cho Note 4 trở nên an toàn hơn khi cầm, có điểm tựa và ít có khả năng bị rơi. Hơn thế nữa, với iPhone bạn phải cưng bao nhiều thì Note 4 không cần, nó không cho chúng ta cảm giác phải nâng niu, bảo vệ như iPhone mà là cảm giác có thể đặt bất cứ nơi đâu mà không sợ trầy. Thực chất thì mình thích một thứ cho cảm giác tự tin như vậy hơn.

Samsung_Galaxy_Note_4_review-18.
Note 4 cho phép bạn đặt lên bàn mà không quá lo lắng về việc làm trầy nó, cái này bạn nào xài iPhone sẽ thấy nỗi khổ vì phải nâng niu máy:D
​
Một trong những lý do dẫn đến việc đó là Samsung vẫn tiếp tục dùng lớp giả da ở mặt sau, nhìn thì không được đẹp nhưng nó sần, không trơn và hiếm khi bị trầy. Nếu bạn chọn phiên bản màu đen thì da sẽ trơn hơn một chút.

Samsung_Galaxy_Note_4_review-27.
Cả bản màu đen và trắng đều là mạ vào, chưa rõ về lâu về dài lớp mạ này có bị bạc màu đi như iPhone 5 trước kia hay không
​
Lại câu chuyện màu sắc, hãy chọn màu đen, đừng mua trắng hay hồng nếu muốn tận hưởng vẻ đẹp nhất của Note 4. Cũng như iPhone 6 Plus đẹp nhất là màu xám, phiên bản Note 4 đen thể hiện rất rõ các cạnh được vuốt về phía mép mà Samsung gọi là màn hình 2.5D, nó hắt ánh sáng bên ngoài và làm cho màn hình có cảm giác nổi lên phía trên. Bản màu trắng mình mượn được không thể hiện rõ điều đó.

Samsung_Galaxy_Note_4_review-13.
Màu đen thể hiện tốt nhất những phẩm chất ở mặt trước của Note 4
​
Quay trở lại với mặt trước, mặt đẹp nhất của Note 4. Như đã nói thì Samsung đã dùng màn hình với mép được mài xuống về phía viền để tạo cảm giác thoải mái khi chúng ta sử dụng. Tuy vậy, họ lại không làm cho khung viền nhôm bảo vệ máy bên ngoài thấp xuống dưới màn hình, hệ quả là khi chúng ta thực hiện các thao tác vuốt hay kéo thì tay sẽ va vào viền bị đội lên không dễ chịu như Xperia Z3, iPhone 6 Plus hay Lumia 930. Vấn đề này được nói khá nhiều lần nhưng có lẽ Samsung cảm thấy người dùng của họ không quá khó chịu với nó nên sẽ không đưa ra nhiều thay đổi. Trước đó thì Galaxy Alpha ít bị tình trạng này, mặt phẳng khung máy thấp hơn 1 tí xíu so với mặt cắt của màn hình. Viền cao hơn màn hình giảm khả năng vỡ nếu bị rơi.

Samsung_Galaxy_Note_4_review-7.
Ở phiên bản thử nghiệm, màn hình thấp hơn rõ rệt so với khung xung quanh nhưng bản thương mại thì điều đó đã được cải thiện đáng kể, màn hình sẽ nằm hơi cao hơn một chút xíu nhưng không tiệp vào khung, vẫn tạo ra khoảng trống và gờ sác khi tay chúng ta vuốt. Có thể bạn nghĩ nó bình thường nhưng nếu thực hiện thao tác đa nhiệm kiểu vuốt sang phải/trái và vuốt ngược lại nhanh thì sẽ rất khó chịu
​
Hầu hết các loại xe trên thị trường đều có đầu đẹp hơn đuôi vì đặc tính khí động học. Trên điện thoại thì cũng tương tự như vậy, phải chứa loa cùng cụm camera nên mặt trước thường đẹp hơn mặt sau. Ở một số thiết bị, người ta chuyển loa xuống đáy và phẳng hóa camera, giúp nó hòa hợp với phần khung còn lại & tăng tính thẩm mỹ lên rất nhiều. Note 4 tiếp tục thể hiện rõ tư tưởng đưa tính năng lên trên thiết kế khi đẩy loa trở lại mặt sau, tạo một gờ cho nó hơi cao hơn so với mặt bàn thoát tiếng đồng thời nâng cụm camera chụp ảnh tốt hơn bất chấp việc nó sẽ dày và lớn hơn khá nhiều. iPhone 6 Plus cũng có cụm camera hơi nổi lên nhưng nó lệch hẳn sang một bên, dễ gây chú ý hơn cụm camera đặt giữa của Note 4.

Samsung_Galaxy_Note_4_review-3.
​
Ngoài camera thì Note 4 còn chứa thêm cụm cảm biến nhịp tim kiêm đèn flash ở mặt sau. Cá nhân mình ước nó là khu vực đặt cảm biến vân tay thì sẽ tốt hơn. Có thể bạn không tin, nhưng rõ ràng là ngoại trừ cách chạm và mở của cảm biến vân tay kiểu iPhone có thể đặt ở bất cứ đâu thì các cảm biến kiểu quét nên đặt ở vị trí nào thuận tiện nhất cho chúng ta sử dụng. Rõ ràng là nếu tích hợp vào một phần phím home như Note 4, chúng ta vừa phải dùng 2 tay để mở khóa màn hình mà vừa phải quét nhiều lần. Trung bình thì mình phải quét 2 lần mới vào được máy, điều rất hiếm khi xảy ra nếu đặt vân tay ở mặt lưng như HTC One Max. Để dùng bằng một tay thì bạn có thể quét vân tay theo chiều ngang, nó sẽ hơi với nhưng vẫn hoạt động được.

 Samsung_Galaxy_Note_4_review-19.
Phím Home nhạy hơn S5 nhưng vẫn chưa thật tốt, một phần vì cách quét, một phần vì vị trí đặt
Samsung_Galaxy_Note_4_review-11.
Mình thích phím home đặt ở đây hơn
​
Như thường lệ, các thiết bị Samsung vẫn cho phép thay pin bằng cách mở nắp sau. Nhiều người cho rằng đây là một ưu điểm nhưng với những thông tin mới nhất thì có vẻ như Samsung cũng sẽ sớm chuyển sang các máy nguyên khối hoàn toàn. Tuy nhiên, trước khi điều đó xảy ra, bạn vẫn có thể thay bao nhiêu viên pin tùy thích với Note 4. Thực chất thì trừ khi bạn đi du lịch hay di chuyển dài thì thay pin là vô nghĩa, thời lượng pin của Note 4 đủ cho chúng ta dùng một ngày.

 Samsung_Galaxy_Note_4_review-22. Phần lồi lõm này là phần xấu nhất của Note 4, giống với Galaxy Alpha
Samsung_Galaxy_Note_4_review-21. Samsung_Galaxy_Note_4_review-20. Cổng USB 3.0 đã bị thay bằng USB 2.0, có lẽ Samsung không muốn gặp vấn đề với driver như Note 3 trước đó. Nếu bạn chờ có nhiều thay đổi thì USB 3.1 Type C mới là cái chúng ta mong chờ Samsung_Galaxy_Note_4_review-16.
Vẫn có khoảng hở giữa viền máy và màn hình, cái này là cái mình chưa thích khi chạy đa nhiệm​

Màn hình:
Trên Note 4, Samsung đã thay màn hình FullHD ở phiên bản trước bằng 2K. Cách sắp xếp điểm ảnh kiểu PenTile như một số đời Note đầu tiên không còn được duy trì mà chuyển hẳn sang dạng diamond mới hơn. Với cách sắp xếp này thì tình trạng rỗ đã giảm hẳn, gần như không còn xuất hiện như trước kia. Một trong những nguyên nhân làm cho màn hình này tốt hơn cũng là nhờ độ phân giải mới có số điểm ảnh cao hơn.

Samsung_Galaxy_Note_4_review.
​
Về màu sắc, rõ ràng là Note 4 và Galaxy Alpha có sự cải tiến đáng kể. Không thể nói màu sắc trên Note 4 là tiêu chuẩn, nhưng rõ ràng nó đã đỡ đậm hơn, dịu hơn một chút so với mắt. Thông thường thì người dùng nên chuyển về chế độ adaptive display để màn hình tự điều chỉnh tùy theo nội dung hiển thị. Cá nhân mình vẫn ước màn hình Note 4 dịu hơn một chút nữa, giảm một xíu saturation và constrast sẽ tuyệt hơn nhưng có rất nhiều người dùng Samsung cực kỳ thích màu sắc kiểu này.

Ngoài Adaptive, Samsung còn bổ sung thêm một vài chế độ, trong đó có Basic mà rất nhiều người tin rằng nó sẽ giúp hình ảnh hiển thị ít bị đậm, dành cho những ai thích hình ảnh trung thực hơn. Tuy nhiên, thực tế cho thấy với chế độ basic thì hình ảnh lại trở nên nhạt quá mức cần thiết, độ tương phản thấp, bão hòa màu cũng kém và vàng quá mức cần thiết, một vẻ vàng hơi ngả xanh green một chút (vàng là kết hợp của red và green).

 Samsung_Galaxy_Note_4_review-24.
Samsung vẫn trung thành với cách dùng phím cảm ứng vật lý phía dưới nhưng phím home lại không cho cảm ứng theo, đang back back mà phải nhấn phím home sẽ không đồng bộ với nhau
​
Trên thực tế, khi hiển thị màu trắng thì ở bất cứ góc nhìn nào bạn vẫn thấy màn hình hơi nghiêng nhẹ về màu xanh. Điều này là chắc chắn và không thể bàn cãi. Tuy nhiên, không phải ai cũng khó chịu với màu xanh này, mình nghĩ nó đã dễ chấp nhận hơn rất nhiều so với các máy Note trước kia. Đây là những cảm nhận mang tính cá nhân nhiều hơn vì mình thích cách IPS LCD hiển thị hơn là AMOLED của Samsung căn chỉnh ở thời điểm hiện tại.

Về vấn đề hiển thị, có rất nhiều thông tin cho rằng màn hình Note 4 là tuyệt nhất ở thời điểm hiện tại, đặc biệt là khi đo bằng máy. Chuyện đó không có gì đáng ngạc nhiên, về thông số tương phản, góc nhìn thì chắc chắn LCD không và sẽ không bao giờ bằng OLED nói chung hay AMOLED nói riêng. Nếu đo bằng việc hiển thị màu xanh xanh nhất hay đỏ đỏ nhất, LCD cũng sẽ thua. Tuy nhiên, đó là thông số hiển thị tối đa và các giá trị ở giữa, tức các giá trị mà chúng ta hiển thị hằng ngày mới là cái quan trọng. Chẳng ai dùng điện thoại của họ chỉ để hiển thị RGB suốt cả ngày. Có thể thấy rõ ràng là ở hạng mục đó, màn hình AMOLED do Samsung cân chỉnh không được đánh giá cao. Cá nhân mà nói, màn hình iPhone 6 Plus vẫn là màn hình đẹp nhất hiện tại trên di động, màn hình Note 4 vẫn có cảm giác bị một lớp sương phủ rất mỏng trên đó làm cho đục nhẹ đi, màn hình Xperia Z3 thì trong nhất nhưng hơi rực một chút, bù lại nó cho phép chúng ta chỉnh từng kênh màu một.

Samsung_Galaxy_Note_4_review-15.
​
Chế độ điều chỉnh tự động trên Note 4 khá lạ, nó có xu hướng nghiêng về góc hơi tối khi ở trong nhà, ngoài nắng vẫn điều chỉnh bình thường. Điều này giúp tiết kiệm pin nhưng đôi khi sẽ khiến mắt chung ta hơi khó chịu. Khi này việc điều chỉnh tay rất cần thiết, không quá khó ngay vì nó nằm ngay trên notification bar.

Sức mạnh:
Note 4 đang sở hữu cấu hình mạnh nhất trong tất cả các máy Android trên thị trường, nó có 2 phiên bản sử dụng chip Exynos 5433 (ở Việt Nam, dùng modem LTE Intel) hoặc SnapDragon 805 (tích hợp sẵn LTE). Thông thường bảo SnapDragon được đánh giá cao hơn nhưng rõ ràng là với việc sử dụng Exynos ở thị trường nội địa là Hàn Quốc, Samsung đã ngầm tỏ rõ con chip của họ không còn lép vé như trước kia nữa.

Con chip 5433 là con chip mạnh nhất của Samsung hiện tại, nếu chỉ nhìn vào các điểm benchmark thì rõ ràng là nó cũng rất xuất sắc với Antutu đạt…, con chip đồ họa…. giúp đạt xx điểm 3D Mark và Geekbench. Một trong những lý do là nó dùng nhân 4 nhân Cortex A57 và 4 nhân phụ Cortex A53 hỗ trợ 64bit và dựa trên vi kiến trúc ARMv8 mới hoàn toàn so với A15 trước kia. ARMv8 là vi kiến trúc nền của Apple A7/A8 hay nVidia Denver (Tegra K1).

Đó chỉ là một vấn đề trên giấy tờ, còn thực tế thì điểm mạnh lớn nhất của Exynos 5433 là nó ít lag hơn rất nhiều. Các con chip Exynos chạy ứng dụng rất tốt nhưng ở màn hình home thì bị trễ khi chúng ta chạm, có vẻ như nó cần thời gian “hâm nóng” nhưng 5433 đã giảm đi đáng kể. Rất hiếm khi máy bị đơ hay khựng, chỉ khi vẽ S-Pen tren S-Note bằng ngòi bút nghệ thuật mới thì máy mới bị trễ khoảng 0.5 giây do phải xử lý nhiều hơn để tạo ra ngòi bút này, bù lại thì dù chữ xấu như mình nhưng nhìn cũng không đến nỗi tệ lắm. S-Pen là một điểm rất hay của Note 4 mà chúng ta sẽ đi chi tiết hơn ở phần sau của bài viết.

Screenshot_2014-10-24-13-32-27. Screenshot_2014-10-23-15-05-57. Screenshot_2014-10-23-15-07-17. Screenshot_2014-10-24-13-32-36. Screenshot_2014-10-24-13-35-10. ​
Camera:
Trước khi đi sâu hơn, chúng ta cần khẳng định một điều: camera Note 4 rất tuyệt. Nếu như iPhone từng được yêu thích không phải vì nó chụp đẹp nhất (chào Lumia) mà vì nó dễ sử dụng nhất thì nay camera Note cũng phần nào làm được điều đó. Việc của bạn chỉ là bấm nút chụp, máy sẽ xử lý phần còn lại cho chúng ta. Camera Note 4 đặc biệt xuất sắc ở phần tự động, Samsung đã cải tiến rất nhiều thuật toán của họ, đặc biệt là local tone mapping để chỉnh sửa ảnh theo khu vực rất khéo. Điều này làm cho chúng ta có cảm giác dải Dynamic Range của máy rất rộng, đôi khi là rộng quá giới hạn của một chiếc điện thoại. Lúc trước thì iPhone 5s trở về trước xử lý local tone mapping cực kỳ tốt, nhưng iPhone 6/6 Plus lại chưa tốt làm cho ảnh dễ bị cháy khi bị chênh sáng mạnh còn Note 4 cải thiện nhiều so với đời trước. Năm ngoái mình có mang Note 3 đi chơi chung với Lumia 1020, hệ quả là Note 3 thường xuyên bị ném ở nhà vì khu vực sáng thường hay bị blow out, thuật toán xử lý ảnh quá giả còn Note 4 đã đỡ hơn nhiều, rất nhiều.

 Samsung_Galaxy_Note_4_review-10.
Camera này cũng lồi, "mốt mới", hy vọng mode này sớm biến mất
​
Tự động tốt hơn nhưng Samsung vẫn cần phải cải tiến các thuật toán liên quan tới EV và đặc biệt là HDR. Trên iPhone thì chúng ta có chế độ auto HDR sẽ tự nhận diện cảnh nào cần HDR và cảnh nào không trong khi Note 4 người dùng vẫn phải kích hoạt bằng tay. Lý do nhắc đến iPhone ở đây là vì camera Note 4 đi theo hướng đơn giản hóa hơn để ai cũng chụp được, đúng với triết lý iPhone theo từ đầu. Ngoài ra, Samsung cũng cần thay đổi cách người dùng thay đổi EV, tốn quá nhiều bước để tăng/giảm sáng cho một bức hình, EV lại đi theo bước 0.5 theo kiểu HTC thay vì 0.33 truyền thống đồng thời có vẻ như Samsung dựa quá nhiều vào phần mềm, ở mức -2 và +2 EV mà sự thay đổi là không đáng kể, giống như họ đã cố kiếm chế mọi thứ bằng phần mềm chứ không phải thay đổi ISO hay tốc độ thật sự.

Samsung_Galaxy_Note_4_review-11.
​
Trong môi trường đủ sáng, thậm chí là hơi gắt thì Note 4 vẫn giữ được rất nhiều chi tiết, cần lưu ý là bạn phải coi trên máy tính để có cái nhìn chính xác hơn. Samsung có tạo một chế độ hiển thị riêng là AMOLED Photo để chúng ta nhìn trên điện thoại hợp nhãn hơn nhưng nó lại làm cho chúng ta hơi khó khăn trong việc xác định đâu là màu mình mong muốn để up lên Facebook/Flickr chia sẻ với bạn bè. Khi mình dùng các máy khác thì ảnh mình nhìn thấy trên màn hình không khác biệt nhiều với hình trên máy tính còn Note 4 hay các máy AMOLED của Samsung thì có, các bạn nên lưu ý để kéo quá tay :D

Samsung_Galaxy_Note_4_review-12.
​
Còn chụp tối thì sao? Có thể một trong những lý do mà Samsung tạm bỏ cảm biến ISOCELL nhà làm trên S5 để chuyển sang cảm biến Sony là vì hiệu năng chụp tối kém. Cảm biến Sony xuất hiện trên Note 4 có kích cỡ 1/2.6”, lớn hơn một chút so với các cảm biến 1/3” khá phổ biến nhưng nó vẫn quá nhỏ để nhét độ phân giải 16MP vào. Chính vì vậy, kích cỡ mỗi điểm ảnh chỉ là 1.1 micromet vuông, không đủ lớn để các giếng sáng bắt ánh sáng tốt. Samsung đã trang bị chống rung quang học để cải tiến hiệu năng chụp đêm nhưng rõ ràng là cái gì cũng có giới hạn của nó, hiệu năng mặt này của Note 4 chỉ ở mức trung bình.

Có một điểm yếu nữa cần nhắc tới: trải nghiệm người dùng. Lại phải nhắc tới iPhone một lần nữa (không phải iPhone 6 Plus thì ai sẽ là đối thủ chính của Note 4 ở phân khúc màn hình lớn đây?). Mỗi khi bấm nút chụp ảnh trên iPhone, bạn tự tin là hình đó đã được lưu và chúng ta cảm thấy rất an toàn, không lo bị rung vì máy phản hồi cực kỳ nhanh và mọi thao tác đều được đơn giản tới mức tối đa, kể cả các biểu tượng. Trên Note 4 thì dù đã đi theo hướng tự động nhưng cảm giác bấm nút chụp hình cho đến thao tác xem hình lại vẫn cho hơi thiếu tự tin và thường mình phải chụp 2 tấm cho chắc ăn dù điều đó không cần thiết trong hầu hết các trường hợp.

Nhìn chung, camera Note 4 là một thay đổi rất lớn, dù vẫn còn vài nhược điểm về cơ chế điều khiển, cảm giác chụp hay đôi khi là cân bằng trắng nhưng rõ ràng nó tạo rất nhiều cảm hứng cho người chụp, điều không xuất hiện ở Note 3 hay thậm chí là S5. Tiêu cự camera sau của máy ở tỷ lệ 16:9 là 31mm, không quá hẹp cũng không quá rộng.

Một số hình chụp từ Note 4, trừ tấm đầu tiên không làm gì và tấm thứ 2 chỉ crop trực tiếp trên máy thì các tấm còn lại đều có áp màu sơ qua một chút, sau đó resize 1024 bằng Photoshop với chất lượng 100%. Hình gốc không resize camera Galaxy Note 4 xem tại Flickr của mình. Các bạn có thể xem bài chi tiết về camera Galaxy Note 4 tại đây.

2014-10-24 04.41.58 1. 2014-10-24 04.44.56 1. 2014-10-24 04.45.06 1. 2014-10-24 04.45.16 1. 2014-10-24 04.45.31 1. 2014-10-16 03.41.17 1. 2014-10-20 04.23.21 1-1. 2014-10-15 09.58.27 1. ​

Pin:
Đây là điểm mạnh của các dòng Note trước, và rất tiếc mình không thể chia sẻ kỹ với các bạn trong bài review này. Máy mình cầm đang là máy thử nghiệm và chưa thể up lên bản cập nhật mới nhất mà Samsung cho là sẽ “cải thiện đáng kể thời gian sử dụng pin”. Vì lý do gì đó mà máy mình hết pin khá nhanh, chỉ khoảng hơn 4 tiếng on-screen và 8-9 tiếng stand by. Cái này thì anh @vuhai6 sẽ cho bạn biết rõ hơn, anh Hải đã mua 1 máy chính thức để thử rồi.

Samsung_Galaxy_Note_4_review-26.
​
Note 4 có một chế độ sạc pin mới là nếu máy bạn hết pin, nó sẽ nạp đầy 50% chỉ trong 30 phút và sau đó sẽ sạc chậm lại. Samsung thực hiện điều này nhờ vào công nghệ QuickCharge 2.0 của Qualcomm cho phép đẩy nguồn lên 9V 1.67A (khoảng 15W) hoặc 5V 2A (10W) sau này. Bạn có thể dùng cục sạc của Samsung hoặc sạc của HTC như mình có thử nghiệm hôm trước, chỉ cần hỗ trợ QuickCharge 2.0 là được.

Phần mềm và S-Pen:
Samsung có 3 năm kinh nghiệm trong việc tối ưu hóa phần mềm cho màn hình lớn, và họ làm điều đó rất tốt trên Note 4. Lý do máy có 3GB RAM là vì Samsung trang bị quá nhiều tính năng đa nhiệm, chạy nhiều phần mềm cùng lúc (chạy càng nhiều càng giựt, bạn chạy khoảng 2 cái là được rồi). Tính năng mình thích nhất là thu nhỏ màn hình chỉ bằng cách kéo từ mép, giống với thu nhỏ cửa sổ trên PC của chúng ta. Ngoài ra, Samsung cũng đã cải tiến đa nhiệm trên Note 4, thay nút Option bằng nút đa nhiệm hiện đại, nhiều chức năng hơn (xem chi tiết hơn trong video).



Samsung_Galaxy_Note_4_review-4.
​
Có một điểm yếu ở đây là tuy đã thu nhỏ cửa sổ lại nhưng chúng ta chưa thể kéo nó vào một góc để tự động co vào làm 2 như Windows 7 trở lên (tính năng Aero Snap) và đặc biệt là chưa thể tự động gom ứng dụng theo tab kiểu Chat Head của Facebook Messenger (Note 4 có hiển thị kiểu Chat Head nhưng rất lung tung và chưa đẹp). Nếu Samsung làm được điều đó thì tính năng đa nhiệm trên Note 4 còn tuyệt vời hơn nữa. Các bạn có thể xem video để có cái nhìn sơ bộ, bạn @Duy Luân sẽ nói chi tiết hơn về vấn đề này trong một bài khác.

Samsung_Galaxy_Note_4_review-9.
​
Vê S-Pen, thường thì mình không thích nó vì chẳng mang lại nhiều tác dụng động thời khá phiền phức khi phải rút ra rút vào. Trên Note 4 thì cuối cùng, S-Pen đã có tác dụng thật sự: nó cho phép chúng ta chọn văn bản ở rất nhiều các trường dữ liệu khác nhau, chẳng hạn như trên ứng dụng Facebook là khu vực mà chúng ta thường xuyên không copy được nội dung mà chỉ có thể share lại. Để làm điều đó bạn chỉ cần nhấn nút trên S-Pen và kéo lên trên đoạn văn bản cần bôi là xong. Một số người cho là Samsung đã chụp ảnh màn hình khu vực đó lại và dùng công nghệ nhận diện chữ để lấy dữ liệu nhưng mình thì cho rằng họ đã đọc HTML để lấy trường dữ liệu đó. Dù sao thì nó cũng rất phức tạp và chúng ta không cần quan tâm nhiều, chỉ biết là nó làm việc được, thế là xong:D

Samsung_Galaxy_Note_4_review-23.
​
Ngoài ra, trên S-Pen mới còn xuất hiện một ngòi bút với tên gọi tiếng Việt là “bút thư pháp”. Với cây bút này thì cả người viết xấu như mình cũng vẽ được các nét không tệ chút nào. Trong video mình có thử các nét bút đó, cực kỳ thích. Các nét còn lại thì vẽ cỡ nào cũng xấu nên không hài lòng lắm :D Trên Note 4 thì S-Pen mới được cải tiến độ nhạy lên nhận được 2048 mức lực khác nhau so với 1024 trước đó. Cái này bạn nào vẽ chắc sẽ cảm nhận được, mình không có hoa tay nên thấy cũng bình thường.

Khen nhiều về phần mềm, vậy điểm yếu là gì? TouchWiz vẫn quá nặng nề và khó sử dụng, đặc biệt là các tính năng liên quan tới S-Pen hay các tính năng nâng cao, các thiết lập. Nếu chưa từng dùng S-Pen bao giờ hoặc thậm chí đã dùng Note 2, Note 3 thì bạn vẫn phải đánh vật cả ngày để nhớ đủ các chức năng của cây bút mới. Thật may là Samsung đã dần cố gắng cải tiến, đưa nhiều hơn những chức năng hữu ích vào menu bên ngoài nhưng rõ ràng nó vẫn chưa đủ, rất nhiều người cuối cùng chẳng đụng tới bút (dù dây là lý do họ mua Note) hoặc chỉ dùng một vài chức năng nào đó mà không phải là tất cả như Samsung mong muốn.

Kết luận:
Samsung Galaxy Note 4 là một chiếc điện thoại rất tuyệt, nó không chỉ xử lý bài toán về cấu hình (lag) mà còn xử lý luôn cả vấn đề ngoại hình mà nhiều người than phiền trước kia. Rõ ràng với những thay đổi đó thì Note 4 trở nên đáng lựa chọn hơn, rất tiếc là nó không còn một mình một đường như trước kia với sự xuất hiện của iPhone 6 Plus. Hiện tại chưa rõ giá của iPhone 6 Plus ở Việt Nam là bao nhiêu nhưng với động thái tăng một triệu đồng lên thành 18 triệu thì chắc chắn thiện cảm của người dùng dành cho Note 4 sẽ bị giảm đi ít nhiều.

Xét về mặt sản phẩm đơn thuần, Note 4 rất xuất sắc. Nếu bạn chưa bao giờ có kinh nghiệm dùng Android và muốn thử nghiệm một cái gì đó rất Android, tức rối nhưng cực kỳ nhiều tính năng và cho phép tùy biến rất nhiều thì rõ ràng Note 4 là một lựa chọn khó có thể sai được. Tuy nhiên, người dùng muốn mọi thứ nhẹ nhàng, vui vẻ hơn mà không bị ngập tràn trong quá nhiều thứ, và đặc biệt là có giá rẻ hơn thì HTC One Max, LG G Pro 2 hay đặc biệt là Xperia Z Ultra với giá siêu thấp so với cấu hình cũng là những lựa chọn rất hợp lý.

Ưu điểm:
Thiết kế cải tiến rất nhiều
S-Pen hữu ích hơn hẳn
Hiệu năng tốt hơn các đời Exynos trước
Camera chụp ngày xuất sắc
Pin tốt (chờ xác nhận), sạc nhanh

Nhược điểm:
Chụp đêm chưa ngon
Không chống nước như S5, hoàn thiện chưa tương xứng với giá thành
Phần mềm vẫn còn rối
Giá

Cấu hình cơ bản:
Mạng: 2.5G, 3G, 4G (Cat 4, Cat 6)
Vi xử lý: Exynos 8 nhân (1.9GHz Quad Cortex A57 + 1.3GHz Quad Cortex A53 ở Việt Nam
Màn hình: 5.7", Quad HD Super AMOLED (2560 x 1440)
OS: Android 4.4
Camera: 16 Mp, Smart OIS (chống rung quang học thông minh), Camera trước: 3.7Mp, F1.9
Bộ nhớ trong: 32GB, khe thẻ MicroSD hỗ trợ thẻ lên đến 64GB
RAM 3GB
Cảm biến vân tay
Pin: 3220mAh, sạc nhanh
Kích thước: 153.5 x 78.6 x 8.5mm
Trọng lượng: 176g


đánh giá rất hay nhưng i cuồng lại sắp gọi thím Sơn là seeder r

Note năm nào cũng là đỉnh của đỉnh. Năm nay không biết sẽ làm ăn thế nào khi các đối thủ cũng có những bược tiến lớn, gần như SS không đủ sức cản lại.

Năm 2013, Samsung cũng đứng thứ 6 trên thế giới về tiền kiếm được - lợi nhuận (Công ty số 1 không phải là Apple mà là 1 công ty Trung Quốc nhé).

6. Samsung Electronics

> Net income from cont. operations: $27.2 billion
> Country: South Korea
> Industry: Technology hardware, storage and peripherals
> Revenue: $216.6 billion

có con note 1 vỡ màn giờ vẫn vứt xó nhìn con này không có thích lắm

Mong a e bình tĩnh không war ở topic này.....:D ...
Với mình thì note 4 là hàng top điện thoại của thế giới ,mà đã là top thì mọi so sánh là khập khiễng ...
Cũng như ng dùng iphone vậy ....

Huề , hoặc M.U thua 1 trái mình có Note 4 , không thì mất 2 tuần lương :(

nếu giá chỉ 10 chai thì mới gọi là hoàn thiện
trời sanh sam 1938 sao còn sanh pô 1975 (apple)

thiết nghĩ là 1 đảng viên yeu nuoc thì hãy mua máy samsung
quá chuan phải ko

SamSung chỉ đem so với SamSung với phiên bản trước
và chỉ như thế

Thích Quá mà hem được mua híc híc

Đã và đang dùng e nó...quá tuyệt

Ngoại trừ hở 4 cạnh viền. còn lại ok :)

Ngoại trừ hở 4 cạnh viền. còn lại ok :)

Bên Cellphones ngta Tets và kiểm tra rồi, cái khe đó đâu có lớn đâu và các máy z2 or z1 của Sony cũng có . Nhưng mắt thường k thể nhìn đc

Điện thoại đầu tiên trên thế giới có chức năng cà credit card.

Tết giảm giá mua là hợp lí, 18 củ quá chát

Cứ tưởng đc bóc tem

Phần Open in multi window view của máy em k bật ON được hiccc.

Bình thường thôi

10 chai , mua đồ ăn cắp à :)

Cái title quá chính xác :D

Đồng ý với tiêu đề của chủ thớt: chiếc Note hoàn thiện nhất (so với samsung Note1, Note2, Note3), chứ hoàn toàn không phải và không bao giờ là chiếc điện thoại hoàn thiện nhất.

Làm gì có cái điện thoại nào mà gọi là hoàn thiện nhất được

Đắt hơn Note 3 1tr nhưng đáng giá hơn 6,9 lần. Đó là so với riêng Note 3.

Mọi thứ gần như rất ổn, trừ cái chỗ camera thì quá xấu

Cùng lắm là hoàn thiện nhất ở thời điểm hiện tại thôi thớt :v

Các bác chỉ cách cho e với...e k bật on được ạ.

Máy nào cũng hở nhưng Note 4 hở nhất. Cái nắp lưng nhựa năm nay a Samsung có chiêu mài mỏng lét áp vào khung nhôm nên cảm giác khít và chặt ché hơn hẳn mọi năm.

ngon toàn tập.........................chờ giá mềm!

bóc tem là gì
bóc tem được gì ?
hay chỉ để chứng tỏ mình kéo nhanh nhất mà chả đọc được gì ?

Một phong cách pha tạp rất hoàn hảo : Nắp lưng giống BB, cạnh giống iP5 , mặt trước sau vẫn SS. :p

Có tiền thì hốt ngay em này. Không đắng đo :)

"Bò, cứ lo bò trắng răng"

Note 4 tất nhiên là chiếc Note tốt nhất rồi

Em đang xài note 3 và đang rất hài lòng, phần mềm samsung sẽ là thừa thải với ai ít khám phá và rất đầy đủ nếu ai có nhu cầu. Đang cố kiếm tiền sang năm mua note 4

Mấy hôm nay ngày nào cũng có 1 bài nói về Note 4 . Note 4 là 1 chiếc điện thoại tuyệt vời , điều nay không ai phủ nhận. Nhưng có 1 sự thật là nó không phải là chiếc điện thoại dành cho đại đa số mọi người . Tinh tế cũng nên quan tâm đến nhu cầu của mọi người bây h là các smartphone giá bình dân . Lumia 730 , SS Prime ra mắt đã lâu nhưng mình chưa thấy có bài đánh giá nào về những chiếc điện thoại tầm trung này , trong khi đó nó lại là lựa chọn của hầu hết các bạn trẻ bây h . Tinh tế dành quá nhiều thời gian cho những smartphone cao cấp rồi .

Chỉ cần nhìn mặt sau là biết của Samsung

Bài viết rất hay. Note bao giờ cũng đc hoàn thiện rất tốt. 
Ps: lần trc có 2 bài đánh giá 6 vs 6+ viết rất dài. Các thím nói Mod ko ra gì. Bây giờ Mod viết thế này mấy thím thích gây war hài lòng chưa hay vẫn ngứa :D
Đã toàn là máy đt fs của các hãng thì phải nói là nhìn thật đã. :")

Ngoài @Duy Luân thì @sonlazio là người có những bài viết khách quan và trung thực, chi tiết nhất mà mình thích đọc :)

Vẫn chung tình vs em 5s thôi!

giá chát quá thôi chứ nhìn chung là ổn

Bài review nào của anh Sơn cũng cực kì chi tiết với công phu :)

Mình nhớ không nhầm thì ấn và giữ nút trở lại (back) là bật được bạn

Mình nhớ không nhầm thì ấn và giữ nút trở lại (back) là bật được bạn

Vẫn biết là máy thử nghiệm mà sao pin tệ quá nhể :eek:

Thế còn ip6 theo ý kiến của bạn thì đặt tiêu đề như thế nào

Cứ đến trang 5 hoặc 6 là bắt đầu có tranh luận nảy lửa :)

Bản thân Apple cũng luôn chỉ nói "chiếc iPhone tốt nhất thôi", chả có cái phone nào là tốt nhất chung chung cả, tốt nhất với cá nhân nào đó thì có.
À mà tốt nhất đừng nhắc đến iPhone ở đây, lại chuẩn bị loạn lên cho mà xem

Mod sonlazio với Duyluan viết bài tôi thấy là tốt nhất trong Tinh tế. Còn các mod khác tôi thấy chỉ làm tròn vai thôi, người thì đánh giá hơi chủ quan, thiên vị, người thì hời hợt, người thì cái tật nói ngọng mãi không sửa được.

Các bác cứ có mấy bài nhận xét chi tiết kiểu này làm em ham hố. Là fan note và đang dùng note 2, nhìn mà cứ muốn đổi mà kinh tế không cho phép

Nói ngọng thì phải chịu sữa mới được.

Mình đã thử mà không được :(

:confused: công nghệ thay đổi luôn luôn và các bạn đam mê công nghệ luôn phải đối mặt với tình trạng có thể lạc hậu bất kỳ lúc nào.

Thật sự ngày xưa màn hình sony Z rất xấu, nhưng đến Z1, Z2 thì tiến bộ rõ rệt
Màn hình Note và Galaxy S trước bị cho là màu rực nhưng giờ màn LCD của LG còn rực hơn
Màn 2k của G3 mọi người nghĩ là sẽ vượt trội hơn màn full HD nhưng nhiều người lại than là ám vàng nặng.
Trong bài này thì Mod bảo màn Note bị ám nhẹ một lớp mờ (thực sự thì từ dạo note 3 mình đã thấy hiện tượng này rồi).
Nói chung giờ ko biết màn hình nào đẹp hơn màn nào nữa

thế đem qua so sánh vs ip cho tụi mày gây war gõ phím nhé

:DTạm thời chúng ta công nhận Note 4 đứng đầu android và thứ 2/3 lần lượt là Z3/Z3c.
Sau đó đến cac thím M8 và G3. Vậy là đủ tứ đại thiên vương của android rồi: Sung - Sony - HTC - LG.

:rolleyes: 6 tháng đầu năm sau sẽ ntn đây? Sản phẩm càng ngày càng ngon, anwm sau còn được buff ăn L tập thể nữa.

Vỏ của ip6 thích hợp cho màn hình to, nhưng thiết kế của 5s lại quá đẹp.
Đã yêu thì cũng phải học chấp nhận sự hơi xấu của 6 so với 5s nữa anh

Câu cuối của bác chuẩn, nhưng mà không được đâu, mới 3 trang đầu đã có gạch rồi, mà tinhte cũng cần view, thôi vẫn sẽ cứ loạn lên thôi.

mod đúng hay admin đúng...:v

Tự ty à, tự kỷ à ! Thích war à ??? Sao phải xoắn thế ! Bất lực à ? Ăn với nói ! 

Phải công nhận Note là dòng đáng mua nhất của Sam, về mọi mặt, S giá chát + gà ! S phải sau Note 1 cấp mới đúng giá trị sử dụng

Ăn Tết xong, nếu N4 Docomo loại 2 sim tầm ~9tr thì sẽ múc e nó, chốt hạ là vậy

Pin hơn 4h tẹo là rất bình thường, RIÊNG VỀ PIN thua đứt ip6+ ! Cấm cãi

đã xài Note 2, chán chuyển LG G2. Giờ yên lòng với G2 và đợi thời đủ gạo sẽ rinh iphone cho nó lành các bác ạ.

@sonlazio Note 4 chip exynos dùng cảm biến ảnh isocell mà, chip 805 thì xài của sony

Không phải là SS mài mỏng mà là SS thiết kế mỏng =))
Bạn chả hiểu gì về gia công chế tạo cả :p

Hàng chôm chôm :)
Giá xtay dễ chịu hơn 18 triệu nhiều
https://www.nhattao.com/threads/note4-quoc-te-nguyen-seal.3504270/
Mọi thứ đều ngon, trừ mỗi cái cut diamond

Mình rất ko thích Anh Sam với cái vỏ nhựa rẽ tiền, và chưa bao giờ sài Smartphone Samúng, nhưng khi Note 4 ra mình là người đầu tiên mua nó. Mua ngày 23/10 và đến hôm nay là đã 3 ngày trên tay rồi mà vẩn còn nhiều thứ để vọc. Đúng là phần mềm còn hơi rối nhưng mức độ hoàn thiện của sản phẩm là OK. Ơ nhưng mà giá em này hiện nay chỉ ~17tr thui mà

Đã từng dùng tất cả các thể loại và ký hợp đồng dài hạn với dòng note của Samsung.
Bh có ip6 + chứ trước đây một tay ip một tay ipad thấy mệt . Cầm con note3 cảm thấy thay thế được ip + ipad .
Các sp của apple thì có độ hoàn thiện rất xuất sắc , phần cứng phần mềm được tối ưu hóa cao nhưng hệ điều hành đóng của Apple lại gây khó cho người sử dụng rất nhiều.

Đang dùng note 3, Rajasthan ok và đợi note4 giảm giá. 18 tr có vẻ k hợp lý

Macbook ko phải con nào màu cũng chuẩn ông Hiệp so với lap làm gì, kiếm cái tivi màu chuẩn mà so sáng ấy chứ :)

iPhone 5 là chiếc điện thoại khá hoàn thiện của Apple, và giờ Note 4 cũng vậy:D

Tưởng note 4 ê xỳ nốt xài cảm biến ISOCELL chứ MOD, snap mới xài cảm biến SONY.

Ôi, Nikon Df....

Sai ngay từ tít, chiếc note 5 mà ra tốt hơn chiếc này thì sao, đùa với bạn đọc công nghệ ah ?

de lai con note 1 gia tot di bac :)

Diển đàn bựa ra vì những comment của bác đấy

Dù ko thích samesung nhưng mà phải công nhận camera note 4 này khá là ngon đó. Mie Sony Làm gì đi chứ :D. Mảng chụp hình ngon vậy mà đưa vô phone chụp ko thể gọi là quá xuất sắc dc. Mấy dòng cybershot trc thì còn ok. xperia sau này thấy có vẻ đuối quá. 



Để tới ra Tết âm lịch là đổi ngang dc con ip5s ngay. Hơn nữa lúc đó thì Note 4 cũng lên Android 5. Lollipop, tối ưu phần mềm hơn, pin tốt hơn, mượt hơn. Lúc đó đổi cũng chưa muộn.

Em đang có Note 4 snapdragon bản quốc tế của nhà mạng At&T.Anh em nào có nhu cầu thì pm em nhé.Giá cả rất hấp dẫn (chỉ có 1 cái thôi nhé).Em được ng nhà mua cho mà thấy không hợp nên muốn bán (imei đầy đủ nên cac bác yên tâm nhé,test thoải mái)

Bán Note 3, ai mua hem?

đi đâu cũng thấy bạn này đưa cái doanh số ra khè, sản phẩm được nhiều người sử dụng nhất chưa chắc là sản phẩm tốt nhất nhé bạn.

em đang chờ màu Gold ra để đổi thôi, tự nhiên máu màu gold quá. hehe

Hoàn thiện tốt thì phải kể tới Lumia 930 , Xperia Z3 , Iphone 6 chứ note 4 chưa có cửa vì Samsung họ chưa có nhiều kinh nghiệm

Cũng giống như mỗi khi bắt ma túy nếu số lượng lớn thì đều nói. Đây là vụ.....vs số lượng lớn nhất từ trước tới nay. h đến note 4 cũng phán y như notwe3 năm ngoái. Sang năm nếu có not5 cũng vậy

Cứ đua cấu hình mình chỉ cần như Note 3 là okê. Cải thiện pin thì ủng hộ.... 18 chai. Quá mặn?

chỉ tranh thủ anti được chỗ nào thì anti thôi à

Công nhận là Note 4 cải tiến nhiều nhất là thiết kế nhìn sang hơn hẳn đặc biệt là màu đen.

Mình cũng đồng ý là không thể có chiếc điện thoại nào là hoàn hảo nhất được vì thời gian sẽ làm độ hoàn hảo của 1 sản phẩm thụt lùi lại, năm trước nó là hoàn hảo nhất thì năm nay không phải là nó nữa.

một chiếc điện thoại tốt, đáng mua nếu có tiền.

hế cái hãng có doanh số cao nhất ấy nó có bố thí cho bạn đồng nào ko mà bạn tỏ ra tự hào vì doanh số của nó như vậy? lúc bạn chết nó có khóc cho bạn ko? cái điện thoại ta chỉ đánh giá qua tác dụng mà nó mang lại cho mình, thấy phù hợp với nhu cầu thì mình khen, ko nên mù quáng như vậy bạn ạ.

Mình thích chất liệu da mặt sau quá, giản dị nhưng sang trọng, quý phái : )

Thâm vcc.

Note 4 năm nay quá tuyệt, nhưng mà em hy vọng vào Note 5, năm sau nhất định sẽ có nhiều đột phá, quan trọng là lúc ấy thì em mới có đủ lúa để mua :D

18 chai mà vẫn giả da à anh sam làm nguyên khối như ip cho sang...

thích thì nhích, mai bán nhà làm con coi. Samsung đại đế

vãi điểm bechmark hơn s5 bản VN có tí xíu s5 42000 rồi

Tuyệt vời nhất của Note 4 là giảm giá nhanh , bao nhiêu chú chờ ..sẵn ...xách tay để xúc

Con này chắc ai cho mình dùng tạm thôi. To quá. Thua 1280 huyễn thoại của mình. 

hay đấy. ngắn gọn hơn

Cái này mình cũng có thông tin như bác, nên tính hỏi lại chủ thớt ☺

Đã vọc qua rất nhiều máy và hệ điều hành, mình nhận thấy điểm yếu nhất của Note 4 đó là có quá nhiều cái mới mẻ để mình tìm hiểu :D Nhất là cây bút Spen.

Bài đánh giá rất hay và thật, không như nhiều mod áp đặt ý kiến chủ quan của mình vào bài đánh giá hoặc so sánh, gây không ít war

Cập nhật thông tin chậm vậy bạn.

y như rằng con nào được anh em tinh tế khen nhiêu thì bán ế hơn bình thường, còn con nào bj chê là bán tốt hơn bình thường. thế mới thấy đời nó khác xa so với diễn đàn.
