**thời_gian dùng pin của iphone 5 so với các máy smartphone cao_cấp khác là rất tuyệt_vời và vượt trội . 
+++ Named Entities +++
pin	BATA01
iphone 5	SPAP0031
+++ Sentiment Words +++
cao_cấp	OTHP
tuyệt_vời	OTHP
++Overall sentiment: Positive

**earpod trên iphone cho âm_thanh trong và tách hơn chút so với tai_nghe đi_kèm với iphone 4s . 
+++ Named Entities +++
iphone	SPAP0000
âm_thanh	SOUA01
tai_nghe	HEPA02
iphone 4s	SPAP0021
++Overall sentiment: Unspecified

**samsung galaxy note 6 có_thể sẽ có độ dày tương_đương iphone 6 plus 
+++ Named Entities +++
samsung galaxy note 6	SPSS0121
độ dày	OTHA04
iphone 6 plus	SPAP0071
++Overall sentiment: Unspecified

**m8 chắc_chắn hơn so với m7 
+++ Named Entities +++
m8	SPHT0012
m7	SPHT0093
+++ Sentiment Words +++
chắc_chắn	OTHP
++Overall sentiment: Positive

**iphone 6 đáng chọn hơn galaxy s6 
+++ Named Entities +++
iphone 6	SPAP0061
galaxy s6	SPSS0113
++Overall sentiment: Unspecified

**trải_nghiệm m8 thì sung_sướng hơn m7 50 % 
+++ Named Entities +++
m8	SPHT0012
m7	SPHT0093
+++ Sentiment Words +++
sung_sướng	OTHP
++Overall sentiment: Positive

**tốc_độ lấy nét của m8 chưa nhanh như những gì mình được trải nghiệm trên galaxy s5 . 
+++ Named Entities +++
m8	SPHT0012
galaxy s5	SPSS0042
+++ Sentiment Words +++
nét	LCDP,LIVP
nhanh	CHSP,CPUP,GPUP,RAMP,WLAP,BLTP,AFCP
++Overall sentiment: Positive

**nắp lưng giống bb , cạnh giống ip5 , mặt trước_sau vẫn ss . 
+++ Named Entities +++
ip5	SPAP0032
++Overall sentiment: Unspecified

**phần viền của m8 cũng_được dát và đánh_bóng kim_cương tương_tự như bên m7 . 
+++ Named Entities +++
m8	SPHT0012
m7	SPHT0093
++Overall sentiment: Unspecified

**ở bốn_góc máy của m8 được bo tròn nhiều hơn so với m7 nên mình thấy m7 trông mạnh_mẽ hơn , còn m8 thì mượt_mà hơn . 
+++ Named Entities +++
m8	SPHT0012
m7	SPHT0093
m7	SPHT0093
m8	SPHT0012
+++ Sentiment Words +++
nhiều	SENP,AFMP,FCPP
mạnh_mẽ	DESP,CHSP,CPUP,GPUP
mượt_mà	OTHP
++Overall sentiment: Positive

**những chi_tiết này giúp m8 không_hề thua_kém những đối_thủ mới như sony xperia z2 , samsung galaxy s5 , oppo find 7 
+++ Named Entities +++
m8	SPHT0012
sony xperia z2	SPSO0032
samsung galaxy s5	SPSS0044
oppo find 7	SPOP0091
+++ Sentiment Words +++
thua_kém	VERN
mới	CHSP,CPUP,GPUP,SENP,BLTP
++Overall sentiment: Neutral

**tốc_độ lấy nét của m8 cũng tốt nhưng chưa nhanh như những gì mình được trải nghiệm trên galaxy s5 . 
+++ Named Entities +++
m8	SPHT0012
galaxy s5	SPSS0042
+++ Sentiment Words +++
nét	LCDP,LIVP
tốt	PRCP,WARP,PHOP,MARP,DESP,SCRP,PTTP,OSSP,SIZP
nhanh	CHSP,CPUP,GPUP,RAMP,WLAP,BLTP,AFCP
++Overall sentiment: Positive

**ko_lẽ giá cao như iphone thì bạn lại đủ ? ! 
+++ Named Entities +++
giá	PRCA01
iphone	SPAP0000
++Overall sentiment: Unspecified

**m8 muốn đạt được như sony xperia z2 thì còn xa lắm . 
+++ Named Entities +++
m8	SPHT0012
sony xperia z2	SPSO0032
++Overall sentiment: Unspecified

**ảnh không_còn ám tím như m7 
+++ Named Entities +++
ảnh	SENV007
m7	SPHT0093
++Overall sentiment: Unspecified

**mẫu điện_thoại cao_cấp tiếp_theo của lg sẽ có cấu hình phần_cứng tương_tự như chiếc g flex 2 ra_mắt cách đây không_lâu 
+++ Named Entities +++
lg	LG
cấu hình	CFG
phần_cứng	HAR
g flex 2	SPLG0052
+++ Sentiment Words +++
cao_cấp	OTHP
++Overall sentiment: Positive

**điện_thoại cũng nhỏ hơn nên bạn sẽ không bị cấn nhiều như note 4 . 
+++ Named Entities +++
note 4	SPSS0013
+++ Sentiment Words +++
nhỏ	SIZN,INTN,SIZN,LIVN
nhiều	SENP,AFMP,FCPP
++Overall sentiment: Neutral

**chưa rõ về lâu về dài lớp mạ này có bị bạc_màu đi như iphone 5 trước_kia hay không 
+++ Named Entities +++
iphone 5	SPAP0031
+++ Sentiment Words +++
rõ	SPKP,COLP
dài	DYRP,ISOP,SIZN
++Overall sentiment: Positive

**thì tay sẽ va vào viền bị đội lên không dễ_chịu như xperia z3 , iphone 6 plus hay lumia 930 . 
+++ Named Entities +++
xperia z3	SPSO0012
iphone 6 plus	SPAP0071
lumia 930	SPNM0021
+++ Sentiment Words +++
dễ_chịu	OTHP
++Overall sentiment: Positive

**có_lẽ samsung không muốn gặp vấn_đề với driver như note 3 trước_đó . 
+++ Named Entities +++
samsung	SS
note 3	SPSS0033
++Overall sentiment: Unspecified

**không chống nước như s5 , 
+++ Named Entities +++
s5	SPSS0043
++Overall sentiment: Unspecified

**cấu hình ngon hơn 530 
+++ Named Entities +++
cấu hình	CFG
530	SPNM0092
+++ Sentiment Words +++
ngon	PRCP
++Overall sentiment: Positive

**con này ram rom đều gấp_đôi 530 , giá lại rẻ hơn 200k ( con_số khá lớn vs sinh_viên ) , ko quan_trọng camera thì con này hơn đứt ( lại có cam trước ) 
+++ Named Entities +++
ram	RAM
530	SPNM0092
giá	PRCA01
camera	CMR,CMRV021
+++ Sentiment Words +++
rẻ	PRCP
lớn	INTP,RAMP,SIZP,PISP
++Overall sentiment: Positive

**cấu hình hơn_hẳn 525 
+++ Named Entities +++
cấu hình	CFG
525	SPNM0082
++Overall sentiment: Unspecified

**ko mạnh hơn con s4 của 525 dc đâu . 
+++ Named Entities +++
525	SPNM0082
+++ Sentiment Words +++
mạnh	WAVP
++Overall sentiment: Positive

**màn_hình lớn hơn và giao_diện người_dùng cũng mới . 
+++ Named Entities +++
màn_hình	SCRA02
+++ Sentiment Words +++
lớn	INTP,RAMP,SIZP,PISP
mới	CHSP,CPUP,GPUP,SENP,BLTP
++Overall sentiment: Positive

**m8 dài hơn m7 
+++ Named Entities +++
m8	SPHT0012
m7	SPHT0093
+++ Sentiment Words +++
dài	DYRP,ISOP,SIZN
++Overall sentiment: Positive

**một camera sau có độ_phân_giải cao hơn mức 20.7 mp trên các mẫu smartphone tiền_nhiệm . 
+++ Named Entities +++
camera	CMR,CMRV021
độ_phân_giải	RESA01
20.7 mp	REGEXEN
+++ Sentiment Words +++
cao	RESP,PPIP,ISOP,PRCN
++Overall sentiment: Positive

**samsung chau chuốt hơn và chứa_đựng đầy sự_bất_ngờ 
+++ Named Entities +++
samsung	SS
++Overall sentiment: Unspecified

**chiếc_máy này không_thể nói_là quá đẹp , quá xuất_sắc nhưng rõ_ràng nó tốt và nhìn cao_cấp hơn_hẳn so với note 3 hay s5 liền kề . 
+++ Named Entities +++
note 3	SPSS0033
s5	SPSS0043
+++ Sentiment Words +++
đẹp	DESP,PHOP,MARP,SCRP,COLP
xuất_sắc	SCRP
tốt	PRCP,WARP,PHOP,MARP,DESP,SCRP,PTTP,OSSP,SIZP
cao_cấp	OTHP
++Overall sentiment: Positive

**thì điểm mạnh lớn nhất của exynos 5433 là nó ít lag hơn rất nhiều 
+++ Named Entities +++
exynos 5433	CHSV035
+++ Sentiment Words +++
mạnh	WAVP
lớn	INTP,RAMP,SIZP,PISP
ít	SENN,OTCN,FLFN,AFMN,FCPN,INTN
lag	OSSN,NETN,WLAN
nhiều	SENP,AFMP,FCPP
++Overall sentiment: Positive

**đắt hơn note 3 1tr nhưng đáng_giá hơn 6,9 lần . 
+++ Named Entities +++
note 3	SPSS0033
++Overall sentiment: Unspecified

**so tầm giá và cấu hình của fonepad 8 với nokia n1 thì n1 ngon hơn hehe 
+++ Named Entities +++
giá	PRCA01
cấu hình	CFG
fonepad 8	TBAS0022,TBAS0024
nokia n1	TBNO0011
+++ Sentiment Words +++
ngon	PRCP
++Overall sentiment: Positive

**samsung đã ngầm tỏ rõ con_chip của họ không_còn lép_vế như trước_kia nữa . 
+++ Named Entities +++
samsung	SS
+++ Sentiment Words +++
rõ	SPKP,COLP
++Overall sentiment: Positive

**galaxy s5 đã không thành_công như mong_đợi 
+++ Named Entities +++
galaxy s5	SPSS0042
+++ Sentiment Words +++
thành_công	OTHP
++Overall sentiment: Positive

**đây là dòng iphone duy_nhất hiện_nay có khả_năng chống rung quang_học 
+++ Named Entities +++
iphone	SPAP0000
chống rung quang_học	CMRV011
++Overall sentiment: Unspecified

**bộ_nhớ ram của note 4 và nexus 6 tương_đương nhau , cùng 3g , lớn gấp 3 lần ram của iphone 6 plus . 
+++ Named Entities +++
bộ_nhớ	INTA02
ram	RAM
note 4	SPSS0013
nexus 6	SPMO0051
3g	OTCV018
ram	RAM
iphone 6 plus	SPAP0071
+++ Sentiment Words +++
lớn	INTP,RAMP,SIZP,PISP
++Overall sentiment: Positive

**nexus 6 bản 32gb không khóa sẽ có giá_bán lên tới 649 usd , tương_đương với iphone 6 tại thị_trường mỹ 
+++ Named Entities +++
nexus 6	SPMO0051
32gb	EXTV010,INTV004
iphone 6	SPAP0061
++Overall sentiment: Unspecified

**samsung galaxy note 6 có_thể sẽ có độ dày tương_đương iphone 6 plus 
+++ Named Entities +++
samsung galaxy note 6	SPSS0121
độ dày	OTHA04
iphone 6 plus	SPAP0071
++Overall sentiment: Unspecified

**lumia 620 có giá tương_đương lumia 520 tại anh_quốc 
+++ Named Entities +++
lumia 620	SPNM0131
giá	PRCA01
lumia 520	SPNM0121
++Overall sentiment: Unspecified

**giá bán lumia 535 tại việt_nam cũng sẽ tương_đương với thị_trường ấn_độ là 140 usd 
+++ Named Entities +++
giá	PRCA01
lumia 535	SPNM0071
++Overall sentiment: Unspecified

**có_vẻ như samsung galaxy a7 và oppo r5 có khá nhiều điểm chung ... trên thực_tế , cả_hai sản_phẩm đều có kích_thước tương_đương nhau . 
+++ Named Entities +++
samsung	SS
oppo r5	SPOP0022
kích_thước	KTHA01
+++ Sentiment Words +++
nhiều	SENP,AFMP,FCPP
++Overall sentiment: Positive

**galaxy note edge có_nhiều trò_vui để làm với phần màn_hình ... trải nghiệm tương_tự như khi xài các thiết_bị có nút chụp_ảnh cứng 
+++ Named Entities +++
galaxy note edge	SPSS0021
màn_hình	SCRA02
++Overall sentiment: Unspecified

