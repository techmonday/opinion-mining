**https : //www.tinhte.vn // threads // apple- chinh-la-nguyen-nhan-khien-cho-nexus - 6 - khong-co-cam-bien-van - tay.2419543/ 
++Overall sentiment: Unspecified

**apple chính là nguyên_nhân khiến cho nexus 6 không có cảm_biến vân_tay 
+++ Named Entities +++
apple	AP
nexus 6	SPMO0051
cảm_biến	SENA01
++Overall sentiment: Unspecified

**trong buổi phỏng_vấn với trang the telegraph , nguyên ceo motorola , dennis woodside đã thừa_nhận rằng apple chính là nguyên_nhân khiến cho nexus 6 không_được trang_bị cảm biến vân_tay . 
+++ Named Entities +++
motorola	MO
apple	AP
nexus 6	SPMO0051
cảm biến	SENA01
++Overall sentiment: Unspecified

**cụ_thể hơn , dennis cho_biết vào năm 2011 , motorola đã cùng với authentec phát_triển thành_công cảm_biến vân_tay và đưa nó lên chiếc smartphone atrix 4g lúc_đó . 
+++ Named Entities +++
motorola	MO
cảm_biến	SENA01
4g	REGEXEN
+++ Sentiment Words +++
thành_công	OTHP
++Overall sentiment: Positive

**tuy_nhiên trong năm 2012 , apple đã nhanh_chóng mua_lại authentec với giá 356 triệu usd , và tất_nhiên motorola đành_phải tìm một nhà_cung_cấp cảm biến vân_tay khác . 
+++ Named Entities +++
apple	AP
giá	PRCA01
motorola	MO
cảm biến	SENA01
+++ Sentiment Words +++
nhanh_chóng	OTHP
++Overall sentiment: Positive

**dennis nói rằng apple đã mua được nhà_cung_cấp công_nghệ nhận_diện vân_tay tốt nhất thế_giới , điều đó khiến cho những hãng làm smartphone android khác phải chật_vật trong việc tìm nhà_cung_cấp " tốt thứ_hai " . 
+++ Named Entities +++
apple	AP
nhận_diện vân_tay	SENV003
android	OSSV002
+++ Sentiment Words +++
mua	VERP
tốt	PRCP,WARP,PHOP,MARP,DESP,SCRP,PTTP,OSSP,SIZP
tốt	PRCP,WARP,PHOP,MARP,DESP,SCRP,PTTP,OSSP,SIZP
++Overall sentiment: Positive

**motorola được cho là đã tìm_ra đối_tác thích_hợp , tuy_nhiên phải chờ đến thế_hệ nexus sau ( nếu có_thể ) hoặc thế_hệ smartphone cao_cấp kế_tiếp thì họ mới đưa cảm_biến vân_tay lên máy . 
+++ Named Entities +++
motorola	MO
cảm_biến	SENA01
+++ Sentiment Words +++
cao_cấp	OTHP
mới	CHSP,CPUP,GPUP,SENP,BLTP
++Overall sentiment: Positive

**chính vì lý_do này mà dennis đã " đổ_thừa " apple là thủ_phạm khiến cho nexus 6 không_được trang_bị công_nghệ nhận_dạng vân_tay như nhiều smartphone cao_cấp hiện_nay . 
+++ Named Entities +++
apple	AP
nexus 6	SPMO0051
+++ Sentiment Words +++
nhiều	SENP,AFMP,FCPP
cao_cấp	OTHP
++Overall sentiment: Positive

**theo telegraph ​ 
++Overall sentiment: Unspecified

**tội nhể = ) ) 
++Overall sentiment: Unspecified

**nhanh_tay quá , apple đi_trước đối_thủ là do_thế . 
+++ Named Entities +++
apple	AP
++Overall sentiment: Unspecified

**buồn quá ! 
++Overall sentiment: Unspecified

**việc mình làm cứ làm chứ . 
++Overall sentiment: Unspecified

**sao lại vì apple mà ko có cảm_biến vân_tay nhỉ ? 
+++ Named Entities +++
apple	AP
cảm_biến	SENA01
++Overall sentiment: Unspecified

**apple đã chơi là phải chơi loại ngon nhất , tốt nhất . 
+++ Named Entities +++
apple	AP
+++ Sentiment Words +++
ngon	PRCP
tốt	PRCP,WARP,PHOP,MARP,DESP,SCRP,PTTP,OSSP,SIZP
++Overall sentiment: Positive

**mấy chú khác trâu chậm thì uống_nước đục thôi : d kêu_ca gì ? ? ? 
+++ Sentiment Words +++
trâu	BATP
chậm	CHSN,CPUN,GPUN,RAMN,BLTN,AFCN
++Overall sentiment: Neutral

**mình bắt_đầu nhòm_ngó tìm_hiểu về android khi có thằng em_họ dùng moto droid gì đấy . 
+++ Named Entities +++
android	OSSV002
++Overall sentiment: Unspecified

**máy chạy rất mượt_mà và pin trâu_bò kinh_khủng , máy rất manly . 
+++ Named Entities +++
pin	BATA01
+++ Sentiment Words +++
mượt_mà	OTHP
++Overall sentiment: Positive

**khác_hẳn với những trải nghiệm trên mấy chiếc samsung . 
+++ Named Entities +++
samsung	SS
++Overall sentiment: Unspecified

**mong một ngày moto quay_lại đường_đua , hiện_tại thì tạm hài_lòng với bb passport , cho những đam_mê thời sinh_viên xa_xăm . 
+++ Named Entities +++
passport	SPBB0011
+++ Sentiment Words +++
hài_lòng	OTHP,VERP
++Overall sentiment: Positive

**rồi cũng có cả thôi . 
++Overall sentiment: Unspecified

**chú làm dc truớc thì cũng bt thôi_a cũng làm đuợc chú ạ : d 
++Overall sentiment: Unspecified

**có những bài như_thế này giúp chúng_ta hiểu_rõ hơn về nguyên_nhân tại_sao những thứ " sắp thành trào_lưu " phải gián_đoạn . 
++Overall sentiment: Unspecified

**và , fan các hãng thôi cãi_nhau xem ai mới là tiên_phong đích_thực . 
+++ Sentiment Words +++
mới	CHSP,CPUP,GPUP,SENP,BLTP
++Overall sentiment: Positive

**qua bài này ta thấy , moto atrix hay apple iphone // ipad cũng là 1 gốc sinh_ra - authentic . 
+++ Named Entities +++
apple iphone	SPAP0000
++Overall sentiment: Unspecified

**sau_này liếm màn hình_phân_tích adn mở_khoá chắc_lun 
++Overall sentiment: Unspecified

**vấn_đề là công_ty cung_cấp cảm biến vân_tay tốt nhất đã bị apple mua cmn rồi . 
+++ Named Entities +++
cảm biến	SENA01
apple	AP
+++ Sentiment Words +++
tốt	PRCP,WARP,PHOP,MARP,DESP,SCRP,PTTP,OSSP,SIZP
mua	VERP
++Overall sentiment: Positive

**thế_nên bây_giờ phải đi tìm thằng tốt thứ 2 . 
+++ Sentiment Words +++
tốt	PRCP,WARP,PHOP,MARP,DESP,SCRP,PTTP,OSSP,SIZP
++Overall sentiment: Positive

**apple tiền đã nhiều lại khôn_lanh nữa : d 
+++ Named Entities +++
apple	AP
+++ Sentiment Words +++
nhiều	SENP,AFMP,FCPP
++Overall sentiment: Positive

**tiên_phong đích_thực gì thực_tế apple luôn đi sau ! 
+++ Named Entities +++
apple	AP
++Overall sentiment: Unspecified

**chỉ cảii tiến làm nó tốt_hơn ! 
++Overall sentiment: Unspecified

**không biết có lúc_nào_đó trong tương_lai không_cần đến bảo_mật để bảo_vệ điện_thoại không nhỉ . 
++Overall sentiment: Unspecified

**cứ cầm_cái nào thì cái_đó nó có dữ_liệu của mình . 
++Overall sentiment: Unspecified

**đỡ phải mất . 
++Overall sentiment: Unspecified

**không cần bảo_mật . 
++Overall sentiment: Unspecified

**ouch id xài đã ko mượt mấy thì ko biết mmý cái khác thế_nào 
+++ Sentiment Words +++
mượt	OSSP
++Overall sentiment: Positive

**đồng_tiền nào đi_trước thì nó mới khôn 
+++ Sentiment Words +++
mới	CHSP,CPUP,GPUP,SENP,BLTP
++Overall sentiment: Positive

**anh táo rất nhanh_chân và chịu_chi 
++Overall sentiment: Unspecified

**bác nên phân_biệt hoa_tiêu // trinh_sát ( pilot ) và tiên_phong ( vanguard ) . 
++Overall sentiment: Unspecified

**sau tiên_phong sẽ là trung_quân . 
++Overall sentiment: Unspecified

**apple không_phải là hãng đầu_tiên làm nhưng thường dẫn_đầu 1 lực_lượng đông_đảo , chính_thống . 
+++ Named Entities +++
apple	AP
++Overall sentiment: Unspecified

**cái điểm cộng nữa của apple là rất biết chờ thời . 
+++ Named Entities +++
apple	AP
++Overall sentiment: Unspecified

**không nắm_được tiên cơ thì rất kiên_nhẫn . 
++Overall sentiment: Unspecified

**vụ iphone màn_hình to là ví_dụ khá rõ . 
+++ Named Entities +++
iphone	SPAP0000
màn_hình	SCRA02
+++ Sentiment Words +++
to	SPKP,SIZN,SPKN,SOUN
rõ	SPKP,COLP
++Overall sentiment: Neutral

**dennis woodside vừa nhập_tịch việt_nam . 
++Overall sentiment: Unspecified

**lấy họ đổ tên thừa . 
++Overall sentiment: Unspecified

**tức_là đổ_thừa = ) ) 
++Overall sentiment: Unspecified

**chỉ_là tốt nhất khi apple mua thôi . 
+++ Named Entities +++
apple	AP
+++ Sentiment Words +++
tốt	PRCP,WARP,PHOP,MARP,DESP,SCRP,PTTP,OSSP,SIZP
mua	VERP
++Overall sentiment: Positive

**sau_khi vào tay apple thì chưa biết_thế_nào nữa . 
+++ Named Entities +++
apple	AP
++Overall sentiment: Unspecified

**siri là ví_dụ . 
++Overall sentiment: Unspecified

**bây_giờ mấy em hậu_sinh đã " qua_mặt " rồi . 
++Overall sentiment: Unspecified

**thế thì bẩn lắm : d , dùng 1 thời có_khi bệnh tiêu_hóa_suốt 
++Overall sentiment: Unspecified

**ip màn to chờ thời gì là sự_chậm_trễ của apple vì apple nghiên_cứu thị_trường thấy nhu_cầu màn_hình từ 5in tăng trửơng hơn 26 % hàng_năm thì phải . 
+++ Named Entities +++
apple	AP
apple	AP
màn_hình	SCRA02
+++ Sentiment Words +++
to	SPKP,SIZN,SPKN,SOUN
++Overall sentiment: Negative

**khi đó mới nhảy vào ! 
+++ Sentiment Words +++
mới	CHSP,CPUP,GPUP,SENP,BLTP
++Overall sentiment: Positive

**lúc_này đi ngược chiến_lược kinh_doanh của apple là thường tạo_ra thị_trường mới , nhu_cầu mới cho kh đằng_này apple lại đi_theo thị_trường ! 
+++ Named Entities +++
apple	AP
apple	AP
+++ Sentiment Words +++
mới	CHSP,CPUP,GPUP,SENP,BLTP
mới	CHSP,CPUP,GPUP,SENP,BLTP
++Overall sentiment: Positive

**sản phảm aw cũng sẽ như_vậy , ip5c là dẫn_chứng vì theo thị_trường mà apple mới tạo_ra rồi ko thành_công và đa_số các sản_phẩm đi_theo thị_trường đa_số apple đều ko thành_công ! 
+++ Named Entities +++
ip5c	SPAP0042
apple	AP
apple	AP
+++ Sentiment Words +++
mới	CHSP,CPUP,GPUP,SENP,BLTP
thành_công	OTHP
thành_công	OTHP
++Overall sentiment: Positive

**trâu chậm thì uống_nước đục thôi , đồng_tiền đi_trước là đồng_tiền khôn mà 
+++ Sentiment Words +++
trâu	BATP
chậm	CHSN,CPUN,GPUN,RAMN,BLTN,AFCN
++Overall sentiment: Neutral

**ví_dụ dễ_hiểu như tớ ... cmt chậm sau pố phải ko. . 
+++ Sentiment Words +++
chậm	CHSN,CPUN,GPUN,RAMN,BLTN,AFCN
++Overall sentiment: Negative

**hihii. . 
++Overall sentiment: Unspecified

**nhưng cmt trước chưa_chắc cmt hay_đâu 
++Overall sentiment: Unspecified

**bác này mới_hay nè. . 
++Overall sentiment: Unspecified

**nghiên_cứu cho_thấy màn_hình đt với bàn_phím máy_tính bẩn hơn_cả bồn cầu :d , bác cứ_việc liếm : cool : 
+++ Named Entities +++
màn_hình	SCRA02
bàn_phím	KBR,KBR
++Overall sentiment: Unspecified

**:d thì cũng giống như bạn đi_chợ thôi . 
++Overall sentiment: Unspecified

**chọn mua là phải mua cái_tốt nhất , ngon nhất . 
+++ Sentiment Words +++
mua	VERP
mua	VERP
ngon	PRCP
++Overall sentiment: Positive

**nhưng về chế_biến sao_cho ngon hơn với tất_cả mọi_người lại là chuyện khác : d 
+++ Sentiment Words +++
ngon	PRCP
++Overall sentiment: Positive

**không hẳn_thế . 
++Overall sentiment: Unspecified

**theo những người thạo_tin thì apple có_thể đã ra iphone màn_hình to từ năm 2012-2013 cơ vì iphone 5 lúc_đầu bán không tốt lắm . 
+++ Named Entities +++
apple	AP
iphone	SPAP0000
màn_hình	SCRA02
iphone 5	SPAP0031
+++ Sentiment Words +++
to	SPKP,SIZN,SPKN,SOUN
tốt	PRCP,WARP,PHOP,MARP,DESP,SCRP,PTTP,OSSP,SIZP
++Overall sentiment: Neutral

**tuy_nhiên , nhu_cầu iphone vẫn tăng tốt_dù màn_nhỏ . 
+++ Named Entities +++
iphone	SPAP0000
++Overall sentiment: Unspecified

**và , một_lần nữa người_ta lại thấy apple chờ_đợi là đúng_đắn hơn so với ra sớm vì nếu ra sớm thì khó khiến galaxy s khốn_đốn như bây_giờ vì galaxy s3 có quá_nhiều cái để vọc_vạch trong_khi galaxy s5 đã không có gì mới . 
+++ Named Entities +++
apple	AP
galaxy s5	SPSS0042
+++ Sentiment Words +++
khó	OTHN
mới	CHSP,CPUP,GPUP,SENP,BLTP
++Overall sentiment: Neutral

**apple watch khó_nói vì apple ngoài tạo thiết_bị ra còn tạo môi_trường cho thiết_bị ấy hoạt_động nữa . 
+++ Named Entities +++
apple	AP
apple	AP
++Overall sentiment: Unspecified

**chúng_ta mới chỉ biết apple pay ( ở mỹ ) thôi . 
+++ Named Entities +++
apple	AP
+++ Sentiment Words +++
mới	CHSP,CPUP,GPUP,SENP,BLTP
++Overall sentiment: Positive

**ngoài_ra , vẫn phải chờ . 
++Overall sentiment: Unspecified

**cảm_biến vân_tay ngon thì cũng ngon , dc cái mình ko dùng nhiều lắm , có thêm nó là phải chi 1 thêm 1 đống obama , chả vui tý nào . 
+++ Named Entities +++
cảm_biến	SENA01
+++ Sentiment Words +++
ngon	PRCP
ngon	PRCP
nhiều	SENP,AFMP,FCPP
++Overall sentiment: Positive

**quan_trọng là hợp_tác với apple có lợi hơn google . 
+++ Named Entities +++
apple	AP
++Overall sentiment: Unspecified

**số_lượng ổn_định , nhiều và đơn giá. : d 
+++ Sentiment Words +++
ổn_định	NETP,CHSP,CPUP,GPUP,OSSP,WLAP,BLTP,POWP
nhiều	SENP,AFMP,FCPP
++Overall sentiment: Positive

**tiền nhiều đúng là có lợi thật , mua luôn_cả hãng cho khỏi cạnh_tranh . 
+++ Sentiment Words +++
nhiều	SENP,AFMP,FCPP
mua	VERP
++Overall sentiment: Positive

**thế_giới đã mất đi một công_ty nhận_diện vân_tay tốt , đáng_buồn 
+++ Named Entities +++
nhận_diện vân_tay	SENV003
+++ Sentiment Words +++
tốt	PRCP,WARP,PHOP,MARP,DESP,SCRP,PTTP,OSSP,SIZP
++Overall sentiment: Positive

**cũng may là có_nhiều hãng bảo_mật vân_tay 
++Overall sentiment: Unspecified

**biết nhìn xa trông rộng thì đâu đến_nỗi 
+++ Sentiment Words +++
rộng	DYRP,LIVP
++Overall sentiment: Positive

**bác có muốn bị mất_tiền bản_quyền cho đối_thủ của mình ko , có_khi nó còn kiện cho ko bán được hàng nữa ấy chứ 
++Overall sentiment: Unspecified

**motorola đang làm rồi thì apple nhảy vô mua hết_sạch thì lấy_gì mà làm : d 
+++ Named Entities +++
motorola	MO
apple	AP
+++ Sentiment Words +++
mua	VERP
++Overall sentiment: Positive

**mình không nghĩ là cùng_nhau phát_triển , có_chăng là ông_ấy là khách_hàng đầu_tiên thôi : d 
++Overall sentiment: Unspecified

**ko xài luôn. . 
++Overall sentiment: Unspecified

**đến giờ em vẫn nghĩ siri là do vấn_đề tầm_nhìn hoặc chiến_lược nó thay_đổi chứ không nghĩ là apple không đủ khả_năng làm_cho nó tốt_hơn vì gần_như ít_thấy apple quan_tâm tới siri nhiều như khi họ giới_thiệu nữa . 
+++ Named Entities +++
apple	AP
apple	AP
+++ Sentiment Words +++
nhiều	SENP,AFMP,FCPP
++Overall sentiment: Positive

**không cần phải là người_làm đầu_tiên , chỉ cần là người_làm tốt nhất trong một thời_điểm cụ_thể nào_đó . 
+++ Sentiment Words +++
tốt	PRCP,WARP,PHOP,MARP,DESP,SCRP,PTTP,OSSP,SIZP
++Overall sentiment: Positive

**và tại thời_điểm này , apple là kẻ làm tốt nhất trong phân_khúc phone và tablet , cứ nhìn vào doanh_số bán_hàng là biết . 
+++ Named Entities +++
apple	AP
tablet	TB
+++ Sentiment Words +++
tốt	PRCP,WARP,PHOP,MARP,DESP,SCRP,PTTP,OSSP,SIZP
++Overall sentiment: Positive

**ngày_mai nó công_bố doanh_số q4.2014 rồi sẽ_biết nó còn tốt đến chừng_nào 
+++ Sentiment Words +++
tốt	PRCP,WARP,PHOP,MARP,DESP,SCRP,PTTP,OSSP,SIZP
++Overall sentiment: Positive

**ps : làm ra đầu_tiên nhưng không_biết chắc chiu , bảo_hộ bị mua_lại cũng_như không. vậy mang danh đầu_tiên trong thời_buổi cạnh_tranh khốc_liệt này để làm_chi ? 
++Overall sentiment: Unspecified

**apple luôn có cái nhìn xa rộng . 
+++ Named Entities +++
apple	AP
+++ Sentiment Words +++
rộng	DYRP,LIVP
++Overall sentiment: Positive

**nói vậy thì moto giống như người ngả giá dầu tiên , đang thương_lượng thì apple nhảy vô ra giá cao hơn , thế_là đứt , chuyện bình_thường thôi , ko mua dc thì co nguoi khác mua 
+++ Named Entities +++
giá	PRCA01
apple	AP
giá	PRCA01
+++ Sentiment Words +++
mua	VERP
mua	VERP
++Overall sentiment: Positive

**gg đâu_phải 1 lần uống_nước đục với motorola . 
+++ Named Entities +++
motorola	MO
++Overall sentiment: Unspecified

**năm 2007 uống nguyên 1 cái biển nước đục và đắng . 
++Overall sentiment: Unspecified

**số_là năm 2007 , gg định tung_ra 1 chiếc đt android phone bàn_phím cứng giống như bb và nokia . 
+++ Named Entities +++
android	OSSV002
bàn_phím	KBR,KBR
nokia	NO
++Overall sentiment: Unspecified

**nhưng khi iphone ra_mắt , gg đành âm_thầm dẹp tiệm cái đt đó đi . 
+++ Named Entities +++
iphone	SPAP0000
++Overall sentiment: Unspecified

**gần_đây mới biết_được tin này . 
+++ Sentiment Words +++
mới	CHSP,CPUP,GPUP,SENP,BLTP
++Overall sentiment: Positive

**ôm_hận trong_lòng nên gg mới đem hđh android phân_phát tứ_tung với ý_đồ sẽ nhờ tay oems tiêu_diệt iphone . 
+++ Named Entities +++
android	OSSV002
iphone	SPAP0000
+++ Sentiment Words +++
mới	CHSP,CPUP,GPUP,SENP,BLTP
++Overall sentiment: Positive

**lại ký thoả_thuận mada , xúi các oems cứ vi_phạm , nếu bị kiện gg sẽ trả_tiền . 
++Overall sentiment: Unspecified

**ss nghe kèo thơm nhảy vào nên dính chấu ... 
+++ Sentiment Words +++
thơm	PRCP
++Overall sentiment: Positive

**nói_chung là thâm_thù - đại hận = ) ) 
++Overall sentiment: Unspecified

**còn những kẻ đi sau có cải_tiến nhiều nhưng làm rắc_rối , hay_là cải lùi nữa : ) . 
+++ Sentiment Words +++
cải_tiến	VERP
nhiều	SENP,AFMP,FCPP
++Overall sentiment: Positive

**cảm biến vân_tay + nfc + ví điện_tử nó là một câu_chuyện khác đấy bác. giờ dùng cũng nhiều lắm chứ đâu_có ít 
+++ Named Entities +++
cảm biến	SENA01
+++ Sentiment Words +++
nhiều	SENP,AFMP,FCPP
ít	SENN,OTCN,FLFN,AFMN,FCPN,INTN
++Overall sentiment: Neutral

**cái_đó mình hiểu nhưng mình đang nói ở góc_độ vị_thế trên thị_trường . 
++Overall sentiment: Unspecified

**có_thể vì vào nhà apple quá lớn nên nó trở_nên bé_nhỏ . 
+++ Named Entities +++
apple	AP
+++ Sentiment Words +++
lớn	INTP,RAMP,SIZP,PISP
++Overall sentiment: Positive

**khi còn độc_lập , nó là cần_câu cơm chính của siri inc nhưng khi về với apple nó chỉ_là tuỳ_chọn . 
+++ Named Entities +++
apple	AP
++Overall sentiment: Unspecified

**tội cho anh moto ! 
++Overall sentiment: Unspecified

**sáng_tạo thì đạt mức đỉnh_cao , mỗi_tội nghèo . 
++Overall sentiment: Unspecified

**tích hợp cảm_biến vân_tay xong thì bị apple lượm mất_công_ty đối_tác . 
+++ Named Entities +++
cảm_biến	SENA01
apple	AP
++Overall sentiment: Unspecified

**ra_mắt project ara thì bị google lượm luôn : confused : 
++Overall sentiment: Unspecified

**dennis nói vậy tọii apple quá . 
+++ Named Entities +++
apple	AP
++Overall sentiment: Unspecified

**kinh_doanh thằng nào trước thì thắng thôi . 
++Overall sentiment: Unspecified

**có ý_tưởng nhưng không thực_hiện thì sẽ thua các đối_thủ cạnh_tranh đó . 
+++ Sentiment Words +++
thua	VERN
++Overall sentiment: Negative

**chỉ tại thằng hàng_xóm đi_ỉa mà em không có nước rửa_tay 
++Overall sentiment: Unspecified

**cảm biến vân_tay để mở khóa_máy trong_khi máy dễ bị jailbreak , có cũng bằng_thừa . 
+++ Named Entities +++
cảm biến	SENA01
++Overall sentiment: Unspecified

**apple nhanh_chân ghê 
+++ Named Entities +++
apple	AP
++Overall sentiment: Unspecified

**sao bọn ấy không mua note 4 để khoe của ta ? 
+++ Named Entities +++
note 4	SPSS0013
+++ Sentiment Words +++
mua	VERP
++Overall sentiment: Positive

**giá cũng ngang_hàng mà . 
+++ Named Entities +++
giá	PRCA01
++Overall sentiment: Unspecified

**vậy android đem_lên làm_gì khi bấm 1 phát máy bị root , bấm phát thứ 2 thay luôn cái rom : rolleyes : 
+++ Named Entities +++
android	OSSV002
++Overall sentiment: Unspecified

**touch id mở khóa là phụ , chính là cho apple pay . 
+++ Named Entities +++
apple	AP
++Overall sentiment: Unspecified

**mà jb thì liên_quan gì touchid : rolleyes : 
++Overall sentiment: Unspecified

**chuẩn_lun . 
++Overall sentiment: Unspecified

**việc dùng cảm_biến vân_tay trên phone đã có từ lâu , chỉ_là chưa phổ_biến và riêng mình thấy nó ... rườm_rà . 
+++ Named Entities +++
cảm_biến	SENA01
+++ Sentiment Words +++
phổ_biến	LOAP
++Overall sentiment: Positive

**làm chặt_chẽ thì hay bị chặn không vào được , dễ quá thì ko bảo_mật . 
++Overall sentiment: Unspecified

**ai đi_làm ở cty có máy check vân_tay sẽ rõ . 
+++ Sentiment Words +++
rõ	SPKP,COLP
++Overall sentiment: Positive

**tiên_cơ cái con khỉ , áp_lực người_dùng ngày_càng tăng , nên phải ra mà hình to , đúng là iphan cuồng , cái zì cũng tâng_bốc lên được hehe , tội_nghiệp wá 
+++ Sentiment Words +++
to	SPKP,SIZN,SPKN,SOUN
++Overall sentiment: Negative

**365 triệu đối_với bọn này là muỗi sao mình hợp_tác trước không mua đi đến_khi người_khác bỏ tiền ra mua_lại trách ! 
+++ Sentiment Words +++
mua	VERP
++Overall sentiment: Positive

**cứ_làm_như là 365 tỉ đô ấy ! : d dám chơi dám chịu cùng lắm không_dùng đến thì mua để_đấy khỏi thằng khác mua , không_dám giờ đổ_thừa ! = ) ) 
+++ Sentiment Words +++
mua	VERP
mua	VERP
++Overall sentiment: Positive

**cạnh_tranh mạnh được yếu thua . 
+++ Sentiment Words +++
mạnh	WAVP
yếu	WAVN,CHSN,CPUN,GPUN,BATN
thua	VERN
++Overall sentiment: Negative

**bác quả thật có nhìu kiến_thức mà với nhìu người ko biết. . 
++Overall sentiment: Unspecified

**hoặc có_khi biết nhưng ko tơi nơi tới chốn. . 
++Overall sentiment: Unspecified

**hihiii 
++Overall sentiment: Unspecified

**tinhtế bây_giờ là diễn_đàn của iphan cuồng , các thành_viên khác đã lần_lượt rời tinhtế sang diễn_đàn khác hết_rồi , vì ko chịu nổi cái sự_cuồng của iphan 
++Overall sentiment: Unspecified

**ví điện_tử thì mình lâu_lâu có xài , thực_ra dùng visa + token nhiều hơn , còn cảm_biến vân_tay + nfc ko thấy tác_dụng nhiều lắm đối_với mình , còn với người_khác thì ko rõ , có thêm 2 cái_đó chít ít cũng phải thêm cả 100 obama , với lại người việt không thấy dùng iphone để giao_dịch nhiều lắm , 10 người hết 7 8 người_mua iphone hoặc điện_thoại khác vì camera hoặc máy chạy nhanh thôi , mình sài iphone để nghe_gọi , nhạc nhẽo , còn giao_dịch làm_việc vẫn sử_dụng tablet hoặc laptop 
+++ Named Entities +++
cảm_biến	SENA01
iphone	SPAP0000
iphone	SPAP0000
camera	CMR,CMRV021
iphone	SPAP0000
tablet	TB
laptop	LT
+++ Sentiment Words +++
nhiều	SENP,AFMP,FCPP
nhiều	SENP,AFMP,FCPP
rõ	SPKP,COLP
ít	SENN,OTCN,FLFN,AFMN,FCPN,INTN
nhiều	SENP,AFMP,FCPP
nhanh	CHSP,CPUP,GPUP,RAMP,WLAP,BLTP,AFCP
++Overall sentiment: Positive

**fan hãng nào chẳng_cuồng . 
++Overall sentiment: Unspecified

**quan_trọng là nó có_đáng để người_ta bỏ tiền ra hay không thôi . 
++Overall sentiment: Unspecified

**mà_không chỉ nghệ_sỹ vn đâu . 
++Overall sentiment: Unspecified

**mình follow cả tá nghệ_sỹ tây cũng toàn tweet via iphone thôi ( ko biết có_phải 6 + không ) . 
+++ Named Entities +++
iphone	SPAP0000
++Overall sentiment: Unspecified

**có xài , mà dùng tweak vitual home thôi chứ mở khóa vẫn ko_ngon 
++Overall sentiment: Unspecified

**nghệ_sĩ ( ca_sĩ diễn_viên đạo_diễn , , , ) hollywood kiếm_tiền qua itunes rất nhiều nên dùng sp apple dễ_hiểu 
+++ Named Entities +++
apple	AP
+++ Sentiment Words +++
nhiều	SENP,AFMP,FCPP
++Overall sentiment: Positive

**đệch ... bạn có lộn không vậy . 
++Overall sentiment: Unspecified

**cảm biến vân_tay trên iphone hay ipad mang lại một trải nghiệm tuyệt_vời . 
+++ Named Entities +++
cảm biến	SENA01
iphone	SPAP0000
+++ Sentiment Words +++
tuyệt_vời	OTHP
++Overall sentiment: Positive

**bạn cứ thử mở_máy bằng vân_tay , mở một_số app bằng vân_tay thay_vì gõ gõ mấy cái ký_tự xem . 
++Overall sentiment: Unspecified

**nó khác nhiều lắm 
+++ Sentiment Words +++
nhiều	SENP,AFMP,FCPP
++Overall sentiment: Positive

**đó . 
++Overall sentiment: Unspecified

**không thừa_đâu . 
++Overall sentiment: Unspecified

**mở_máy nè , mua app nè , mở app nè ... nó chung_tiết_kiệm được khá khá thời_gian và suy_nghĩ ( bỏ cái vân_tay vào là vô_thức còn nhất mã là_phải có ý_thức nhiều lắm ) 
+++ Sentiment Words +++
mua	VERP
nhiều	SENP,AFMP,FCPP
++Overall sentiment: Positive

**samsung cũng có trang_bị loại này luôn đó , chỉ trên các máy cao_cấp thôi . 
+++ Named Entities +++
samsung	SS
+++ Sentiment Words +++
cao_cấp	OTHP
++Overall sentiment: Positive

**nhưng cái của samsung là quẹt qua chứ không_phải dạng như của apple . 
+++ Named Entities +++
samsung	SS
apple	AP
++Overall sentiment: Unspecified

**và cái của samsung tỉ lện thành_công nó không cao và hệ_sinh_thái kém . 
+++ Named Entities +++
samsung	SS
+++ Sentiment Words +++
thành_công	OTHP
cao	RESP,PPIP,ISOP,PRCN
kém	WARN,SPKN,PTTN,OSSN,SENN,WLAN,SOUN,SIZN,LIVN
++Overall sentiment: Positive

**có_lẽ bạn xài cái của samsung thấy không ngon rồi bảo cái của apple cũng không ngon . 
+++ Named Entities +++
samsung	SS
apple	AP
+++ Sentiment Words +++
ngon	PRCP
ngon	PRCP
++Overall sentiment: Positive

**may là công_nghệ swype không chịu bán cho apple , nếu bị ông_lớn này mua thì cũng bị độc_quyền rồi . 
+++ Named Entities +++
apple	AP
+++ Sentiment Words +++
mua	VERP
++Overall sentiment: Positive

**mà khi apple độc_quyền thì người_dùng bị hút_máu kinh_khủng . 
+++ Named Entities +++
apple	AP
++Overall sentiment: Unspecified

**người_nghèo không_được phổ_cập , sử_dụng những chức_năng tiên_tiến . 
++Overall sentiment: Unspecified

**https : //www . 
++Overall sentiment: Unspecified

**tinhte . 
++Overall sentiment: Unspecified

**vn // threads // apple . 
+++ Named Entities +++
apple	AP
++Overall sentiment: Unspecified

**. 
++Overall sentiment: Unspecified

**. 
++Overall sentiment: Unspecified

**ng- nghe-nhap-lieu-moi-cho - touchscreen . 
++Overall sentiment: Unspecified

**404614/ 
++Overall sentiment: Unspecified

**anh có tiền thì tội_gì không mua đồ xịn : d 
+++ Sentiment Words +++
mua	VERP
++Overall sentiment: Positive

**nghĩa là táo có_nhiều - tao có_quyền mua_lại hết 
++Overall sentiment: Unspecified

**vị_ceo này nói cũng chưa hẳn đúng , công_nghệ sinh trắc học hiện_diện ở nhiều nơi , chẳng_hạn như nhận_diện thông_qua con_ngươi của mắt , ... sao không đi_trước apple để phát_triển hệ_thống này cho trào_lưu kế_tiếp . 
+++ Named Entities +++
apple	AP
+++ Sentiment Words +++
nhiều	SENP,AFMP,FCPP
++Overall sentiment: Positive

**nhưng nói authentec là tốt nhất và không_thể tìm được công_ty tốt thứ 2 , thứ 3 nghe nó hơi khiên_cưỡng . 
+++ Sentiment Words +++
tốt	PRCP,WARP,PHOP,MARP,DESP,SCRP,PTTP,OSSP,SIZP
tốt	PRCP,WARP,PHOP,MARP,DESP,SCRP,PTTP,OSSP,SIZP
++Overall sentiment: Positive

**vì như hiện_nay ở norway và vài nước ở bắc_âu mới set up xong hệ_thống thanh_toán bằng nhận_diện vân_tay đó thôi . 
+++ Named Entities +++
nhận_diện vân_tay	SENV003
+++ Sentiment Words +++
mới	CHSP,CPUP,GPUP,SENP,BLTP
++Overall sentiment: Positive

**vn có chỗ nào hổ trợ apple pay đâu mà bác kêu người việt giao_dịch bằng cái_đó . 
+++ Named Entities +++
apple	AP
++Overall sentiment: Unspecified

**http : //www . 
++Overall sentiment: Unspecified

**networkworld . 
++Overall sentiment: Unspecified

**com // article . 
++Overall sentiment: Unspecified

**. 
++Overall sentiment: Unspecified

**. 
++Overall sentiment: Unspecified

**siri- vs-google-now-personal-assistant - ai . 
++Overall sentiment: Unspecified

**html 
++Overall sentiment: Unspecified

**http : //www.pcmag.com // article2/0,2817,2457910,00.asp 
++Overall sentiment: Unspecified

**dành cho các bạn nào muốn tham_khảo về cuộc_chiến cortana vs siri vs google now . 
++Overall sentiment: Unspecified

**nên có thông_tin experiments trước_khi kết_luận chất_lượng dịch_vụ . 
+++ Sentiment Words +++
chất_lượng	WARP
++Overall sentiment: Positive

**riêng chuyện gg now hỗ_trợ đa_ngôn_ngữ thì là chuyện bình_thường , cần_câu cơm của gg là ở số_lượng , đặc_biệt là world wide , còn  tập_trung nhiều vào us và euro zone trước giờ nên chất_lượng tập_trung về tiếng a là đương_nhiên thôi . 
+++ Sentiment Words +++
đặc_biệt	OTHP
nhiều	SENP,AFMP,FCPP
chất_lượng	WARP
++Overall sentiment: Positive

**cortana ra sau , nếu không tìm điểm khác_biệt thì 2 thằng kia nó nhấn cho chìm . 
++Overall sentiment: Unspecified

**virtual assistant hay virtual reality hiện_tại là 2 mảng con_người chỉ mới bắt_đầu , chưa biết đường_dài hay ngắn , kết_luận sớm làm_gì . 
+++ Sentiment Words +++
ngắn	DYRN,ISON
++Overall sentiment: Negative

**ai_nhanh người đó thắng thôi , có_thể apple nhìn ra vấn_đề và nhanh_tay cướp trên giàn mướp . 
+++ Named Entities +++
apple	AP
++Overall sentiment: Unspecified

**thương_trường là chiến_trường , chẳng_thể nói ai lịch_sự ai trơ_trẽn được , chỉ có_thể nói_là phải cẩn_thận và kín_kẽ . 
++Overall sentiment: Unspecified

**lúc_đó không mua luôn , sau bị apple rc 
+++ Named Entities +++
apple	AP
+++ Sentiment Words +++
mua	VERP
++Overall sentiment: Positive

**em thì do 2 người_dùng chung 1 thẻ atm nên thấy cái vụ thanh_toán bằng đt nó tiện hơn_hẳn bác ạ : d 
++Overall sentiment: Unspecified

**moto bữa_đó nghèo rồi : ) ) đâu dư_giả tài_chính như apple đâu . 
+++ Named Entities +++
apple	AP
++Overall sentiment: Unspecified

**dùng chung cảm_biến của thím sam chắc là ổn : eek : 
+++ Named Entities +++
cảm_biến	SENA01
+++ Sentiment Words +++
ổn	WARP
++Overall sentiment: Positive

**dùng nền_tảng web thôi bạn à , chứ ko dùng passcode . 
++Overall sentiment: Unspecified

**bao_giờ pay và các dịch_vụ tương_tự khác mới nở_rộ và thiết_thực đây ? ? ? 
+++ Sentiment Words +++
mới	CHSP,CPUP,GPUP,SENP,BLTP
++Overall sentiment: Positive

**lấy ví_dụ công_nghệ điều_khiển bằng giọng_nói đã có chất_lượng tương_đối cao từ cuối những năm 70 . 
+++ Named Entities +++
giọng_nói	SENV019
+++ Sentiment Words +++
chất_lượng	WARP
cao	RESP,PPIP,ISOP,PRCN
++Overall sentiment: Positive

**đến thập niên 90-2000 ibms cố_gắng tích_hợp vào pc cho end users nhưng vẫn thất_bại và nhiều cty khác cũng không thành_công . 
+++ Sentiment Words +++
nhiều	SENP,AFMP,FCPP
thành_công	OTHP
++Overall sentiment: Positive

**về fingerprint recognition cũng không ngoại_lệ . 
++Overall sentiment: Unspecified

**ứng_dụng thì đầy_rẫy , đặc_biệt là chấm_công . 
+++ Sentiment Words +++
đặc_biệt	OTHP
++Overall sentiment: Positive

**nhưng làm_được một module đủ nhỏ , đủ tiết_kiệm pin , độ_tin_cậy cao , bảo_mật cao thì lại là chuyện hoàn_toàn khác . 
+++ Named Entities +++
pin	BATA01
+++ Sentiment Words +++
nhỏ	SIZN,INTN,SIZN,LIVN
cao	RESP,PPIP,ISOP,PRCN
cao	RESP,PPIP,ISOP,PRCN
++Overall sentiment: Positive

**tại thời_điểm đó authentec thiết_kế được module tốt nhất để có_thể implement vào đt là chuyện bình_thường . 
+++ Named Entities +++
thiết_kế	DESA01
+++ Sentiment Words +++
tốt	PRCP,WARP,PHOP,MARP,DESP,SCRP,PTTP,OSSP,SIZP
++Overall sentiment: Positive

**ví_dụ hệ_thống thanh_toán bằng nhận_diện dấu vân_tay bạn nói_đến , thì database lưu ở server , còn access point chỉ làm mỗi nhiệm_vụ đọc dấu vân_tay , mã hoá rồi gởi đi . 
++Overall sentiment: Unspecified

**có_thể dùng kiểu quét , hay bất_cứ kiểu nào , vì áp_lực tốc_độ trong tình_huống này là không lớn . 
+++ Sentiment Words +++
lớn	INTP,RAMP,SIZP,PISP
++Overall sentiment: Positive

**chưa kể máy được cắm thẳng vào nguồn thì không lo về mặt hiệu_suất . 
++Overall sentiment: Unspecified

**các cty này luôn có bộ_phận đi săn công_nghệ , ngoài_ra các công_ty nắm_giữ công_nghệ mới luôn chào_hàng đến các hãng này . 
+++ Sentiment Words +++
mới	CHSP,CPUP,GPUP,SENP,BLTP
++Overall sentiment: Positive

**thế_nên chuyện họ không_biết đến công_nghệ đó là cũng khá hiếm . 
++Overall sentiment: Unspecified

**kiếng gorilla bị bỏ_quên từ năm 60s mà còn moi ra được mà . 
++Overall sentiment: Unspecified

**: d cảm_giác lúc_nào apple cũng cho người_dùng ăn món " mầm đá " . 
+++ Named Entities +++
apple	AP
++Overall sentiment: Unspecified

**apple quá nhanh. moto mất 1 tính_năng . 
+++ Named Entities +++
apple	AP
tính_năng	FUN
++Overall sentiment: Unspecified

**quá bẩn luôn , đọc mấy bài nó là sợ liền . 
++Overall sentiment: Unspecified

**ko đọc_bài hả ? 
++Overall sentiment: Unspecified

**á đậu ! 
++Overall sentiment: Unspecified

**tạm hài_lòng vs bb passport á > < 
+++ Named Entities +++
passport	SPBB0011
+++ Sentiment Words +++
hài_lòng	OTHP,VERP
++Overall sentiment: Positive

**con_đó > 14 củ mà bác nói tạm hài_lòng , chắc em chớt quá : ( 
+++ Sentiment Words +++
hài_lòng	OTHP,VERP
++Overall sentiment: Positive

**có kêu_ca cũng_thế thôi , anh chậm_chân thì anh chịu . 
++Overall sentiment: Unspecified

**mà apple cũng hay thiệt , không_cần là người đầu_tiên , nắm thời_cơ và làm nó nổi_bật trong tất_cả . 
+++ Named Entities +++
apple	AP
+++ Sentiment Words +++
nổi_bật	OTHP
++Overall sentiment: Positive

**apple bá_đạo : ) ) 
+++ Named Entities +++
apple	AP
++Overall sentiment: Unspecified

**lúc_đó chắc có khóa_miệng vì sợ bị ăn_cắp nước_bọt 
++Overall sentiment: Unspecified

