**smartphone cao_cấp của samsung với thiết_kế lạ_mắt , tính_năng và màn_hình giống galaxy note 4 dự_kiến sẽ được bán chính_thức ở việt_nam trong tháng 2 , nhưng chưa có giá . 
+++ Named Entities +++
samsung	SS
thiết_kế	DESA01
tính_năng	FUN
màn_hình	SCRA02
galaxy note 4	SPSS0012
giá	PRCA01
+++ Sentiment Words +++
cao_cấp	OTHP
++Overall sentiment: Positive

**galaxy note edge là một trong những smartphone độc_đáo nhất trên thị_trường hiện_nay với việc màn_hình được bẻ_cong và uốn ra phía mép viền của cạnh phải . 
+++ Named Entities +++
galaxy note edge	SPSS0021
màn_hình	SCRA02
++Overall sentiment: Unspecified

**thiết_kế này không_chỉ tạo cảm_giác lạ_mắt , nó cũng đem_đến cho người_dùng thêm nhiền tiện_ích sử_dụng mới. ra_mắt từ tháng 9 năm_ngoái cùng với galaxy note 4 , tuy_nhiên , note edge vẫn chưa có_mặt chính_thức tại việt_nam và xuất_hiện hạn_chế ở thị_trường " xách tay " với mức_giá khoảng 15 đến 17 triệu đồng . 
+++ Named Entities +++
thiết_kế	DESA01
galaxy note 4	SPSS0012
note edge	SPSS0022
++Overall sentiment: Unspecified

**trước_đó , có thông_tin cho_rằng việt_nam sẽ không_bán galaxy note edge chính hãng nhưng theo một_số đại_lý bán_lẻ trong nước , chiếc phablet màn_hình cong độc_đáo của samsung chuẩn_bị lên kệ hàng chính hãng , sớm nhất_là trước tết . 
+++ Named Entities +++
galaxy note edge	SPSS0021
màn_hình	SCRA02
samsung	SS
++Overall sentiment: Unspecified

**dù_vậy , chưa có giá_bán chính_thức . 
++Overall sentiment: Unspecified

**chiếc phablet độc_đáo của samsung sắp lên kệ hàng chính hãng . 
+++ Named Entities +++
samsung	SS
++Overall sentiment: Unspecified

**chiếc phablet độc_đáo của samsung sắp lên kệ hàng chính hãng . 
+++ Named Entities +++
samsung	SS
++Overall sentiment: Unspecified

**galaxy note edge là một trong những smartphone android cao_cấp nhất hiện_nay . 
+++ Named Entities +++
galaxy note edge	SPSS0021
android	OSSV002
+++ Sentiment Words +++
cao_cấp	OTHP
++Overall sentiment: Positive

**máy có cấu_hình và tính_năng tương_tự như note 4 với bút cảm_ứng s - pen thế_hệ mới , tích_hợp cảm_biến vân_tay , cảm_biến đo nhịp tim cũng_như camera 16 megapixel tích_hợp ống_kính chống rung quang_học ois . 
+++ Named Entities +++
cấu_hình	CFG
tính_năng	FUN
note 4	SPSS0013
cảm_ứng	SENA02
s - pen	OTHA01
cảm_biến	SENA01
cảm_biến	SENA01
camera	CMR,CMRV021
16 megapixel	REGEXEN
chống rung quang_học	CMRV011
ois	CMRV012
+++ Sentiment Words +++
mới	CHSP,CPUP,GPUP,SENP,BLTP
++Overall sentiment: Positive

**vi xử_lý snadragon 804 của qualcomm cùng với ram 3gb và hoạt_động trên nền android 4.4 kitkat . 
+++ Named Entities +++
ram	RAM
3gb	REGEXEN
android	OSSV002
++Overall sentiment: Unspecified

**những điểm hấp_dẫn và khác_biệt nhất trên note edge chính là màn_hình được uốn cong ở mép viền . 
+++ Named Entities +++
note edge	SPSS0022
màn_hình	SCRA02
+++ Sentiment Words +++
hấp_dẫn	OTHP
++Overall sentiment: Positive

**nó có kích_thước thực_tế 5,6 inch , nhỏ hơn một_chút so với note 4 nhưng vẫn giữ độ_phân_giải 2k siêu_nét . 
+++ Named Entities +++
kích_thước	KTHA01
5,6 inch	REGEXEN
note 4	SPSS0013
độ_phân_giải	RESA01
+++ Sentiment Words +++
nhỏ	SIZN,INTN,SIZN,LIVN
++Overall sentiment: Negative

**nếu nhìn qua , người_dùng hơi khó nhận_ra galaxy note edge khi ngoại_trừ phần màn hình cong , các chi_tiết như khung viền kim_loại và mặt_lưng vân da không khác nhiều so với galaxy note 4 . 
+++ Named Entities +++
galaxy note edge	SPSS0021
màn hình	SCRA02
galaxy note 4	SPSS0012
+++ Sentiment Words +++
khó	OTHN
nhiều	SENP,AFMP,FCPP
++Overall sentiment: Neutral

**phần viền màn_hình ở cạnh được uốn cong cho_phép người_dùng có_thể tạo các trang_trí cá_nhân riêng , hiển_thị các ứng_dụng thường_xuyên mở hay thông_tin thông_báo , tin_tức , thời_tiết ... 
+++ Named Entities +++
màn_hình	SCRA02
++Overall sentiment: Unspecified

**trong một_số ứng_dụng riêng như camera hay trò_chơi , phần màn hình cong giúp hiển_thị thanh công_cụ ... giúp không_gian chính được thoáng đãng hay không bị mất tập_trung khi có các thông_báo . 
+++ Named Entities +++
camera	CMR,CMRV021
màn hình	SCRA02
++Overall sentiment: Unspecified

**màn hình 2k công_nghệ super amoled nhưng được uốn cong về viền bên_phải . 
+++ Named Entities +++
màn hình	SCRA02
super amoled	SCRV006
++Overall sentiment: Unspecified

**màn hình 2k công_nghệ super amoled nhưng được uốn cong về viền bên_phải . 
+++ Named Entities +++
màn hình	SCRA02
super amoled	SCRV006
++Overall sentiment: Unspecified

**điểm hữu_dụng ở thiết_kế lạ này là giúp cho việc chuyển_đổi giữa các ứng_dụng thường_xuyên dùng nhanh_chóng , không bị mất thời_gian và linh_hoạt hơn dùng phím đa nhiệm thông_thường . 
+++ Named Entities +++
thiết_kế	DESA01
+++ Sentiment Words +++
nhanh_chóng	OTHP
linh_hoạt	OTHP
++Overall sentiment: Positive

**thực_tế , samsung đang mở_rộng thêm nhiều tính_năng riêng cho phần màn hình cong và phát_hành trên kho galaxy apps . 
+++ Named Entities +++
samsung	SS
tính_năng	FUN
màn hình	SCRA02
+++ Sentiment Words +++
nhiều	SENP,AFMP,FCPP
++Overall sentiment: Positive

**tuy_vậy , thiết_kế màn_hình cong lạ_mắt của note edge vẫn cần có thời_gian để người_dùng làm_quen với trải nghiệm mới_lạ này . 
+++ Named Entities +++
thiết_kế	DESA01
màn_hình	SCRA02
note edge	SPSS0022
++Overall sentiment: Unspecified

**việc dùng màn_hình cong ở viền phải khiến phím nguồn phải chuyển lên đỉnh máy và hơi khác với thao_tác thông_thường . 
+++ Named Entities +++
màn_hình	SCRA02
+++ Sentiment Words +++
đỉnh	OSSP
++Overall sentiment: Positive

**tuấn_anh 
++Overall sentiment: Unspecified

