﻿<?xml version="1.0" encoding="UTF-8"?>
<source>https://www.tinhte.vn/threads/cap-nhat-samsung-cap-nhat-phan-mem-phien-ban-v1-2-voi-nhieu-doi-moi-ve-tinh-nang-cho-chiec-nx1.2418738/</source>
<title>[Cập nhật] Samsung cập nhật phần mềm phiên bản V1.2 với nhiều đổi mới về tính năng cho chiếc NX1</title>
<main>
Samsung vừa phát hành bản cập nhật V1.2 cho dòng máy ảnh cao cấp nhất Samsung NX1. Những thay đổi đáng chú ý lần này gồm khả năng thay đổi ISO, âm lượng khi quay video, xuất time code qua HDMI và cho phép thay đổi vai trò của các phím chức năng. Bản cập nhật còn cải thiện rất nhiều về hiệu năng, tính năng. Chi tiết bản cập nhật lần này:

Về quay phim
Cho phép điều chỉnh âm lượng video khi đang quay
Cho phép điều chỉnh ISO khi đang quay
Cho phép quay Video 4K UHD và 1080p ở mức 23.98fps và 24fps
Khả năng tuỳ chỉnh ‘Chuyên nghiệp’ được thêm vào chế độ phim FullHD
Thêm nhiều tuỳ chọn hiển thị như đường kẻ ô, vùng trung tâm, tỉ lệ khung hình và khung hình an toàn
Xuất time code thông qua cổng HDMI khi dùng thiết bị thu âm ngoài
Thêm C Gamma và D Gamma curves vào chế độ quay video
Điều chỉnh màu đen sâu hơn
Mức độ sáng (Luminance) được giới hạn [0-255], [16-235], [16-255]
Khả năng điều chỉnh tốc độ lấy nét tự động (3 chế độ) để lấy nét mượt hơn
Khả năng điều chỉnh độ phản hồi của lấy nét tự động (ra lệnh cho camera thay đổi chủ thể lấy nét tự động)
Thêm công cụ để chọn và loại bỏ các khung hình của video
Thay đổi về các phím chức năng
Thêm tuỳ chọn khoá lấy nét tự động trong chế độ quay phim (nếu cài vào nút AEL)
Thay đổi AF/MF trong chế độ quay phim thông qua nút AF ON
Chức năng của nút Wi-Fi và [REC] có thể đổi cho nhau
Chức năng của nút AF On và AEL có thể đổi cho nhau
Vòng điều khiển có thể đảo ngược hướng hoạt động
Thêm ISO hoặc EV vào vòng điều khiển. Thay đổi trong chế độ PASM
Nút DoF Preview và Delete có thể thay đổi chức năng tuỳ thích
Tuỳ chọn ISO tự động (ISO, thay đổi ISO, Tốc độ màn chập tối thiểu) được gom lại thành nhóm trong menu
Các tính năng thông qua kết nối không dây
Ứng dụng trên điện thoại có thể kích chụp từ xa thông qua Bluetooth
Tính năng ‘Quick Transfer’ tự động gởi ảnh thu nhỏ vào điện thoại ngay khi chụp
Kết nối với nhiều thiết bị thông minh
Kiểm tra firmware thông qua Wifi
Các thay đổi khác
Tính năng Trap Shot: Tự động chụp khi có đối tượng di chuyển trong khung hình đã định trước
Điều khiển máy ảnh thông qua chương trình Samsung Remote Studio trên Windows
Samsung phát hành bộ SDK cho Windows để khuyến khích các phần mềm điều khiển máy ảnh khác
Tải về bản cập nhật V1.2 cho Samsung NX tại đây (kéo xuống dưới phần Download): Samsung.com

Theo Dailycameranews​
</main>
<comment>wow</comment>
<comment>Sam sung ngày càng ngon các bác ạ . Nghe đồn bên Mỹ Note 4 rất đc ưa chuộng :p</comment>
<comment>Samsung dù bị nhiều ng ko thích nhưng đã có chỗ đứng trong làng điện thoại,giờ quay sang máy ảnh với khả năng tài chính của họ có thể tương lai ko lâu dòng máy ảnh của họ cũng sẽ phổ biến như điện thoại bây giờ thôi</comment>
<comment>bình thường tun</comment>
<comment>:)</comment>
<comment>không biết chất ảnh con này ra sao đây :D có bài review chưa nhỉ?</comment>
<comment>Để xem cái MRL bự bằng DSLR này làm được gì !</comment>
<comment>Có bài review chụp thử trên tinhte rồi đó. Chất lượng ảnh ngang với ...điện thoại !</comment>
<comment>Chắc ngang với mấy cái đt dành cho người già ! Chỉ cần thấy hình là mấy người già nghĩ toàn giống nhau.</comment>
<comment>Samsung nó biết kg đấu ở mục mirroless nhỏ gọn chất lượng hình đặt lên hàng đầu nên nó bỏ qua mảng đó cho olympus fujifilm với sony nghịch
Nó biết mảng dslr to chịch của 2 đại gia kg kịch lại đc nên cũng bỏ qua
nó đi dành đất quay film lấy nét với th panasonic nhìn clip so sánh bám nét dính chủ thể mà buồn cho GH4</comment>
<comment>Mời 2 vị thưởng lãm. Nhiều người nói cỡ điện thoại, không riêng gì tại hạ
https://www.tinhte.vn/threads/tren-tay-may-anh-khong-guong-lat-samsung-nx1.2411907/</comment>
<comment>Fan sàm chính hiệu đây</comment>
<comment>Máy ảnh SS chắc là ko có cửa với 2 anh canon và nikon rồi</comment>
<comment>Máy ảnh này của SS được đánh giá rất cao về chất lượng ảnh, nhưng buồn nhất cho SS là giá cũng cao vời vợi luôn. Hệ thống ống kính còn đang vô cùng èo uột, mà giá máy lại đang du ngoạn trên mây, khó mà tiếp cận người dùng, vì với cái giá đó thì FF là đích đến lý tưởng mất rồi.</comment>
<comment>Có chứ. Cửa tử</comment>
<comment>quote nhầm rồi cưng</comment>
<comment>Chắc tại em ít dc đi đây đó,chứ em chưa bao giờ thấy ai dùng dslr hay microless của SS ngoài đời :D</comment>
<comment>point and shoot SS ngoài đời mình còn chưa thấy nữa, chỉ có trên kệ bán thôi</comment>
<comment>Samsung ngày càng tung ra nhiều sản phẩm cạnh tranh quá, mạnh thiệt!</comment>
<comment>Đây là bộ ảnh chụp bởi em nó đấy.
http://niceshot.vn/vietnam-bike-week-2014/#gallery
Nên nhớ đây là chụp mô tô đang chạy nhé. Tốc độ lấy nét tuyệt vời luôn</comment>
<comment>Từ lúc nào mod tinhte thành chuẩn đánh giá quality máy ảnh vậy? Nếu trình độ người chụp có hạn thì hình ảnh ko thể khá được. Ngay cả hình sample của DPreview dành do 7D II, A7 II cũng bị chê thậm tệ. Muốn hình đẹp lung linh thì phải chụp tốt và biết hậu kỳ. Hình sample kiểu trên tay thì thường là ko có cả 2 điều đó.</comment>
<comment>Đúng rồi rất nhiều dản phẩm cạnh tranh mạnh và bán rất chạy VD như: siêu phẩm gear hay Galaxy V.... Galaxy Zoom, camera..., hay Galaxy dòng A với giá quá rẻ, rẻ hơn giá thành SX nữa.......</comment>
<comment>Cứ sx và quảng cáo mạnh, thế nào cũng có bán dc một ít, đủ vốn là dc rồi.</comment>
<comment>Con này QC cho nhiều chứ ra tiệm chính hãng cũng không ai bán cho đâu (nếu có nhu cầu mua thật sự)</comment>
<comment>Hình hậu kì thì không còn là sản phẩm 100% của máy ảnh nữa. Chính những bức hình chụp đại, qua loa mới lộ hết ưu nhược điểm của máy ra đó . Review mà đưa ảnh trau chuốt hậu kì thì để làm gì ??</comment>
<comment>Đồng ý là hình test phải là gốc ko hậu kỳ.nhưng ko có nghĩa là bấm đại cho ra cái hình
Nểu cùng 1 góc chụp và cách bấm y chang mấy tấm chụp bằng NX1 kia thì kể cả 1DX cũng chỉ khác xí xi về DR và Dof thôi,còn lại cũng chỉ như ĐT tăng saturation ạ</comment>
<comment>VN trọng thương hiệu nên vậy thôi,tầm 5-7 tr đầy rẫy ng chọn mua e pns Canon,Nikon,Sony với cái cảm biến 1/2,3 chứ quyết ko mua mirroless SS với cảm biến Aps-c
Thiếu hiểu biết nhưng lại cuồng thương hiệu chỉ có ở VN

Còn như NX1 thì bị chê là phải,như 7d mk2 dân tình còn chê ầm ầm chỉ vì ko ai nhận ra cái đáng tiền ở đâu mà toàn so với FF entry như 6d

Tóm lại dân VN cái gì ko biết dùng hoặc ko có nhu cầu đều bảo dở và chê</comment>
<comment>Nói như bác thì đem đốt bỏ mấy cái FF DSLR cho rồi. Khác có xí xi :D</comment>
<comment>Bác đọc kỹ,cùng góc cùng setting và cách bấm với mấy tấm chụp test NX1 kia kìa

Manh động quá bác ơi,ý mình trong cùng hoàn cảnh đó FF đầu bảng cỡ 1Dx có cho hình khá hơn hay ko hay vẫn như đt chụp như ý cuả bác

Mình đâu có điên mà bảo FF khác xí xi Crop,nhưng trong hoàn cảnh nêu trên khác xí xí là đúng á</comment>
<comment>Bớt seeder đi thím. Nếu Point & shoot SS thì nước ngoài xài nhiều, r dân chơi ảnh Vn cũng đu theo. Cơ bản là nó ko tốt nên ko ai thèm xài. Chứ đừg vơ đũa VN trọng thương hiệu.</comment>
<comment>Ko ai thèm sài hay ko dám sài vị sợ ng ta chê hai chữ samsung....</comment>
<comment>Thì cùng góc máy, cùng setting mà flagship 1DX, D4 chụp ra hình cũng bệt, chi tiết kém, tương phản kém, DR tệ hại ngang ngủa NX1 thì đập máy phải rồi</comment>
<comment>Chụp như đt sao ở đây người ta đánh giá chất lượng ảnh 5/5 vậy ? Chắc mấy ng này bị khùng hết. Chê thì cũng vừa thôi nói cho cố mạng làm như cả đám SS ngu hết làm cái máy chụp thua đt HÀI
http://www.photographyblog.com/reviews/samsung_nx1_review/</comment>
<comment>Apple và sony diểm 10 cho chất lượng...nói ko với hàng ss.lg .htc...khựa và đồng bọn (việt nam)</comment>
<comment>Đừng nghe những gì quảng cáo nói :D</comment>
<comment>lại thèm, lại thích phượt, chụp choẹt,</comment>
<comment>Đã nói là bớt vơ đũa đi. Thế ôg có đang xài máy ảnh SS ko ?</comment>
<comment>Có sài đây bác,để lâu lâu chụp gd cho tiện và thường xuyên bỏ cốp xe để lúc cần lôi ra chụp chứ ko phải lúc nào cũng phải đeo balo bên mình</comment>

