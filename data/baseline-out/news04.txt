**đánh_giá samsung galaxy note 4 : khi ông lớn xứ hàn_đáp trả ! 
+++ Named Entities +++
samsung galaxy note 4	SPSS0013
+++ Sentiment Words +++
lớn	INTP,RAMP,SIZP,PISP
++Overall sentiment: Positive

**image - 1412096174 - image - 1412092823 - samsung-galaxy-note - 4 - techz - 28 [ 1 ] 
++Overall sentiment: Unspecified

**ttcn.vn - 
++Overall sentiment: Unspecified

**trong năm nay , samsung lại tiếp_tục ‘ gây bão ’ với phiên_bản note thế_hệ mới . 
+++ Named Entities +++
samsung	SS
+++ Sentiment Words +++
mới	CHSP,CPUP,GPUP,SENP,BLTP
++Overall sentiment: Positive

**không chỉ trang_bị cấu hình mạnh_mẽ bậc_nhất hiện_nay , thiết_kế của galaxy note 4 cũng đã ‘ thoát_xác ’ ngoạn_mục . 
+++ Named Entities +++
cấu hình	CFG	Positive: mạnh_mẽ
thiết_kế	DESA01
galaxy note 4	SPSS0012
+++ Sentiment Words +++
mạnh_mẽ	DESP,CHSP,CPUP,GPUP
++Overall sentiment: Positive

**dòng sản_phẩm galaxy note đã tạo_ra một xu_hướng mới với thiết_kế màn_hình lớn từ những năm về trước . 
+++ Named Entities +++
thiết_kế	DESA01
màn_hình	SCRA02	Positive: lớn
+++ Sentiment Words +++
mới	CHSP,CPUP,GPUP,SENP,BLTP
lớn	INTP,RAMP,SIZP,PISP
++Overall sentiment: Positive

**không chỉ đơn_thuần là một thiết_kế lớn , ông ‘ lớn ’ đến từ hàn_quốc còn thổi vào đó rất nhiều những tính_năng hay_ho , hữu_ích nhằm giúp người_dùng dễ_dàng hơn trong thao_tác , đặc_biệt khi sử_dụng với bút s pen . 
+++ Named Entities +++
thiết_kế	DESA01	Positive: lớn
tính_năng	FUN	Positive: hữu_ích
s pen	OTHA03
+++ Sentiment Words +++
lớn	INTP,RAMP,SIZP,PISP
lớn	INTP,RAMP,SIZP,PISP
nhiều	SENP,AFMP,FCPP
hữu_ích	OTHP
dễ_dàng	OTHP
đặc_biệt	OTHP
++Overall sentiment: Positive

**image 1412092831 samsung galaxy note 4 techz 01 đánh_giá samsung galaxy note 4 : khi ông lớn xứ hàn_đáp trả ! 
+++ Named Entities +++
samsung galaxy note 4	SPSS0013
samsung galaxy note 4	SPSS0013
+++ Sentiment Words +++
lớn	INTP,RAMP,SIZP,PISP
++Overall sentiment: Positive

**samsung galaxy note 4 với nhiều cải_tiến cả về thiết_kế lẫn cấu hình . 
+++ Named Entities +++
samsung galaxy note 4	SPSS0013	Positive: nhiều
thiết_kế	DESA01
cấu hình	CFG
+++ Sentiment Words +++
nhiều	SENP,AFMP,FCPP
cải_tiến	VERP
++Overall sentiment: Positive

**ảnh : hồng_long 
+++ Named Entities +++
ảnh	SENV007
++Overall sentiment: Unspecified

**cũng đã có rất nhiều nhà_sản_xuất thử ‘ dấn_thân ’ vào phát_triển mẫu smartphone màn_hình lớn như lg với lg optimus g pro , sony xperia z ultra , htc one max tuy_nhiên hiệu_ứng khách_hàng và doanh_thu các mẫu sản_phẩm trên không_thể vượt_qua cái bóng lớn của dòng note . 
+++ Named Entities +++
màn_hình	SCRA02	Positive: lớn
lg	LG
lg	LG
sony xperia z ultra	SPSO0081
htc one max	SPHT0071
+++ Sentiment Words +++
nhiều	SENP,AFMP,FCPP
lớn	INTP,RAMP,SIZP,PISP
bóng	VOMP
lớn	INTP,RAMP,SIZP,PISP
++Overall sentiment: Positive

**và trong lần ra_quân này , samsung tiếp_tục ‘ khuấy đảo ’ thị_trường phablet với một note 4 cứng hơn , thiết_kế đẹp và sang_trọng hơn . 
+++ Named Entities +++
samsung	SS
note 4	SPSS0013
thiết_kế	DESA01	Positive: đẹp
+++ Sentiment Words +++
đẹp	DESP,PHOP,MARP,SCRP,COLP
sang_trọng	SCRP
++Overall sentiment: Positive

**còn bây_giờ , mời bạn cùng đến với bài đánh_giá mẫu sản_phẩm này . 
++Overall sentiment: Unspecified

**thiết_kế 
+++ Named Entities +++
thiết_kế	DESA01
++Overall sentiment: Unspecified

**image 1412092883 samsung galaxy note 4 techz 11 đánh_giá samsung galaxy note 4 : khi ông lớn xứ hàn_đáp trả ! 
+++ Named Entities +++
samsung galaxy note 4	SPSS0013
samsung galaxy note 4	SPSS0013
+++ Sentiment Words +++
lớn	INTP,RAMP,SIZP,PISP
++Overall sentiment: Positive

**samsung galaxy note 4 sở_hữu kiểu dáng vuông_vắn hơn so với các phiên_bản tiền_nhiệm . 
+++ Named Entities +++
samsung galaxy note 4	SPSS0013
++Overall sentiment: Unspecified

**ảnh : hồng_long 
+++ Named Entities +++
ảnh	SENV007
++Overall sentiment: Unspecified

**nếu bạn đã từng có dịp cầm trên tay mẫu galaxy alpha , chắc_hẳn bạn sẽ cho_rằng galaxy note 4 một phiên_bản ‘ phóng to ’ của thiết_bị này , với khung viền nhôm vát kim_cương rất đẹp và sắc_nét . 
+++ Named Entities +++
galaxy alpha	SPSS0062
galaxy note 4	SPSS0012
+++ Sentiment Words +++
to	SPKP,SIZN,SPKN,SOUN
đẹp	DESP,PHOP,MARP,SCRP,COLP
++Overall sentiment: Positive

**bên_cạnh_đó , samsung cũng làm máy vuông_vức hơn so với các bậc tiền_nhiệm của mình , chính vì điều đó kết_hợp cùng yếu_tố chất_liệu thiết_kế mới , note 4 khó ‘ thuần phục ’ hơn so với iphone 6 plus . 
+++ Named Entities +++
samsung	SS
chất_liệu	MARA01
thiết_kế	DESA01	Positive: mới
note 4	SPSS0013	Negative: khó
iphone 6 plus	SPAP0071
+++ Sentiment Words +++
mới	CHSP,CPUP,GPUP,SENP,BLTP
khó	OTHN
++Overall sentiment: Neutral

**nhưng bù_lại bạn sẽ cảm_thấy được sự_cứng_cáp và chắc_chắn khi cầm_nắm . 
++Overall sentiment: Unspecified

**các chi_tiết còn_lại đều được samsung ‘ trau_chuốt ’ khá cẩn_thận và tỉ_mỉ như các phím tăng_giảm âm_lượng , phím nguồn , lỗ micro , jack cắm tai_nghe . 
+++ Named Entities +++
samsung	SS	Positive: trau_chuốt
tai_nghe	HEPA02
+++ Sentiment Words +++
trau_chuốt	VERP
++Overall sentiment: Positive

**image 1412093014 samsung galaxy note 4 techz 37 đánh_giá samsung galaxy note 4 : khi ông lớn xứ hàn_đáp trả ! 
+++ Named Entities +++
samsung galaxy note 4	SPSS0013
samsung galaxy note 4	SPSS0013
+++ Sentiment Words +++
lớn	INTP,RAMP,SIZP,PISP
++Overall sentiment: Positive

**samsung galaxy note 4 và iphone 6 plus là 2 đối_thủ ‘ nặng_ký ’ vào thời_điểm hiện_tại . 
+++ Named Entities +++
samsung galaxy note 4	SPSS0013
iphone 6 plus	SPAP0071
++Overall sentiment: Unspecified

**ảnh : hồng_long 
+++ Named Entities +++
ảnh	SENV007
++Overall sentiment: Unspecified

**samsung galaxy note 4 đã thực_sự ‘ thoát_xác ’ với chất_liệu khung nhôm rắn_chắc , sắc_sảo và sang_trọng 
+++ Named Entities +++
samsung galaxy note 4	SPSS0013
chất_liệu	MARA01
+++ Sentiment Words +++
sắc_sảo	DESP
sang_trọng	SCRP
++Overall sentiment: Positive

**cũng giống như iphone 6 plus , samsung đã gia_cố thêm cho phần màn_hình được làm_nổi hơn chút , và nó được bo dần xuống các cạnh của máy , tuy_nhiên khu_vực tiếp_xúc giữa màn_hình và cạnh_máy vẫn chưa thực_sự liền_lạc lắm , tạo_ra các khoảng hở trông khá khó_chịu . 
+++ Named Entities +++
iphone 6 plus	SPAP0071
samsung	SS
màn_hình	SCRA02
màn_hình	SCRA02
+++ Sentiment Words +++
khó_chịu	OTHN
++Overall sentiment: Negative

**màn hình máy được samsung cách_điệu chút_ít hơn với các vạch 3d li_ti nằm_ngang chứ không hoàn_toàn trống trơn như đối_thủ ‘ sừng_sỏ ’ 6 plus . 
+++ Named Entities +++
màn hình	SCRA02
samsung	SS
3d	SOUV003
6 plus	SPAP0073
++Overall sentiment: Unspecified

**image 1412092835 samsung galaxy note 4 techz 02 đánh_giá samsung galaxy note 4 : khi ông lớn xứ hàn_đáp trả ! 
+++ Named Entities +++
samsung galaxy note 4	SPSS0013
samsung galaxy note 4	SPSS0013
+++ Sentiment Words +++
lớn	INTP,RAMP,SIZP,PISP
++Overall sentiment: Positive

**cạnh phải là phím_nguồn 
++Overall sentiment: Unspecified

**image 1412092839 samsung galaxy note 4 techz 03 đánh_giá samsung galaxy note 4 : khi ông lớn xứ hàn_đáp trả ! 
+++ Named Entities +++
samsung galaxy note 4	SPSS0013
samsung galaxy note 4	SPSS0013
+++ Sentiment Words +++
lớn	INTP,RAMP,SIZP,PISP
++Overall sentiment: Positive

**khung nhôm note 4 được vát kim_cương rất sắc_sảo và rắn_chắc 
+++ Named Entities +++
note 4	SPSS0013
+++ Sentiment Words +++
sắc_sảo	DESP
++Overall sentiment: Positive

**image 1412092846 samsung galaxy note 4 techz 05 đánh_giá samsung galaxy note 4 : khi ông lớn xứ hàn_đáp trả ! 
+++ Named Entities +++
samsung galaxy note 4	SPSS0013
samsung galaxy note 4	SPSS0013
+++ Sentiment Words +++
lớn	INTP,RAMP,SIZP,PISP
++Overall sentiment: Positive

**cạnh trái là phím tăng_giảm âm_lượng 
++Overall sentiment: Unspecified

**image 1412092858 samsung galaxy note 4 techz 06 đánh_giá samsung galaxy note 4 : khi ông lớn xứ hàn_đáp trả ! 
+++ Named Entities +++
samsung galaxy note 4	SPSS0013
samsung galaxy note 4	SPSS0013
+++ Sentiment Words +++
lớn	INTP,RAMP,SIZP,PISP
++Overall sentiment: Positive

**đỉnh_đầu là jack cắm tai_nghe , cổng hồng_ngoại và lỗ_mic 
+++ Named Entities +++
tai_nghe	HEPA02
hồng_ngoại	SENV008
++Overall sentiment: Unspecified

**image 1412092845 samsung galaxy note 4 techz 04 đánh_giá samsung galaxy note 4 : khi ông lớn xứ hàn_đáp trả ! 
+++ Named Entities +++
samsung galaxy note 4	SPSS0013
samsung galaxy note 4	SPSS0013
+++ Sentiment Words +++
lớn	INTP,RAMP,SIZP,PISP
++Overall sentiment: Positive

**cạnh dưới là cổng microusb , lỗ_mic và bút s - pen 
+++ Named Entities +++
s - pen	OTHA01
++Overall sentiment: Unspecified

**image 1412092862 samsung galaxy note 4 techz 07 đánh_giá samsung galaxy note 4 : khi ông lớn xứ hàn_đáp trả ! 
+++ Named Entities +++
samsung galaxy note 4	SPSS0013
samsung galaxy note 4	SPSS0013
+++ Sentiment Words +++
lớn	INTP,RAMP,SIZP,PISP
++Overall sentiment: Positive

**mặt_sau với cụm camera được thiết_kế hơi nhô lên và bộ_phận cảm_biến nhịp_tim 
+++ Named Entities +++
camera	CMR,CMRV021
thiết_kế	DESA01
cảm_biến	SENA01
nhịp_tim	SENV013
++Overall sentiment: Unspecified

**image 1412092873 samsung galaxy note 4 techz 09 đánh_giá samsung galaxy note 4 : khi ông lớn xứ hàn_đáp trả ! 
+++ Named Entities +++
samsung galaxy note 4	SPSS0013
samsung galaxy note 4	SPSS0013
+++ Sentiment Words +++
lớn	INTP,RAMP,SIZP,PISP
++Overall sentiment: Positive

**phím home với khả_năng nhận_diện vân_tay . 
+++ Named Entities +++
nhận_diện vân_tay	SENV003
++Overall sentiment: Unspecified

**ảnh : hồng_long 
+++ Named Entities +++
ảnh	SENV007
++Overall sentiment: Unspecified

**image 1412092920 samsung galaxy note 4 techz 19 đánh_giá samsung galaxy note 4 : khi ông lớn xứ hàn_đáp trả ! 
+++ Named Entities +++
samsung galaxy note 4	SPSS0013
samsung galaxy note 4	SPSS0013
+++ Sentiment Words +++
lớn	INTP,RAMP,SIZP,PISP
++Overall sentiment: Positive

**thiết_kế tổng_thể của galaxy note 4. ảnh : hồng_long 
+++ Named Entities +++
thiết_kế	DESA01
ảnh	SENV007
++Overall sentiment: Unspecified

**thừa_kế những tinh_hoa từ note 3 , note 4 vẫn có khả_năng tháo nắp vỏ sau để người_dùng có_thể dễ_dàng thay_thế pin . 
+++ Named Entities +++
note 3	SPSS0033
note 4	SPSS0013
vỏ	VOMA01
pin	BATA01
+++ Sentiment Words +++
dễ_dàng	OTHP
++Overall sentiment: Positive

**nắp vỏ sau cũng_được làm từ chất_liệu nhựa giả_da được thiết_kế theo kiểu vân mềm_mại , so với người anh_em note 3 mặt_lưng sau có_vẻ trơn hơn và không bám tay bằng . 
+++ Named Entities +++
vỏ	VOMA01
chất_liệu	MARA01
nhựa	PTTV007
thiết_kế	DESA01
note 3	SPSS0033
+++ Sentiment Words +++
mềm_mại	DESP
++Overall sentiment: Positive

**kế_đến là cụm camera lồi , đèn flashled , bộ phân đo nhịp tim , logo và khe loa , đây cũng là kiểu_cách bố_trí truyền_thống của samsung . 
+++ Named Entities +++
camera	CMR,CMRV021
loa	SPKA01
samsung	SS
++Overall sentiment: Unspecified

**màn_hình 
+++ Named Entities +++
màn_hình	SCRA02
++Overall sentiment: Unspecified

**image 1412092887 samsung galaxy note 4 techz 12 đánh_giá samsung galaxy note 4 : khi ông lớn xứ hàn_đáp trả ! 
+++ Named Entities +++
samsung galaxy note 4	SPSS0013
samsung galaxy note 4	SPSS0013
+++ Sentiment Words +++
lớn	INTP,RAMP,SIZP,PISP
++Overall sentiment: Positive

**màn hình 5.7 inch với độ_phân_giải 2k cực_kỳ sắc_nét . 
+++ Named Entities +++
màn hình	SCRA02
5.7 inch	REGEXENT
độ_phân_giải	RESA01
++Overall sentiment: Unspecified

**ảnh : hồng_long 
+++ Named Entities +++
ảnh	SENV007
++Overall sentiment: Unspecified

**có_vẻ như kích_thước 5.7 inch là quá đủ cho một mẫu di_động lai máy_tính bảng , chính vì_thế hãng không nâng kích_thước của note 4 mà thay vào đó nó được nhồi_nhét một thông_số cấu hình cực_kỳ vượt trội . 
+++ Named Entities +++
kích_thước	KTHA01
5.7 inch	REGEXENT
kích_thước	KTHA01
note 4	SPSS0013
cấu hình	CFG
++Overall sentiment: Unspecified

**và màn_hình cũng không ngoại_lệ , note 4 trang_bị màn_hình super amoled với độ_phân_giải lên_đến quad hd ( 2560 × 1440 pixels ) , điều này đã cung_cấp cho note 4 mật_độ điểm ảnh ‘ đáng kinh_ngạc ’ 500 pixel trên mỗi inch . 
+++ Named Entities +++
màn_hình	SCRA02
note 4	SPSS0013
màn_hình	SCRA02
super amoled	SCRV006
độ_phân_giải	RESA01
quad hd	RESV014
note 4	SPSS0013
mật_độ điểm ảnh	PPIA03
500 pixel	REGEXENT
++Overall sentiment: Unspecified

**image 1412092909 samsung galaxy note 4 techz 16 đánh_giá samsung galaxy note 4 : khi ông lớn xứ hàn_đáp trả ! 
+++ Named Entities +++
samsung galaxy note 4	SPSS0013
samsung galaxy note 4	SPSS0013
+++ Sentiment Words +++
lớn	INTP,RAMP,SIZP,PISP
++Overall sentiment: Positive

**màn hình note 4 có độ bão_hoà màu nhẹ hơn so với note 3 . 
+++ Named Entities +++
màn hình	SCRA02
note 4	SPSS0013
note 3	SPSS0033
+++ Sentiment Words +++
nhẹ	OSSP
++Overall sentiment: Positive

**ảnh : hồng_long 
+++ Named Entities +++
ảnh	SENV007
++Overall sentiment: Unspecified

**màn hình hiển_thị galaxy note 4 ít bị bão_hoà hơn , hiển_thị rõ nét , sống_động và thể_hiện được độ_sâu của hình_ảnh 
+++ Named Entities +++
màn hình	SCRA02
galaxy note 4	SPSS0012	Negative: ít
+++ Sentiment Words +++
ít	SENN,OTCN,FLFN,AFMN,FCPN,INTN
rõ	SPKP,COLP
nét	LCDP,LIVP
++Overall sentiment: Positive

**để có những trải nghiệm_thực_tế , techz đã thử_tải một video với chất_lượng uhd . 
+++ Sentiment Words +++
chất_lượng	WARP
++Overall sentiment: Positive

**quả_thực galaxy note 4 không làm chúng_ta thất_vọng với màu_sắc hiển_thị sống_động , chi_tiết cực_kỳ rõ_ràng và rất sâu . 
+++ Named Entities +++
galaxy note 4	SPSS0012
màu_sắc	COLA01
+++ Sentiment Words +++
thất_vọng	OTHN
++Overall sentiment: Negative

**ngay cả khi bạn phóng to cũng không xuất_hiện hiện_trạng răng_cưa , vỡ_hạt , mà ngược_lại nó rất mượt_mà và không xảy_ra hiện_trạng giật hay lag khung hình nào . 
+++ Sentiment Words +++
to	SPKP,SIZN,SPKN,SOUN
mượt_mà	OTHP
lag	OSSN,NETN,WLAN
++Overall sentiment: Neutral

**khi được so_sánh với phiên_bản tiền_nhiệm galaxy note 3 , độ bão_hoà màu của note 4 cũng nhẹ hơn một_chút , đồng_thời , giảm phần_nào hiện_tượng ám xanh thường thấy trên màn_hình amoled . 
+++ Named Entities +++
galaxy note 3	SPSS0032
note 4	SPSS0013	Positive: nhẹ
màn_hình	SCRA02
amoled	SCRV005
+++ Sentiment Words +++
nhẹ	OSSP
++Overall sentiment: Positive

**hiệu_năng và thời_lượng pin 
+++ Named Entities +++
pin	BATA01
++Overall sentiment: Unspecified

**image 1412092925 samsung galaxy note 4 techz 20 đánh_giá samsung galaxy note 4 : khi ông lớn xứ hàn_đáp trả ! 
+++ Named Entities +++
samsung galaxy note 4	SPSS0013
samsung galaxy note 4	SPSS0013
+++ Sentiment Words +++
lớn	INTP,RAMP,SIZP,PISP
++Overall sentiment: Positive

**cấu_hình galaxy note 4 có_thể nói là mạnh_mẽ bậc_nhất hiện_nay . 
+++ Named Entities +++
cấu_hình	CFG
galaxy note 4	SPSS0012
+++ Sentiment Words +++
mạnh_mẽ	DESP,CHSP,CPUP,GPUP
++Overall sentiment: Positive

**ảnh : hồng_long 
+++ Named Entities +++
ảnh	SENV007
++Overall sentiment: Unspecified

**như đã đề_cập , galaxy note 4 sở_hữu cấu hình khủng bậc_nhất hiện_nay với chip xử_lý qualcomm snapdragon 805 mạnh_mẽ ( phiên_bản dành cho thị_trường mỹ ) , và loại vi xử_lý ‘ cây nhà_lá vườn ’ exynos 5433 8 nhân , trong_đó có 4 nhân cortex a57 tốc_độ 1.9ghz và 4 nhân cortex a53 1.3ghz ( phiên_bản quốc_tế ) . 
+++ Named Entities +++
galaxy note 4	SPSS0012
cấu hình	CFG
chip xử_lý	CHSA05
qualcomm snapdragon	CHSV001	Positive: mạnh_mẽ
exynos	CHSV021
8 nhân	CPUV018
4 nhân	CPUV014
1.9ghz	REGEXENT
4 nhân	CPUV014
1.3ghz	REGEXENT
+++ Sentiment Words +++
mạnh_mẽ	DESP,CHSP,CPUP,GPUP
++Overall sentiment: Positive

**với thông_số cấu hình này , bạn sẽ không_phải e_ngại khi chạy bất_cứ ứng_dụng nào , thậm_chí truy_xuất rất nhanh . 
+++ Named Entities +++
cấu hình	CFG
+++ Sentiment Words +++
nhanh	CHSP,CPUP,GPUP,RAMP,WLAP,BLTP,AFCP
++Overall sentiment: Positive

**tuy_nhiên máy vẫn xuất_hiện độ trễ khi thoát các ứng_dụng nặng , đây là điều mà hầu_hết các thiết_bị android đều gặp_phải . 
+++ Named Entities +++
android	OSSV002
++Overall sentiment: Unspecified

**với cấu hình mạnh_mẽ bậc_nhất hiện_nay , galaxy note 4 thực_thi khá nhanh nhậy và không vướng phải trở_ngại lớn nào khi chạy các ứng_dụng nặng 
+++ Named Entities +++
cấu hình	CFG	Positive: mạnh_mẽ
galaxy note 4	SPSS0012	Negative: nhanh
+++ Sentiment Words +++
mạnh_mẽ	DESP,CHSP,CPUP,GPUP
nhanh	CHSP,CPUP,GPUP,RAMP,WLAP,BLTP,AFCP
vướng	OTHN
lớn	INTP,RAMP,SIZP,PISP
++Overall sentiment: Positive

**mặc_dù với màn_hình ‘ cực nét ’ , song note 4 vẫn chỉ được cung_cấp mức dung_lượng pin 3.220mah . 
+++ Named Entities +++
màn_hình	SCRA02	Positive: nét
note 4	SPSS0013
dung_lượng	CPCA01
pin	BATA01
3.220mah	REGEXENT
+++ Sentiment Words +++
nét	LCDP,LIVP
++Overall sentiment: Positive

**để bù_đắp khoảng này , samsung trang_bị tính_năng ‘ siêu tiết_kiệm pin ’ lấy từ galaxy s5 . 
+++ Named Entities +++
samsung	SS
tính_năng	FUN
pin	BATA01
galaxy s5	SPSS0042
++Overall sentiment: Unspecified

**chỉ còn 10 % , nhưng máy vẫn trụ được 1 ngày . 
++Overall sentiment: Unspecified

**bên_cạnh_đó , note 4 còn có khả_năng sạc cực nhanh 50 % chỉ trong nửa giờ . 
+++ Named Entities +++
note 4	SPSS0013
+++ Sentiment Words +++
nhanh	CHSP,CPUP,GPUP,RAMP,WLAP,BLTP,AFCP
++Overall sentiment: Positive

**image 1412092916 samsung galaxy note 4 techz 18 đánh_giá samsung galaxy note 4 : khi ông lớn xứ hàn_đáp trả ! 
+++ Named Entities +++
samsung galaxy note 4	SPSS0013
samsung galaxy note 4	SPSS0013
+++ Sentiment Words +++
lớn	INTP,RAMP,SIZP,PISP
++Overall sentiment: Positive

**thời_lượng pin note 4 có_thể trụ đến 2 ngày với những tác_vụ thông_thường . 
+++ Named Entities +++
pin	BATA01
note 4	SPSS0013
++Overall sentiment: Unspecified

**ảnh : hồng_long 
+++ Named Entities +++
ảnh	SENV007
++Overall sentiment: Unspecified

**khi trải nghiệm_thực_tế , máy có_thể hoạt_động với những thao_tác nặng như trong_vòng một ngày như lướt web và mạng xã_hội liên_tục với wifi , chụp_hình . 
+++ Named Entities +++
mạng	NETA01
wifi	WLAA01
++Overall sentiment: Unspecified

**còn khi sử_dụng với mức dùng thông_thường , note 4 có_thể sống_sót được hơn 2 ngày . 
+++ Named Entities +++
note 4	SPSS0013
++Overall sentiment: Unspecified

**thời_lượng này rất đáng để chúng_ta hoan_nghênh , khi note 4 phải chịu áp_lực lớn từ màn_hình có độ_phân_giải cao 2k . 
+++ Named Entities +++
note 4	SPSS0013
màn_hình	SCRA02
độ_phân_giải	RESA01	Positive: cao
+++ Sentiment Words +++
lớn	INTP,RAMP,SIZP,PISP
cao	RESP,PPIP,ISOP,PRCN
++Overall sentiment: Positive

**loa ngoài note 4 thể_hiện khá_tốt so với note 3 , âm_thanh cũng lớn và rõ_ràng hơn , tất_nhiên nó vẫn chưa thể bì với công_nghệ boomsound vốn đình_đám trên htc one m8 , nhưng với một thiết_bị di_động có_lẽ như_vậy là quá đủ . 
+++ Named Entities +++
loa	SPKA01
note 4	SPSS0013
note 3	SPSS0033
âm_thanh	SOUA01	Positive: lớn
htc one m8	SPHT0012
+++ Sentiment Words +++
lớn	INTP,RAMP,SIZP,PISP
++Overall sentiment: Positive

**camera 
+++ Named Entities +++
camera	CMR,CMRV021
++Overall sentiment: Unspecified

**camera cả mặt trước và sau của galaxy note 4 nâng_cấp hẳn_hoi so với phiên_bản tiền_nhiệm note 3 , với độ_phân_giải 16mp , tích hợp thêm chức_năng chống rung quang_học . 
+++ Named Entities +++
camera	CMR,CMRV021
galaxy note 4	SPSS0012
note 3	SPSS0033
độ_phân_giải	RESA01
16mp	REGEXENT
chống rung quang_học	CMRV011
++Overall sentiment: Unspecified

**trong_khi_đó , camera mặt trước lên_đến 3.7 mp với khẩu_độ f/1.9 , cung_cấp khả_năng chụp_ảnh góc rộng khi ghép 1 lúc 3 hình_ảnh lại với_nhau . 
+++ Named Entities +++
camera	CMR,CMRV021
3.7 mp	REGEXENT
+++ Sentiment Words +++
rộng	DYRP,LIVP
++Overall sentiment: Positive

**ngoài_ra , bạn có_thể sử_dụng cảm_biến nhịp tim để chụp_ảnh , khá tiện_dụng đối_với những ai thích ‘ tự sướng ’ . 
+++ Named Entities +++
cảm_biến	SENA01
+++ Sentiment Words +++
tiện_dụng	OTHP
thích	VERP
sướng	OTHP
++Overall sentiment: Positive

**image 1412092902 samsung galaxy note 4 techz 15 đánh_giá samsung galaxy note 4 : khi ông lớn xứ hàn_đáp trả ! 
+++ Named Entities +++
samsung galaxy note 4	SPSS0013
samsung galaxy note 4	SPSS0013
+++ Sentiment Words +++
lớn	INTP,RAMP,SIZP,PISP
++Overall sentiment: Positive

**camera trên note 4 có độ_phân_giải 16mp . 
+++ Named Entities +++
camera	CMR,CMRV021
note 4	SPSS0013
độ_phân_giải	RESA01
16mp	REGEXENT
++Overall sentiment: Unspecified

**ảnh : hồng_long 
+++ Named Entities +++
ảnh	SENV007
++Overall sentiment: Unspecified

**camera galaxy note 4 có cùng độ_phân_giải so với s5 nhưng chụp nhanh hơn , bắt_nét tốt_hơn 
+++ Named Entities +++
camera	CMR,CMRV021
galaxy note 4	SPSS0012
độ_phân_giải	RESA01
s5	SPSS0043	Positive: nhanh
+++ Sentiment Words +++
nhanh	CHSP,CPUP,GPUP,RAMP,WLAP,BLTP,AFCP
tốt_hơn	OTHP
++Overall sentiment: Positive

**mặc_dù độ_phân_giải tương_đương với cảm_biến máy_ảnh trên galaxy s5 , nhưng camera 16 chấm trên galaxy note 4 được hoàn_thiện hơn đôi_chút , nổi_bật hơn_cả chính ở tốc_độ lấy nét và tốc_độ chụp . 
+++ Named Entities +++
độ_phân_giải	RESA01
cảm_biến	SENA01
máy_ảnh	CMRA01
galaxy s5	SPSS0042
camera	CMR,CMRV021
galaxy note 4	SPSS0012	Positive: hoàn_thiện
+++ Sentiment Words +++
hoàn_thiện	DESP
nổi_bật	OTHP
nét	LCDP,LIVP
++Overall sentiment: Positive

**tuy_nhiên , hình_ảnh vẫn còn chưa thật lắm trong một_số trường_hợp . 
++Overall sentiment: Unspecified

**với khả_năng ‘ nịnh mắt ’ người_dùng vốn là sở_trường của samsung , màu_sắc các vật_thể có_phần tăng cao hơn một_chút so với bình_thường , và hình_ảnh khi phóng to lại cho cảm_giác không mịn_màng cho lắm . 
+++ Named Entities +++
samsung	SS
màu_sắc	COLA01
+++ Sentiment Words +++
cao	RESP,PPIP,ISOP,PRCN
to	SPKP,SIZN,SPKN,SOUN
++Overall sentiment: Neutral

**với tính_năng hdr , chúng_tôi đánh_giá cao những nỗ_lực của samsung với khả_năng tối_ưu hoá giữa phần_mềm và phần_cứng , giúp hình_ảnh thể_hiện tốt_hơn , màu_sắc hiển_thị cũng trung_thực hơn . 
+++ Named Entities +++
tính_năng	FUN
samsung	SS	Positive: tối_ưu
phần_cứng	HAR
màu_sắc	COLA01	Positive: trung_thực
+++ Sentiment Words +++
cao	RESP,PPIP,ISOP,PRCN
tối_ưu	VERP
tốt_hơn	OTHP
trung_thực	COLP
++Overall sentiment: Positive

**bên_cạnh_đó , chúng_ta không_thể không nhắc_đến những bổ_sung đáng_giá trên note 4 với công_nghệ chống rung quang_học ois , tính_năng này sẽ giúp bạn giảm_thiểu được hiện_trạng hình_ảnh bị giật và nhoè khi quay video . 
+++ Named Entities +++
note 4	SPSS0013
chống rung quang_học	CMRV011
ois	CMRV012
tính_năng	FUN
+++ Sentiment Words +++
nhoè	PHON
++Overall sentiment: Negative

**image 1412092902 samsung galaxy note 4 techz 15 đánh_giá samsung galaxy note 4 : khi ông lớn xứ hàn_đáp trả ! 
+++ Named Entities +++
samsung galaxy note 4	SPSS0013
samsung galaxy note 4	SPSS0013
+++ Sentiment Words +++
lớn	INTP,RAMP,SIZP,PISP
++Overall sentiment: Positive

**nhiều chức_năng chụp_hình thú_vị trên galaxy note 4. ảnh : hồng_long 
+++ Named Entities +++
ảnh	SENV007
+++ Sentiment Words +++
nhiều	SENP,AFMP,FCPP
thú_vị	OTHP
++Overall sentiment: Positive

**trong điều_kiện tối , chất_lượng hình_ảnh của galaxy note 4 cũng tốt_hơn so với người tiền_nhiệm note 3 , tuy_rằng vẫn không tránh_khỏi tình_trạng nhiễu hạt , nhưng vẫn ổn hơn so với mặt_bằng chung một_số mẫu điện_thoại cao_cấp hiện_nay . 
+++ Named Entities +++
galaxy note 4	SPSS0012	Positive: tốt_hơn
note 3	SPSS0033
+++ Sentiment Words +++
tối	COLN
chất_lượng	WARP
tốt_hơn	OTHP
ổn	WARP
cao_cấp	OTHP
++Overall sentiment: Positive

**và khi sử_dụng đèn flashled , hình_ảnh xuất ra không quá chói , nó vẫn đảm_bảo được chi_tiết vật_thể . 
++Overall sentiment: Unspecified

**samsung trang_bị khá nhiều tính_năng chụp_hình cho note 4 , nổi_bật trong_đó phải kể đến khả_năng xử_lý da mặt khi chụp_ảnh tự sướng , chụp 2 camera khá thú_vị , hay chụp xoá phông vật_thể , và còn rất nhiều những chức_năng khác . 
+++ Named Entities +++
samsung	SS	Negative: nhiều
tính_năng	FUN
note 4	SPSS0013	Negative: nổi_bật
camera	CMR,CMRV021	Positive: thú_vị
+++ Sentiment Words +++
nhiều	SENP,AFMP,FCPP
nổi_bật	OTHP
sướng	OTHP
thú_vị	OTHP
nhiều	SENP,AFMP,FCPP
++Overall sentiment: Positive

**image 1412093931 10440160 716990468374980 5492836968798832694 n đánh_giá samsung galaxy note 4 : khi ông lớn xứ hàn_đáp trả ! 
+++ Named Entities +++
samsung galaxy note 4	SPSS0013
+++ Sentiment Words +++
lớn	INTP,RAMP,SIZP,PISP
++Overall sentiment: Positive

**image 1412093931 10649693 716990405041653 3273089722015373905 n đánh_giá samsung galaxy note 4 : khi ông lớn xứ hàn_đáp trả ! 
+++ Named Entities +++
samsung galaxy note 4	SPSS0013
+++ Sentiment Words +++
lớn	INTP,RAMP,SIZP,PISP
++Overall sentiment: Positive

**chụp trong điều_kiện tối 
+++ Sentiment Words +++
tối	COLN
++Overall sentiment: Negative

**image 1412093932 10696212 717221128351914 1229013279688113423 n đánh_giá samsung galaxy note 4 : khi ông lớn xứ hàn_đáp trả ! 
+++ Named Entities +++
samsung galaxy note 4	SPSS0013
+++ Sentiment Words +++
lớn	INTP,RAMP,SIZP,PISP
++Overall sentiment: Positive

**chụp dưới ánh_sáng ngoài_trời 
+++ Named Entities +++
ánh_sáng	SENV002
++Overall sentiment: Unspecified

**đối_với camera trước , samsung nâng_cấp từ mức 2.1mp trên note 3 lên 3.7mp đối_với note 4 , với hy_vọng hình_ảnh xuất ra khi bạn tự sướng sẽ sắc nét hơn . 
+++ Named Entities +++
camera	CMR,CMRV021
samsung	SS
2.1mp	REGEXENT
note 3	SPSS0033
3.7mp	REGEXENT
note 4	SPSS0013
+++ Sentiment Words +++
sướng	OTHP
++Overall sentiment: Positive

**ngoài những tính_năng học_tập từ một số_nhà sản_xuất khác như xoá mụn , trắng da , thì chất_lượng ảnh_chụp vẫn chưa thực_sự gây được ấn_tượng , đặc_biệt khi selfish trong điều_kiện thiếu sáng . 
+++ Named Entities +++
tính_năng	FUN
+++ Sentiment Words +++
chất_lượng	WARP
ấn_tượng	OTHP
đặc_biệt	OTHP
++Overall sentiment: Positive

**tính_năng tự sướng góc rộng ghép 3 ảnh làm 1 vẫn chưa thân_thiện lắm với người_dùng , rất khó để sử_dụng . 
+++ Named Entities +++
tính_năng	FUN	Positive: sướng
ảnh	SENV007
+++ Sentiment Words +++
sướng	OTHP
rộng	DYRP,LIVP
khó	OTHN
++Overall sentiment: Positive

**phần_mềm và bút s - pen 
+++ Named Entities +++
s - pen	OTHA01
++Overall sentiment: Unspecified

**image 1412092973 samsung galaxy note 4 techz 27 đánh_giá samsung galaxy note 4 : khi ông lớn xứ hàn_đáp trả ! 
+++ Named Entities +++
samsung galaxy note 4	SPSS0013
samsung galaxy note 4	SPSS0013
+++ Sentiment Words +++
lớn	INTP,RAMP,SIZP,PISP
++Overall sentiment: Positive

**với 3gb ram , samsung chịu_khó chăm_chút hơn khả_năng chạy đa nhiệm của note 4 , các ứng_dụng được thu_gọn và chuyển ra màn_hình chính khá dễ_dàng chỉ với thao_tác đơn_giản là giữ góc bên_phải màn_hình và đẩy xuống , trông rất trực_quan và khá giống với giao_diện trên windows . 
+++ Named Entities +++
3gb	REGEXENT
ram	RAM
samsung	SS
note 4	SPSS0013
màn_hình	SCRA02	Negative: dễ_dàng
màn_hình	SCRA02
windows	OSSV004,OSSV009
+++ Sentiment Words +++
dễ_dàng	OTHP
++Overall sentiment: Positive

**tính_năng hữu_ích này có_thể hoạt_động với một số ứng_dụng như gọi_điện , nhắn_tin , trình facebook . 
+++ Named Entities +++
tính_năng	FUN	Positive: hữu_ích
+++ Sentiment Words +++
hữu_ích	OTHP
++Overall sentiment: Positive

**và khi bạn muốn thu_gọn diện_tích của những ứng_dụng đang hoạt_động đa nhiệm này , nó sẽ hiển_thị kiểu giao_diện chat head tương_tự facebook . 
++Overall sentiment: Unspecified

**với nhiều tính_năng hữu_ích như smart select , s pen as mouse , snap note , galaxy note 4 sẽ mang đến cho bạn những trải nghiệm thú_vị với bút s - pen 
+++ Named Entities +++
tính_năng	FUN	Positive: hữu_ích
s pen	OTHA03
galaxy note 4	SPSS0012
s - pen	OTHA01
+++ Sentiment Words +++
nhiều	SENP,AFMP,FCPP
hữu_ích	OTHP
thú_vị	OTHP
++Overall sentiment: Positive

**một trong những thành_phần không_thể thiếu trên note 4 chính là cây_bút ‘ s - pen ’ đầy phép màu . 
+++ Named Entities +++
note 4	SPSS0013
s - pen	OTHA01
++Overall sentiment: Unspecified

**không những cung_cấp cho bút s - pen của note 4 nhiều tính_năng thông_minh như smart select , s - pen as mouse , snap note . 
+++ Named Entities +++
s - pen	OTHA01
note 4	SPSS0013	Positive: nhiều
tính_năng	FUN
s - pen	OTHA01
+++ Sentiment Words +++
nhiều	SENP,AFMP,FCPP
++Overall sentiment: Positive

**cây_bút thần_kỳ này cũng tỏ_vẻ lanh_lợi và nhanh_nhậy hơn so với note 3 , cụ_thể hơn , nét_vẽ , viết của bút s - pen trên note 4 cũng sắc_sảo hơn 
+++ Named Entities +++
note 3	SPSS0033
s - pen	OTHA01
note 4	SPSS0013	Positive: sắc_sảo
+++ Sentiment Words +++
sắc_sảo	DESP
++Overall sentiment: Positive

**image 1412092867 samsung galaxy note 4 techz 08 đánh_giá samsung galaxy note 4 : khi ông lớn xứ hàn_đáp trả ! 
+++ Named Entities +++
samsung galaxy note 4	SPSS0013
samsung galaxy note 4	SPSS0013
+++ Sentiment Words +++
lớn	INTP,RAMP,SIZP,PISP
++Overall sentiment: Positive

**s - pen là cánh_tay_phải ‘ đắc_lực ’ của galaxy note 4. ảnh : hồng_long 
+++ Named Entities +++
s - pen	OTHA01
ảnh	SENV007
++Overall sentiment: Unspecified

**và s - pen as mouse là một trong những trợ_thủ đắc_lực của bút s - pen với khả_năng quét và chọn các kí tự văn_bản rất trơn_tru và mượt_mà . 
+++ Named Entities +++
s - pen	OTHA01
s - pen	OTHA01
+++ Sentiment Words +++
mượt_mà	OTHP
++Overall sentiment: Positive

**hay smart select giúp người_dùng chọn và cắt nội_dung trên màn_hình và chuyển_đổi chúng qua nhiều màn hình khác_nhau , thay_vì chỉ bó_buộc trong một nội_dung hiển_thị duy_nhất . 
+++ Named Entities +++
màn_hình	SCRA02
màn hình	SCRA02
+++ Sentiment Words +++
nhiều	SENP,AFMP,FCPP
++Overall sentiment: Positive

**cuối_cùng , tính_năng snap note giúp bạn chuyển_thể những hình_ảnh thành dạng văn_bản để chỉnh sửa . 
+++ Named Entities +++
tính_năng	FUN
++Overall sentiment: Unspecified

**một_số tính_năng thân_thuộc khác của note 4 như cảm_biến vân_tay , cảm_biến nhịp tim , cảm_biến đo tia_cực_tím , giúp người_dùng quản_lý sức_khoẻ của mình tốt_hơn khi tiếp_xúc dưới ánh_nắng mặt_trời . 
+++ Named Entities +++
tính_năng	FUN
note 4	SPSS0013
cảm_biến	SENA01
cảm_biến	SENA01
cảm_biến	SENA01
++Overall sentiment: Unspecified

**kết_luận : 
++Overall sentiment: Unspecified

**samsung đã không làm người hâm_mộ thất_vọng với những cải_tiến ‘ vượt trội ’ trên note 4 , từ thiết_kế cho_đến chất_liệu hình_thành . 
+++ Named Entities +++
samsung	SS
note 4	SPSS0013
thiết_kế	DESA01
chất_liệu	MARA01
+++ Sentiment Words +++
thất_vọng	OTHN
cải_tiến	VERP
++Overall sentiment: Neutral

**dẫu gặp rất nhiều trở_ngại từ đối_thủ sừng_sỏ iphone 6 plus , nhưng ‘ quái thú ’ mới cóng của samsung vẫn đạt được kết_quả tuyệt_đối trên sân nhà , cháy hàng chỉ sau 4 ngày lên kệ . 
+++ Named Entities +++
iphone 6 plus	SPAP0071
samsung	SS
+++ Sentiment Words +++
nhiều	SENP,AFMP,FCPP
mới	CHSP,CPUP,GPUP,SENP,BLTP
++Overall sentiment: Positive

**image 1412092990 samsung galaxy note 4 techz 28 đánh_giá samsung galaxy note 4 : khi ông lớn xứ hàn_đáp trả ! 
+++ Named Entities +++
samsung galaxy note 4	SPSS0013
samsung galaxy note 4	SPSS0013
+++ Sentiment Words +++
lớn	INTP,RAMP,SIZP,PISP
++Overall sentiment: Positive

**galaxy note 4 dự_kiến sẽ được bán với giá 17 triệu đồng tại việt_nam . 
+++ Named Entities +++
galaxy note 4	SPSS0012
giá	PRCA01
++Overall sentiment: Unspecified

**ảnh : hồng_long 
+++ Named Entities +++
ảnh	SENV007
++Overall sentiment: Unspecified

**nếu bạn loại_trừ được những khó_khăn khi sử_dụng màn_hình lớn , galaxy note 4 sẽ là một lựa_chọn tuyệt_vời với thiết_kế sang_trọng , rắn_chắc , cấu hình mạnh_mẽ , cùng nhiều công_nghệ tiên_tiến kết_hợp với bút s - pen được tích_hợp bên_trong máy . 
+++ Named Entities +++
màn_hình	SCRA02	Positive: lớn
galaxy note 4	SPSS0012
thiết_kế	DESA01	Positive: sang_trọng
cấu hình	CFG	Positive: mạnh_mẽ
s - pen	OTHA01
+++ Sentiment Words +++
lớn	INTP,RAMP,SIZP,PISP
tuyệt_vời	OTHP
sang_trọng	SCRP
mạnh_mẽ	DESP,CHSP,CPUP,GPUP
nhiều	SENP,AFMP,FCPP
++Overall sentiment: Positive

**tại việt_nam , samsung galaxy note 4 dự_kiến sẽ bán_ra vào giữa tháng 10 với mức_giá 17 triệu đồng . 
+++ Named Entities +++
samsung galaxy note 4	SPSS0013
++Overall sentiment: Unspecified

**kết_luận 
++Overall sentiment: Unspecified

**ưu_điểm 
++Overall sentiment: Unspecified

**vẻ_ngoài sang_trọng , cứng_cáp cùng chất_liệu khung nhôm được vát cạnh sắc_sảo . 
+++ Named Entities +++
chất_liệu	MARA01
+++ Sentiment Words +++
sang_trọng	SCRP
sắc_sảo	DESP
++Overall sentiment: Positive

**cấu hình mạnh_mẽ , tốc_độ thực_thi nhanh nhậy . 
+++ Named Entities +++
cấu hình	CFG	Positive: mạnh_mẽ
+++ Sentiment Words +++
mạnh_mẽ	DESP,CHSP,CPUP,GPUP
nhanh	CHSP,CPUP,GPUP,RAMP,WLAP,BLTP,AFCP
++Overall sentiment: Positive

**bút s - pen hoạt_động nhậy . 
+++ Named Entities +++
s - pen	OTHA01
++Overall sentiment: Unspecified

**nhược_điểm 
++Overall sentiment: Unspecified

**thiết_kế sắc_cạnh , cầm_nắm trong thời_gian dài không thoải_mái . 
+++ Named Entities +++
thiết_kế	DESA01
+++ Sentiment Words +++
dài	DYRP,ISOP,SIZN
thoải_mái	OTHP
++Overall sentiment: Positive

**chất_lượng camera trước không tốt khi chụp đêm . 
+++ Named Entities +++
camera	CMR,CMRV021	Negative: tốt
+++ Sentiment Words +++
chất_lượng	WARP
tốt	PRCP,WARP,PHOP,MARP,DESP,SCRP,PTTP,OSSP,SIZP
++Overall sentiment: Positive

**mép màn_hình và khung nhôm chưa thật_sự liền_lạc 
+++ Named Entities +++
màn_hình	SCRA02
++Overall sentiment: Unspecified

**tổng quanvới nhiều cải_tiến về thiết_kế , cấu hình lẫn phần_mềm bên_trong , ‘ quái_vật xứ hàn ’ galaxy note 4 xứng_đáng là đối_thủ ‘ nặng_ký ’ của iphone 6 plus . 
+++ Named Entities +++
thiết_kế	DESA01
cấu hình	CFG
galaxy note 4	SPSS0012
iphone 6 plus	SPAP0071
+++ Sentiment Words +++
nhiều	SENP,AFMP,FCPP
cải_tiến	VERP
++Overall sentiment: Positive

