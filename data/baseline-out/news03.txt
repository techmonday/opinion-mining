**15 days ago 
++Overall sentiment: Unspecified

**news mention 
++Overall sentiment: Unspecified

**00:00 
++Overall sentiment: Unspecified

**mwc 2015 : tiêu_điểm trận_chiến smartphone cao_cấp 
+++ Sentiment Words +++
cao_cấp	OTHP
++Overall sentiment: Positive

**tin247.com - jan 25 , 2015 12:00 am gmt + 07:00 
++Overall sentiment: Unspecified

**0 0 0 
++Overall sentiment: Unspecified

**icon rộn_ràng sắc xuân - tưng_bừng đón tết - giảm đến 50 % icon ... 
++Overall sentiment: Unspecified

**icon rộn_ràng sắc xuân - tưng_bừng đón tết - giảm đến 50 % icon 
++Overall sentiment: Unspecified

**không giống như ces 2015 , mwc năm_nay hứa_hẹn sẽ chứng_kiến những màn đọ sức khốc_liệt của rất nhiều mẫu smartphone cao_cấp đến từ các ông_lớn trên toàn thế_giới . 
+++ Sentiment Words +++
nhiều	SENP,AFMP,FCPP
cao_cấp	OTHP
++Overall sentiment: Positive

**ces 2015 đã khép lại với khá ít điểm nhấn với làng smartphone ngoại_trừ chiếc lg g flex 2 . 
+++ Named Entities +++
lg g flex 2	SPLG0051
+++ Sentiment Words +++
ít	SENN,OTCN,FLFN,AFMN,FCPN,INTN
++Overall sentiment: Negative

**tuy_nhiên , mọi chuyện chắc_chắn sẽ rất khác tại mwc được diễn_ra vào tháng 2 tới đây . 
++Overall sentiment: Unspecified

**theo nhiều nguồn_tin , sẽ có tất_cả 6 chiếc điện_thoại cao_cấp đến từ samsung , htc , lg , sony . 
+++ Named Entities +++
samsung	SS
htc	HT
lg	LG
sony	SO
+++ Sentiment Words +++
nhiều	SENP,AFMP,FCPP
cao_cấp	OTHP
++Overall sentiment: Positive

**6 sản_phẩm này sẽ tạo_nên một cuộc_đua hết_sức khốc_liệt và thiết_bị nào ấn_tượng hơn sẽ là người_chiến_thắng , ít_nhất là trong_mắt các nhà_báo công_nghệ từ khắp_nơi trên thế_giới . 
+++ Sentiment Words +++
ấn_tượng	OTHP
++Overall sentiment: Positive

**lg g4 
+++ Named Entities +++
lg g4	SPLG0061
++Overall sentiment: Unspecified

**gần_như chắc_chắn lg g4 sẽ góp_mặt tại mwc 2015 . 
+++ Named Entities +++
lg g4	SPLG0061
++Overall sentiment: Unspecified

**theo nhiều nguồn_tin rò rỉ , mẫu điện_thoại cao_cấp tiếp_theo của lg sẽ có cấu hình phần_cứng tương_tự như chiếc g flex 2 ra_mắt cách đây không lâu , ngoại_trừ độ_phân_giải màn_hình là 2k . 
+++ Named Entities +++
lg	LG
cấu hình	CFG
phần_cứng	HAR
g flex 2	SPLG0052
độ_phân_giải	RESA01
màn_hình	SCRA02
+++ Sentiment Words +++
nhiều	SENP,AFMP,FCPP
cao_cấp	OTHP
++Overall sentiment: Positive

**mwc 2015 : tiêu_điểm trận_chiến smartphone cao_cấp 
+++ Sentiment Words +++
cao_cấp	OTHP
++Overall sentiment: Positive

**concept lg g4 . 
+++ Named Entities +++
lg g4	SPLG0061
++Overall sentiment: Unspecified

**ảnh : internet 
+++ Named Entities +++
ảnh	SENV007
++Overall sentiment: Unspecified

**g4 sẽ được trang_bị soc qualcomm snapdragon 810 , dung_lượng ram 3gb ( không ngoại_trừ khả_năng lg sẽ đẩy_lên mức 4gb để có_thể cạnh_tranh tốt_hơn với các sản_phẩm khác ) . 
+++ Named Entities +++
g4	SPLG0062
soc	CHSA01
qualcomm snapdragon	CHSV001
dung_lượng	CPCA01
ram	RAM
3gb	REGEXENT
lg	LG
4gb	EXTV007,INTV001
++Overall sentiment: Unspecified

**mới_đây , lg đã đăng_ký sở_hữu lg g pen , điều_đó có_nghĩa rằng g4 cũng sẽ được trang_bị bút cảm_ứng tương_tự note 4 , hứa_hẹn cuộc chạy_đua giữa các mẫu phablet sở_hữu bút s pen sẽ càng gay cấng hơn_nữa trong thời_gian tới . 
+++ Named Entities +++
lg	LG
lg	LG
g4	SPLG0062
cảm_ứng	SENA02
note 4	SPSS0013
s pen	OTHA03
++Overall sentiment: Unspecified

**sony xperia z4 
+++ Named Entities +++
sony	SO
++Overall sentiment: Unspecified

**trong số 6 smartphone sẽ xuất_hiện tại mwc lần_này , xperia z4 là sản_phẩm sở_hữu những thông_tin rò rỉ vô_cùng ít_ỏi . 
++Overall sentiment: Unspecified

**nhiều người cho_rằng thiết_bị này sẽ có một_chút cải_tiến về thiết_kế khi không còn dùng nắp che cho các chi_tiết như loa hay cổng microusb . 
+++ Named Entities +++
thiết_kế	DESA01
loa	SPKA01
+++ Sentiment Words +++
nhiều	SENP,AFMP,FCPP
cải_tiến	VERP
++Overall sentiment: Positive

**ngoài_ra , máy cũng có_thể hỗ_trợ một camera sau có độ_phân_giải cao hơn mức 20.7 mp trên các mẫu smartphone tiền_nhiệm . 
+++ Named Entities +++
camera	CMR,CMRV021
độ_phân_giải	RESA01	Positive: cao
20.7 mp	REGEXENT
+++ Sentiment Words +++
cao	RESP,PPIP,ISOP,PRCN
++Overall sentiment: Positive

**mwc 2015 : tiêu_điểm trận_chiến smartphone cao_cấp 
+++ Sentiment Words +++
cao_cấp	OTHP
++Overall sentiment: Positive

**concept xperia z4 . 
++Overall sentiment: Unspecified

**ảnh : internet 
+++ Named Entities +++
ảnh	SENV007
++Overall sentiment: Unspecified

**về cấu_hình , xperia z4 được cho là sở_hữu màn_hình kích_thước 5.5 inch với độ_phân_giải 2k , sử_dụng chip snapdragon 810 , có ram 4gb cùng bộ_nhớ trong 32gb . 
+++ Named Entities +++
cấu_hình	CFG
màn_hình	SCRA02
kích_thước	KTHA01
5.5 inch	REGEXENT
độ_phân_giải	RESA01
chip	CHSA03
snapdragon 810	CHSV020
ram	RAM
4gb	EXTV007,INTV001
bộ_nhớ trong	INTA01
32gb	EXTV010,INTV004
++Overall sentiment: Unspecified

**bên_cạnh_đó , z4 cũng_được đồn đoán sẽ tích_hợp chuẩn sạc không dây qi cùng khả_năng chống nước theo tiêu_chuẩn ip68 . 
++Overall sentiment: Unspecified

**samsung galaxy s6 và s6 edge 
+++ Named Entities +++
samsung galaxy s6	SPSS0112
s6	SPSS0114
++Overall sentiment: Unspecified

**khả_năng ra_mắt của galaxy s6 edge vẫn còn bỏ_ngỏ nhưng với s6 lại là một chuyện khác . 
+++ Named Entities +++
galaxy s6 edge	SPSS0111
s6	SPSS0114
++Overall sentiment: Unspecified

**sự_kiện samsung unpacked đầu_tiên trong năm luôn chứng_kiến sự_xuất_hiện của một mẫu flag - ship của dòng galaxy , đồng_thời , mở_ra một kỳ mwc thành_công . 
+++ Named Entities +++
samsung	SS
++Overall sentiment: Unspecified

**năm_ngoái , galaxy s5 đã không thành_công như mong_đợi , bởi_vậy , sự_ra_mắt của s6 sẽ được samsung chau chuốt hơn và chứa_đựng đầy sự_bất_ngờ đối_với người tham_gia cũng_như nhà_báo công_nghệ toàn thế_giới . 
+++ Named Entities +++
galaxy s5	SPSS0042
s6	SPSS0114
samsung	SS
++Overall sentiment: Unspecified

**khả_năng ra_mắt của galaxy s6 edge vẫn còn bỏ_ngỏ nhưng với s6 lại là một chuyện khác . 
+++ Named Entities +++
galaxy s6 edge	SPSS0111
s6	SPSS0114
++Overall sentiment: Unspecified

**sự_kiện samsung unpacked đầu_tiên trong năm luôn chứng_kiến sự_xuất_hiện của một mẫu flag - ship của dòng galaxy , đồng_thời , mở_ra một kỳ mwc thành_công . 
+++ Named Entities +++
samsung	SS
++Overall sentiment: Unspecified

**năm_ngoái , galaxy s5 đã không thành_công như mong_đợi , bởi_vậy , sự_ra_mắt của s6 sẽ được samsung chau chuốt hơn và chứa_đựng đầy sự_bất_ngờ đối_với người tham_gia cũng_như nhà_báo công_nghệ toàn thế_giới . 
+++ Named Entities +++
galaxy s5	SPSS0042
s6	SPSS0114
samsung	SS
++Overall sentiment: Unspecified

**mwc 2015 : tiêu_điểm trận_chiến smartphone cao_cấp 
+++ Sentiment Words +++
cao_cấp	OTHP
++Overall sentiment: Positive

**bản concept samsung galaxy s6 . 
+++ Named Entities +++
samsung galaxy s6	SPSS0112
++Overall sentiment: Unspecified

**ảnh : internet 
+++ Named Entities +++
ảnh	SENV007
++Overall sentiment: Unspecified

**thông_số cấu hình samsung galaxy s6 bao_gồm , màn_hình được nâng_cấp lên kích_thước 5.5 inch , độ_phân_giải quad hd ( 1440x2560 ) , camera phụ 5mp va � � camera chính lên tới 20mp . 
+++ Named Entities +++
cấu hình	CFG
samsung galaxy s6	SPSS0112
màn_hình	SCRA02
kích_thước	KTHA01
5.5 inch	REGEXENT
độ_phân_giải	RESA01
quad hd	RESV014
camera	CMR,CMRV021
5mp	REGEXENT
camera	CMR,CMRV021
20mp	REGEXENT
++Overall sentiment: Unspecified

**bên_cạnh_đó , máy sẽ chạy trên vi xử_lý thế_hệ mới nhất của samsung mang tên exynos 7420 8 nhân , hoạt_động dựa_trên cấu_trúc 64- bit , đồ_họa arm mali - t760 tích_hợp . 
+++ Named Entities +++
samsung	SS
exynos	CHSV021
8 nhân	CPUV018
arm mali	GPUV003
+++ Sentiment Words +++
mới	CHSP,CPUP,GPUP,SENP,BLTP
++Overall sentiment: Positive

**kế_đến , máy sẽ vận_hành trên nền_tảng android 5.0 lollipop mới nhất của google , 3g ram , 32gb bộ_nhớ trong . 
+++ Named Entities +++
android	OSSV002	Positive: mới
3g	OTCV018
ram	RAM
32gb	EXTV010,INTV004	Positive: trong
bộ_nhớ trong	INTA01
+++ Sentiment Words +++
mới	CHSP,CPUP,GPUP,SENP,BLTP
trong	HEPP,HEPA01P,SPKP,SOUP
++Overall sentiment: Positive

**bộ đôi m9 và one m9 plus 
+++ Named Entities +++
m9	SCLE0022
one m9	SPHT0081
++Overall sentiment: Unspecified

**dòng one của htc luôn là một điểm nhấn vô_cùng quan_trọng của htc trong vài năm trở_lại đây và ngay_cả khi hãng điện_thoại đài_loan rơi vào tình_thế khó_khăn , dòng one lại đóng một vai_trò là vị_cứu_tinh . 
+++ Named Entities +++
htc	HT
htc	HT
++Overall sentiment: Unspecified

**bởi_vậy , sự_hiện_diện của one m9 tại mwc sẽ quyết_định được sự_thành_bại của htc trong năm 2015 . 
+++ Named Entities +++
one m9	SPHT0081
htc	HT
++Overall sentiment: Unspecified

**mwc 2015 : tiêu_điểm trận_chiến smartphone cao_cấp 
+++ Sentiment Words +++
cao_cấp	OTHP
++Overall sentiment: Positive

**concept tuyệt đẹp về htc one m9 . 
+++ Named Entities +++
htc one m9	SPHT0082
+++ Sentiment Words +++
tuyệt	OTHP
đẹp	DESP,PHOP,MARP,SCRP,COLP
++Overall sentiment: Positive

**ảnh : internet 
+++ Named Entities +++
ảnh	SENV007
++Overall sentiment: Unspecified

**htc được dự_đoán sẽ nâng_cấp khá mạnh_mẽ phần_cứng của htc one ( m9 ) , máy sẽ được trang_bị màn_hình 5.2 inch có độ_phân_giải 1440x2560 pixel ( 2k ) , mật_độ điểm ảnh đạt được 564ppi , cao hơn_hẳn so với one ( m8 ) là 441ppi . 
+++ Named Entities +++
htc	HT
phần_cứng	HAR
htc	HT
m9	SCLE0022
màn_hình	SCRA02
5.2 inch	REGEXENT
độ_phân_giải	RESA01
mật_độ điểm ảnh	PPIA03
564ppi	REGEXENT
m8	SPHT0012
441ppi	REGEXENT
+++ Sentiment Words +++
mạnh_mẽ	DESP,CHSP,CPUP,GPUP
cao	RESP,PPIP,ISOP,PRCN
++Overall sentiment: Positive

**không những thế , htc còn cung_cấp cho one ( m9 ) bộ_vi xử_lý snapdragon 805 , 3g ram , bộ_nhớ trong 64gb và 128gb , cài sẵn hệ_điều_hành android 5.0 . 
+++ Named Entities +++
htc	HT
m9	SCLE0022
snapdragon 805	CHSV018
3g	OTCV018
ram	RAM
bộ_nhớ trong	INTA01
64gb	EXTV011,INTV005
128gb	EXTV012
android	OSSV002
++Overall sentiment: Unspecified

**dung_lượng pin đồng_thời cũng cao hơn để phù_hợp với độ_phân_giải màn_hình của máy . 
+++ Named Entities +++
dung_lượng	CPCA01
pin	BATA01
độ_phân_giải	RESA01
màn_hình	SCRA02
+++ Sentiment Words +++
cao	RESP,PPIP,ISOP,PRCN
++Overall sentiment: Neutral

**ngoài_ra , máy còn được hỗ_trợ khả_năng chống nước , không_rõ đây là loại chứng_chỉ nào , nhưng sẽ cao hơn loại ipx3 đang được trang_bị trên one ( m9 ) . 
+++ Named Entities +++
m9	SCLE0022
+++ Sentiment Words +++
cao	RESP,PPIP,ISOP,PRCN
++Overall sentiment: Neutral

**camera sau cũng sẽ có độ_phân_giải cao hơn , tuy_nhiên vẫn chưa rõ đây có_phải là công_nghệ ultrapixel hay_không . 
+++ Named Entities +++
camera	CMR,CMRV021
độ_phân_giải	RESA01	Positive: cao
+++ Sentiment Words +++
cao	RESP,PPIP,ISOP,PRCN
rõ	SPKP,COLP
++Overall sentiment: Positive

**ngoài_ra , one m9 sẽ có thêm một phiên_bản lớn hơn với cấu hình tương_tự và không có nhiều sự_khác_biệt ngoài kích_thước màn_hình lớn hơn một_chút . 
+++ Named Entities +++
one m9	SPHT0081
cấu hình	CFG
kích_thước	KTHA01
màn_hình	SCRA02	Positive: lớn
+++ Sentiment Words +++
lớn	INTP,RAMP,SIZP,PISP
nhiều	SENP,AFMP,FCPP
lớn	INTP,RAMP,SIZP,PISP
++Overall sentiment: Positive

**htc m9 và smartwatch của htc sẽ trình_làng tại mwc 2015 
+++ Named Entities +++
htc m9	SCLE0022
htc	HT
++Overall sentiment: Unspecified

**nguồn_tin thân cậy bloomberg cho_hay , htc sẽ công_bố mẫu flagship thế_hệ mới tại triển_lãm mwc trong năm_nay , đồng_hành theo đó là chiếc smartwatch đầu_tiên của hãng . 
+++ Named Entities +++
htc	HT
+++ Sentiment Words +++
mới	CHSP,CPUP,GPUP,SENP,BLTP
++Overall sentiment: Positive

