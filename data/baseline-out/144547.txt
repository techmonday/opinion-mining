**
++Overall sentiment: Unspecified

**đánh_giá htc one m8 : thiết_kế đẹp , mạnh_mẽ , âm_thanh hay 
+++ Named Entities +++
htc one m8	SPHT0012
thiết_kế	DESA01	Positive: đẹp
âm_thanh	SOUA01	Positive: hay
+++ Sentiment Words +++
đẹp	DESP,PHOP,MARP,SCRP,COLP
mạnh_mẽ	DESP,CHSP,CPUP,GPUP
hay	HEPP,HEPA01P
++Overall sentiment: Positive

**[ windows phone ] mở hộp lumia 435 ram 1gb , hai sim giá 1.799.000 đồng 
+++ Named Entities +++
windows phone	OSSV003
lumia 435	SPNM0092
ram	RAM
1gb	EXTV005
sim	SIM
giá	PRCA01
++Overall sentiment: Unspecified

**việt_nam có_lẽ là nơi đầu_tiên được microsoft bán chính_thức chiếc lumia 435 có_lẽ do máy được sản_xuất tại việt_nam . 
+++ Named Entities +++
microsoft	MI
lumia 435	SPNM0092
++Overall sentiment: Unspecified

**hôm_nay hãng đã ra_mắt người_dùng hai sản_phẩm lumia chạy windows phone 8.1 có giá_rẻ nhất từ trước đến nay . 
+++ Named Entities +++
windows phone	OSSV003	Positive: giá_rẻ
+++ Sentiment Words +++
giá_rẻ	OTHP
++Overall sentiment: Positive

**lumia 435 sẽ có giá_bán chính_thức 1.799.000 đồng với 4 màu cơ_bản đó là đen , trắng , màu cam và xanh lá tất_cả các màu này đều là vỏ nhám chứ không bóng như một_số máy lumia 630 mà hãng bán_ra trước_đây . 
+++ Named Entities +++
lumia 435	SPNM0092
vỏ	VOMA01	Negative: nhám
lumia 630	SPNM0111
+++ Sentiment Words +++
nhám	VOMN
bóng	VOMP
++Overall sentiment: Neutral

**về thiết_kế máy nhìn giống tương_đối giống thiết_kế của máy nokia x và x2 vuông_vức với kích_thước 118.1 x 64.7 x 11.7 mm và trọng_lượng máy là 134,1g . 
+++ Named Entities +++
thiết_kế	DESA01
thiết_kế	DESA01
nokia	NO
x2	SCLE0052
kích_thước	KTHA01
11.7 mm	REGEXENT
trọng_lượng	WEI
134,1g	REGEXENT
++Overall sentiment: Unspecified

**đặc_biệt lumia 435 vẫn dùng phím điều hướng vật_lý chứ không_phải cảm_ứng như các dòng máy trước lumia 630 hay lumia 730 . 
+++ Named Entities +++
lumia 435	SPNM0092
cảm_ứng	SENA02
lumia 630	SPNM0111
lumia 730	SPNM0041
+++ Sentiment Words +++
đặc_biệt	OTHP
++Overall sentiment: Positive

**mặt trước phía_trên vẫn là logo microsoft , loa_thoại và camera trước , ở dưới vẫn là ba phím cơ_bản của windows phone là back , home và tìm_kiếm bing . 
+++ Named Entities +++
microsoft	MI
camera	CMR,CMRV021
windows phone	OSSV003
++Overall sentiment: Unspecified

**cạnh phải là ba phím cứng nguồn và tăng_giảm âm_lượng , cổng sạc được để ở cạnh dưới của máy và jack tai_nghe 3.5mm nằm trên đầu máy . 
+++ Named Entities +++
tai_nghe	HEPA02
3.5mm	REGEXENT
++Overall sentiment: Unspecified

**lumia 435 là sản_phẩm tầm_thấp nhằm tới phân_khúc người_dùng bình_dân như công_nhân , sinh_viên ... mặc_dù có giá thấp nhưng máy vẫn được trang_bị cấu hình tương_đối tốt với chip xử_lý snapdragon 200 xung nhịp 1.2 ghz , bộ_nhớ ram 1gb có_thể thoải_mái chơi các game hay ứng_dụng mà_không lo giới_hạn . 
+++ Named Entities +++
lumia 435	SPNM0092
giá	PRCA01	Positive: thấp
cấu hình	CFG	Positive: tốt
chip xử_lý	CHSA05
snapdragon 200	CHSV007
1.2 ghz	REGEXENT
bộ_nhớ	INTA02
ram	RAM
1gb	EXTV005	Positive: thoải_mái
+++ Sentiment Words +++
thấp	RESN,PPIN,ISON,PRCP
tốt	PRCP,WARP,PHOP,MARP,DESP,SCRP,PTTP,OSSP,SIZP
thoải_mái	OTHP
++Overall sentiment: Positive

**bộ_nhớ máy là 8gb kèm khe căm thẻ nhớ lên tới 128gb . 
+++ Named Entities +++
bộ_nhớ	INTA02
8gb	EXTV008,INTV002
thẻ nhớ	CASA03
128gb	EXTV012
++Overall sentiment: Unspecified

**microsoft trang_bị cho máy một màn_hình 4 inch dộ phân_giải wvga ( 480 x 800 ) với mật_độ điểm ảnh là 233ppi với công_nghệ màn_hình lcd . 
+++ Named Entities +++
microsoft	MI
màn_hình	SCRA02
4 inch	REGEXENT
wvga	RESV007
mật_độ điểm ảnh	PPIA03
233ppi	REGEXENT
màn_hình	SCRA02
lcd	SCRV001
++Overall sentiment: Unspecified

**ngoài_ra còn có tính_năng kiểm_soát độ sáng , phản_hồi xúc_giác , cảm_ứng hướng , chế_độ tăng màu , tốc_độ làm mới 60 hz , dễ lau sạch , sọc rgb . 
+++ Named Entities +++
tính_năng	FUN
cảm_ứng	SENA02
+++ Sentiment Words +++
mới	CHSP,CPUP,GPUP,SENP,BLTP
++Overall sentiment: Positive

**lumia 435 được trang_bị viên pin có dung_lượng 1.560mah cho thời_gian chờ tối_đa với 2 sim lên tới 21 giờ , thời_gian thoại 2g là 20,9 giờ và thời_gian vào mạng bằng wifi liên_tục được khoảng 9,4 giờ . 
+++ Named Entities +++
lumia 435	SPNM0092
pin	BATA01
dung_lượng	CPCA01
1.560mah	REGEXENT
sim	SIM
21 giờ	REGEXENT
2g	REGEXENT
20,9 giờ	REGEXENT
mạng	NETA01
wifi	WLAA01
9,4 giờ	REGEXENT
++Overall sentiment: Unspecified

**máy bán_ra được chạy sẵn hệ_điều_hành windows phone 8.1 phiên_bản denim với những tính_năng nổi_bật . 
+++ Named Entities +++
windows phone	OSSV003
tính_năng	FUN	Positive: nổi_bật
+++ Sentiment Words +++
nổi_bật	OTHP
++Overall sentiment: Positive

**ngoài_ra lumia 435 cũng_được trang_bị một số tính_năng hay trên các dòng lumia như chạm hai lần để mở khóa , chỉnh độ sáng và màu_sắc hiển_thị trên màn_hình máy . 
+++ Named Entities +++
lumia 435	SPNM0092
tính_năng	FUN
màu_sắc	COLA01
màn_hình	SCRA02
++Overall sentiment: Unspecified

**lumia 435 có_lẽ là thiết_bị chạy windows phone có độ_phân_giải camera thấp nhất với camera chính chỉ có 2mp hỗ_trợ quay video wvga ( 480 x 800 ) và camera phụ là vga . 
+++ Named Entities +++
lumia 435	SPNM0092
windows phone	OSSV003
độ_phân_giải	RESA01
camera	CMR,CMRV021
camera	CMR,CMRV021
2mp	CMRV023
wvga	RESV007
camera	CMR,CMRV021
vga	RESV006,CMRV026
+++ Sentiment Words +++
thấp	RESN,PPIN,ISON,PRCP
++Overall sentiment: Neutral

**một_số hình_ảnh khác lumia 435 ​ 
+++ Named Entities +++
lumia 435	SPNM0092
++Overall sentiment: Unspecified

**sau_khi trải nghiệm thấy lumia 435 hoạt_động mượt_mà , cảm_ứng tốt không bị lỗi như chiếc lumia 535 . 
+++ Named Entities +++
lumia 435	SPNM0092	Positive: mượt_mà
cảm_ứng	SENA02	Positive: tốt
lumia 535	SPNM0071
+++ Sentiment Words +++
mượt_mà	OTHP
tốt	PRCP,WARP,PHOP,MARP,DESP,SCRP,PTTP,OSSP,SIZP
++Overall sentiment: Positive

**một sản_phẩm giá tốt với cấu hình tương_đối , hoàn_toàn phù_hợp với những bạn không có nhu_cầu nhiều về camera . 
+++ Named Entities +++
giá	PRCA01	Positive: tốt
cấu hình	CFG
camera	CMR,CMRV021
+++ Sentiment Words +++
tốt	PRCP,WARP,PHOP,MARP,DESP,SCRP,PTTP,OSSP,SIZP
nhiều	SENP,AFMP,FCPP
++Overall sentiment: Positive

**cấu hình ngon hơn 530 
+++ Named Entities +++
cấu hình	CFG	Positive: ngon
530	SPNM0092
+++ Sentiment Words +++
ngon	PRCP
++Overall sentiment: Positive

**cấu_hình , giá thơm quá 
+++ Named Entities +++
cấu_hình	CFG
giá	PRCA01	Positive: thơm
+++ Sentiment Words +++
thơm	PRCP
++Overall sentiment: Positive

**nhìn có_vẻ dày_nhỉ 
++Overall sentiment: Unspecified

**có 11.7 mm thôi 
+++ Named Entities +++
11.7 mm	REGEXENT
++Overall sentiment: Unspecified

**thế còn 532 có doublr tap k ? hắn có glance , mà thêm double tap thì tốt 
+++ Named Entities +++
532	SPNM0102
+++ Sentiment Words +++
tốt	PRCP,WARP,PHOP,MARP,DESP,SCRP,PTTP,OSSP,SIZP
++Overall sentiment: Positive

**uầy , v ~ thật 
++Overall sentiment: Unspecified

**made in viet nam_kìa . 
++Overall sentiment: Unspecified

**like microsoft vì việt_nam luôn được đón sản_phẩm sớm , về_tay anh soft giá ngon hẳn . 
+++ Named Entities +++
microsoft	MI
giá	PRCA01	Positive: ngon
+++ Sentiment Words +++
ngon	PRCP
++Overall sentiment: Positive

**cũng xấp_xỉ nhau à , được_cái này mất cái kia , chip snapdragon s200 dual core , còn 530 là qual core . 
+++ Named Entities +++
chip	CHSA03
snapdragon	CHSV002
s200	CCCA0042
530	SPNM0092
++Overall sentiment: Unspecified

**so 530 với 532 thì mới thấy đắng lòng : popo45 : 
+++ Named Entities +++
530	SPNM0092
532	SPNM0102	Positive: mới
+++ Sentiment Words +++
mới	CHSP,CPUP,GPUP,SENP,BLTP
++Overall sentiment: Positive

**có bạn nhé . 
++Overall sentiment: Unspecified

**532 thiếu cái đèn nữa thì thành flagship tầm thấp cmnr 
+++ Named Entities +++
532	SPNM0102
+++ Sentiment Words +++
thấp	RESN,PPIN,ISON,PRCP
++Overall sentiment: Neutral

**wp thì dual core vẫn mượt chán , con này ram rom đều gấp_đôi 530 , giá lại rẻ hơn 200k ( con_số khá lớn vs sinh_viên ) , ko quan_trọng camera thì con này hơn đứt ( lại có cam trước ) : popo45 : 
+++ Named Entities +++
ram	RAM
530	SPNM0092
giá	PRCA01	Positive: rẻ
camera	CMR,CMRV021
+++ Sentiment Words +++
mượt	OSSP
rẻ	PRCP
lớn	INTP,RAMP,SIZP,PISP
++Overall sentiment: Positive

**p // s mấy hôm_trước vừa tư_vấn thằng bạn mua 530 , nó đi đá_bóng cho trường đc thưởng 2tr đem_ra mua hết , giờ nó biết con này thì chắc nó đem mình ra làm bóng_đá quá : popo19 : 
+++ Named Entities +++
530	SPNM0092
+++ Sentiment Words +++
mua	VERP
mua	VERP
++Overall sentiment: Positive

**đây là bảng so_sánh cấu hình chi_tiết 532 , 432 và 535 cho bác nào cần : popo67 : 
+++ Named Entities +++
cấu hình	CFG
532	SPNM0102
535	SPNM0072
++Overall sentiment: Unspecified

**http : //www.phonearena.com // phones // co ... 435 , microsoft lumia 535/phones/9093,9092,8989 
+++ Named Entities +++
microsoft	MI
++Overall sentiment: Unspecified

**k thể hiểu nổi ms cho ba phím điều hướng là phím cứng trong_khi 730 là phím ảo.nhọ 
+++ Named Entities +++
730	SPNM0042
++Overall sentiment: Unspecified

**có vẻ đắng quá : popo28 : 
++Overall sentiment: Unspecified

**không đắng bằng em vừa mất máy thì ms giơi thiệu win 10 
++Overall sentiment: Unspecified

**đhs nó cắt 3 phím điều hướng trên con 730 , còn con này nó lại có : v 
+++ Named Entities +++
730	SPNM0042
++Overall sentiment: Unspecified

**cái giá quá tốt cho một smartphone 1 gb ram , kiểu này điện_thoại feature phone khó_sống nổi rồi , giá này cho học_sinh sinh_viên , hoặc mua_về làm điện_thoại chữa_cháy cũng tốt : popo28 : 
+++ Named Entities +++
giá	PRCA01	Positive: tốt
1 gb	REGEXENT
ram	RAM
giá	PRCA01
+++ Sentiment Words +++
tốt	PRCP,WARP,PHOP,MARP,DESP,SCRP,PTTP,OSSP,SIZP
tốt	PRCP,WARP,PHOP,MARP,DESP,SCRP,PTTP,OSSP,SIZP
++Overall sentiment: Positive

**mượt khi lướt ngoài giao_diện hay web thôi , chứ ram 1g mà dùng con cpu này chơi game chắc lag tung chảo vì bị set cấu hình cao cho ram 1g ( ví_dụ như asphalt 8 ) . 
+++ Named Entities +++
ram	RAM
1g	REGEXENT
cpu	CPU
cấu hình	CFG
ram	RAM
1g	REGEXENT
+++ Sentiment Words +++
mượt	OSSP
cao	RESP,PPIP,ISOP,PRCN
++Overall sentiment: Positive

**camera thì coi như có cắt camera sau ra chia_đều thành cái trước : popo20 : . 
+++ Named Entities +++
camera	CMR,CMRV021
camera	CMR,CMRV021
++Overall sentiment: Unspecified

**ư ́ c chế nhất_là cái vụ rom và tính_năng double tap thôi : popo45 : 
+++ Named Entities +++
tính_năng	FUN
++Overall sentiment: Unspecified

**tầm_máy giá_thế này thì anh mic chắc chủ_yếu đánh_vào phân_khúc " các cụ " , mua chủ_yếu để lướt fb ngắm anh con_cái , hoặc để chat chit viber với con_gái đi lấy_chồng : rofl : . 
+++ Sentiment Words +++
mua	VERP
++Overall sentiment: Positive

**mà với cái giá thế này chơi được asphalt 8 thì có giật tí cũng chả sao bác_a . 
+++ Named Entities +++
giá	PRCA01
++Overall sentiment: Unspecified

**thử so với hàng android mà tầm giá này xem có con nào ngon_lành được vậy không , chưa_kể sắp tới còn được lên win10 : rofl : 
+++ Named Entities +++
android	OSSV002
giá	PRCA01
++Overall sentiment: Unspecified

**giá mà wp có grab taxi thì xúc ngay 1 con tặng phụ_huynh : popo09 : 
+++ Named Entities +++
giá	PRCA01
++Overall sentiment: Unspecified

**muốn cho ms 1 phát quá : popo10 : 
++Overall sentiment: Unspecified

**chán k muốn bình_luận 620 
++Overall sentiment: Unspecified

**525 ngon hơn : rofl : 
+++ Named Entities +++
525	SPNM0082	Positive: ngon
+++ Sentiment Words +++
ngon	PRCP
++Overall sentiment: Positive

**đhs 730 lại 3 phím điều hướng_ảo còn con cùi bắp này lại đc bên_ngoài ? 
+++ Named Entities +++
730	SPNM0042
++Overall sentiment: Unspecified

**: popo10 : 
++Overall sentiment: Unspecified

**mới hôm_qua ra tgdd xách e 730 về vẫn chưa quen đc 
+++ Named Entities +++
730	SPNM0042
+++ Sentiment Words +++
mới	CHSP,CPUP,GPUP,SENP,BLTP
++Overall sentiment: Positive

**giá tốt . 
+++ Named Entities +++
giá	PRCA01	Positive: tốt
+++ Sentiment Words +++
tốt	PRCP,WARP,PHOP,MARP,DESP,SCRP,PTTP,OSSP,SIZP
++Overall sentiment: Positive

**nhưng vẫn dùng 520 
++Overall sentiment: Unspecified

**giống y chang em x nhỉ : popo28 : 
++Overall sentiment: Unspecified

**ko xem thì thôi , xem lại ức thêm 
++Overall sentiment: Unspecified

**đúng là đồ công_nghệ theo thời gian. sinh sau đẻ_muộn hơn sinh trước là điểu dễ hiễu , nhưng giá quá rẻ mà_lại có cấu hình ngang có_khi còn hơn mấy e đời trước giá_thành cao hơn 
+++ Named Entities +++
giá	PRCA01	Positive: rẻ
cấu hình	CFG
giá_thành	PRCA03	Negative: cao
+++ Sentiment Words +++
rẻ	PRCP
cao	RESP,PPIP,ISOP,PRCN
++Overall sentiment: Neutral

**: popo22 : 
++Overall sentiment: Unspecified

**11.7mm : popo57 : 
+++ Named Entities +++
11.7mm	REGEXENT
++Overall sentiment: Unspecified

**sao k là 12.7mm cho chất : rofl : 
+++ Named Entities +++
12.7mm	REGEXENT
++Overall sentiment: Unspecified

**thực_ra có 3 phím ảo cũng hay bạn ạ , mình lúc_đầu cũng không quen lắm . 
++Overall sentiment: Unspecified

**nhưng giờ thì dùng cũng ok . 
++Overall sentiment: Unspecified

**: popo20 : : popo20 : : popo20 : ko có gì để chê ngoài cái logo microsoft. . 
+++ Sentiment Words +++
chê	VERN
++Overall sentiment: Negative

**cầm_chắc_tay , cơ mà thời này còn ai dùng camera 2.0 nữa đâu , ít_ra phải 3.2 mới chụp đc 
+++ Named Entities +++
camera	CMR,CMRV021
+++ Sentiment Words +++
mới	CHSP,CPUP,GPUP,SENP,BLTP
++Overall sentiment: Positive

**ngon ! nhìn có_vẻ chắc_chắn nhi ! 
+++ Sentiment Words +++
ngon	PRCP
++Overall sentiment: Positive

**quyết tăng thị_phần nè 
++Overall sentiment: Unspecified

**search làm j có grabtaxi nhỉ ? 
++Overall sentiment: Unspecified

**cấu hình hơn_hẳn 525 
+++ Named Entities +++
cấu hình	CFG
525	SPNM0082
++Overall sentiment: Unspecified

**chán vl : popo77 : 
++Overall sentiment: Unspecified

**tên đầy_đủ là graptaxi : book a taxi 
+++ Sentiment Words +++
đầy_đủ	OTCP
++Overall sentiment: Positive

**cái_đó thì mình chịu bạn ơi . 
++Overall sentiment: Unspecified

**mình cài trên 920 bình_thường 
++Overall sentiment: Unspecified

**hơn đâu mà hơn . 
++Overall sentiment: Unspecified

**phần_cứng thua 525 nhiều . 
+++ Named Entities +++
phần_cứng	HAR
525	SPNM0082	Negative: nhiều
+++ Sentiment Words +++
nhiều	SENP,AFMP,FCPP
++Overall sentiment: Positive

**532 còn xấp_xỉ được . 
+++ Named Entities +++
532	SPNM0102
++Overall sentiment: Unspecified

**thời_gian chờ 2 sim có 21h , nhầm không vậy ? 
+++ Named Entities +++
sim	SIM
++Overall sentiment: Unspecified

**giá rẻ nhưng không có sự_đột_phá như zenphone 
+++ Named Entities +++
giá	PRCA01	Positive: rẻ
+++ Sentiment Words +++
rẻ	PRCP
++Overall sentiment: Positive

**klq. fb beta bị gỡ rồi à các bác , vào kô dc tìm trên store cũng kô cho cài_đặt 
++Overall sentiment: Unspecified

**cpu mạnh hơn 525 
+++ Named Entities +++
cpu	CPU	Positive: mạnh
525	SPNM0082
+++ Sentiment Words +++
mạnh	WAVP
++Overall sentiment: Positive

**phải làm em này để thay em motorola hai sim 2 này mới_đươc 
+++ Named Entities +++
motorola	MO
sim	SIM
++Overall sentiment: Unspecified

**giá thơm , cáu hình cũng ổn : popo28 : 
+++ Named Entities +++
giá	PRCA01	Positive: thơm
+++ Sentiment Words +++
thơm	PRCP
ổn	WARP
++Overall sentiment: Positive

**theo mình với tầm_giá này thì mình cũng không đòi_hỏi gì nhiều hơn , mình có trải nghiệm asphatl 8 trên galaxy a5 , tầm 9tr mà chạy game này không nỗi , cứ đứng ở màn_hình gameloft ( test ở tgdd ) . 
+++ Named Entities +++
galaxy a5	SPSS0072
màn_hình	SCRA02
+++ Sentiment Words +++
nhiều	SENP,AFMP,FCPP
++Overall sentiment: Positive

**mình chỉ cay 2 con này bàn_phím vật_lý còn 730 của mình lại bàn_phím ảo ! 
+++ Named Entities +++
bàn_phím	KBR,KBR
730	SPNM0042
bàn_phím	KBR,KBR
++Overall sentiment: Unspecified

**cơ mà mình vẫn thích bàn_phím vật_lý hơn ! 
+++ Named Entities +++
bàn_phím	KBR,KBR
+++ Sentiment Words +++
thích	VERP
++Overall sentiment: Positive

**bác nói dug vl sao bag 525 thần_thánh dc bọn cùi_bẵp này treo dc modern combat 5 moi_lạ 
+++ Named Entities +++
525	SPNM0082	Positive: thần_thánh
+++ Sentiment Words +++
thần_thánh	OTHP
++Overall sentiment: Positive

**con này chỉ dùng snapdragon 200 và có 2 nhân à . 
+++ Named Entities +++
snapdragon 200	CHSV007
2 nhân	CPUV012
++Overall sentiment: Unspecified

**ko mạnh hơn con s4 của 525 dc đâu . 
+++ Named Entities +++
525	SPNM0082
+++ Sentiment Words +++
mạnh	WAVP
++Overall sentiment: Positive

**nghi_vấn dis game ngay và luôn . 
++Overall sentiment: Unspecified

**sử_dụng bảng mod cho ram 512 chắc được . 
+++ Named Entities +++
ram	RAM
++Overall sentiment: Unspecified

**con 520 giờ thấy hết thần_thánh rồi , ghẻ chịu ko_nổi , đi_thi anh văn_dùng cái bing translator thế éo nào mới dịch vài phát cái treo luôn , lúc_trước dùng ngon mà wp ngày_càng cập_nhật càng dở , dùng bing dịch treo_mãi 
+++ Named Entities +++
520 giờ	REGEXENT	Negative: thần_thánh
+++ Sentiment Words +++
thần_thánh	OTHP
ghẻ	OTHN
mới	CHSP,CPUP,GPUP,SENP,BLTP
ngon	PRCP
dở	HEPN,HEPA01N,SPKN,SOUN
++Overall sentiment: Positive

**sao máy mình có caicâú hinhánh sáng_nhỉ 
++Overall sentiment: Unspecified

