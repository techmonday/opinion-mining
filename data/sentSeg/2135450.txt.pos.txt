﻿/﻿ Đánh_giá/Np BlackBerry/Np Q10/Np :/: sự_trở_lại/N của/E bàn_phím/N QWERTY/Ny vật_lý/N

Q10/Ny là/V một/M trong/E hai/M sản_phẩm/N đánh_dấu/V sự_quay_trở_lại/N của/E BlackBerry/Np trên/A thị_trường/N sau/N hơn/A một/M năm/N im/A hơn/A lặng/V tiếng/N để/Np thực_hiện/V công_tác/N chuẩn_bị/V ./.
Nếu/C như/C Z10/Np là/V thiết_bị/N hoạt_động/N hoàn_toàn/A dựa_vào/V màn_hình/N cảm_ứng/N theo/V xu_thế/N của/E phần_lớn/N smartphone/V ngày_nay/N thì/C Q10/M lại/Np mang/V một_chút/L cổ_điển/A trong_mình/A khi/N sở_hữu/V bàn_phím/N QWERTY/Ny vật_lý/N ./.
Nếu/C so/V với/E Z10/M thì/C Q10/M mang/V đậm/A chất/N BlackBerry/Np trong_mình/A hơn/A ,/, một/M cái/Nc chất/A rất/R riêng/A nhờ/E có/V bàn_phím/N thật/A ./.
Nhưng/C liệu/V Q10/N có_thể/R thu_hút/V được/V người_dùng/L trong/E bối_cảnh/N các/L đối_thủ/N Andorid/Np ,/, iOS/Np ,/, Windows/Np Phone/Np đang/R nổi_lên/V ngày_càng/X mãnh_liệt/A ?/?

1/M ./.
Thiết_kế/Np và/C ngoại_hình/N


Ngay/T từ/E cái/Nc nhìn/V đầu_tiên/A ai/P cũng/R có_thể/R biết/V được/V rằng/C đây/Ny là/V một/M sản_phẩm/N của/E BlackBerry/Np nhờ/V thiết_kế/V mang/V tính/N truyền_thống/N của/E nó/P ./.
Một/M thiết_kế/V thuôn/N gọn/A ,/, rất/R vừa_tay/A ,/, giống/N với/C những/L chiếc/Nc BlackBerry/Np Bold/Np nổi_tiếng/A một/M thời/N ./.
Máy/Np được/V bo/V tròn/A bốn_góc/X mềm_mại/A nhưng/C khi/N kết_hợp/V với/E bàn_phím/N được/V duỗi_thẳng/L cùng/C các/L đường/N kim_loại/N ,/, Q10/Np thật_sự/A rất/R mạnh_mẽ/A và/C mang/V phong_cách/N BlackBerry/Np không/R lẫn_vào/V đâu/T được/V ./.
Cũng/R thành_phần/N này/Np đã/R đem_đến/V sự_hiện_đại/N cho/E thiết_kế/V của/E Q10/Ny ./.
Phần/N cạnh/N dưới/A của/E máy/N ,/, ở/E phần/N giáp/Np với/E nắp/N lưng/N cũng_được/V bo/V tròn/A khiến/V việc/Np cầm/V chiếc/Nc Q10/M rất/R dễ_dàng/A và/C không/R bị/V cấn/V tay/N ./.
Và/C tất_nhiên/A là/C thiết_bị/N này/Np có/V chất_lượng/N hoàn_thiện/Nb ở/E mức/N tốt/A ,/, máy/N đem_lại/V cảm_giác/N chắc_chắn/A khi/N sử_dụng/V ./.


Mặt_sau/N của/E Q10/Ny được/V trang_trí/V bằng/E các/L hoa_văn/N dạng/N sợi/Nc carbon/N ,/, gần/R giống/N hoa_văn/N mà/C BlackBerry/Np từng/Np mang/V lên/E chiếc/Nc Bold/Np 9900/M ,/, tuy_nhiên/C mặt/N lưng/N này/P không_được/R phủ/C một/M lớp/N kính/N bóng_bẩy/A như/C 9900/M mà/Np chỉ/R dùng/V nhựa/N sơn/V mờ/A ./.
Mình/P thấy/V rằng/C nếu_được/N phủ/Nc kính/N thì/C Q10/M trông/V sẽ/R sang_trọng/A hơn/A ./.
Anh_em/N lưu_ý/V rằng/C hoa_văn/N mặt_lưng/Np của/E bản/Nc màu_đen/Np thì/C dạng/N sợi/Nc carbon/N ,/, còn/C hoa_văn/N của/E chiếc/N Q10/M màu_trắng/Np thì/C giống/N Z10/Np ,/, tức_là/X có/V nhiều/A chấm/Nc nhỏ/N nhỏ/A ./.

Các/L cạnh_bên/N của/E máy/N ,/, như/C đã/Np nói/V ở/E trên/A ,/, được/V làm/V cong/Nb nhẹ/A ở/E phần/N giáp/V nắp/N lưng/N để/E nằm/V vừa_vặn/A và/C êm_ái/A trong_lòng/V bàn_tay/N của/E chúng_ta/P ./.
Những/L cạnh/N này/P không_còn/C được/V trang_bị/V một/M viền/V sơn/N màu/N kim_loại/N như_trên/X những/L chiếc/Nc BB/Ny trước_đây/N mà/C nó/P tiệp_màu/Np với/E tổng_thể/N của/E máy/N ./.
Điều_đó/N giúp/V tăng/R cảm_giác/N nguyên_khối/Np cho/E Q10/M ./.
Tuy_nhiên/C cá_nhân/N mình/P vẫn/R thích/V cách/Np mà/C BlackBerry/C thiết/V ra/E Bold/N 9900/M hơn/A ,/, nhìn/V nó/P đẹp/A và/C sang_trọng/A hơn/A so/V với/C Q10/M ,/, còn/C Q10/M nghiêng_về/Np kiểu_cách/N hiện_đại/A pha/V chút/N "/" bí_ẩn/A "/" ./.

Ở/E cạnh/N trái/Nc chúng_ta/P có/T cổng/N microUSB/N và/C microHDMI/V ,/, cạnh/N phải/Np là/C nơi/N đặt/V nút/N tăng_giảm/V âm_lượng/N và/C nút/V Voice/Np Command/Np ./.
Cạnh/N trên/A của/E Q10/Ny có/V nút/N mở/V khóa_máy/N ,/, jack/V tai_nghe/N 3,5mm/M và/C micro/N phụ/A ./.
Những/L nút/N này/P làm/V bằng/E kim_loại/N và/C rất/R dễ/A nhấn/V ./.
Trong_khi_đó/N ,/, cạnh/N dưới/A của/E thiết_bị/N được/V bố_trí/V loa/N ngoài/A với/E chiều_ngang/L khá/R lớn/A và/C lớp/N lưới/N được/V đục/A rất/R nhỏ/A ,/, rất/R tinh_tế/A ,/, ngoài_ra/X còn/R có/T micro/N thoại/N nữa/A ./.
Mình/P chỉ/R chưa/R vừa_ý/A với/E cách/N đặt/V cổng/N microUSB/N của/E BlackBerry/Np vì/E khi/N vừa/V sạc/V vừa/R nghe/V điện_thoại/N rất/R vướng/V ,/, từ/E hồi/N BlackBerry/Np Bold/Np 9700/M tới/Nu giờ/N vẫn_thế/X ./.
Hãng/N có_thể/R cân_nhắc/V mang/V nó/P xuống/V cạnh/N dưới/A máy/N giống/N như/C nhiều/A nhà_sản_xuất/N điện_thoại/N khác/A vẫn/R hay_làm/V ./.

2/M ./.
Màn_hình/N

Q10/M được/V trang_bị/V màn_hình/N AMOLED/Ny 3,1/M "/" độ_phân_giải/N 720/M x/N 720/M ,/, tức/C tỉ_lệ/N 1:1/M hay/C hình_vuông/N ./.
Thoạt_nhìn/Np thì/C mình/R thật_sự/A thất_kiểu/A màn_hình/N này/P rất/R lạ/A ,/, và/C quả_thực/R có_thể/R xem/V tỉ_lệ/N 1:1/M này/Np là/V độc_nhất/A vô/E nhị/N trong/E thế_giới/N điện_thoại/N ngày_nay/N ./.
Tuy_nhiên/C sau/N khoảng/N vài/N phút/N làm_quen/V thì/C ngay_lập_tức/R cảm_giác/N cân_đối/Np sẽ/R trở_lại/V ,/, có_lẽ/X là/C nhờ/V thiết_kế/V hài_hòa/N giữa/E màn_hình/N ,/, bàn_phím/N và/C tổng_thể/N mặt/N trước/A của/E máy/N ./.

Màn/V hình/N của/E Q10/M hơi/Np ngả/V sang/E màu/N vàng/Np và/C chúng_ta/P có_thể/R dễ_dàng/A nhận_thấy/V điều/N đó/P khi/N xem/V các/L menu/N có/V nền_trắng/N ./.
Màu_đen/Np thì/C đảm_bảo/V đen/A ,/, đúng_với/E tính_chất/N của/E AMOLED/Ny ./.
Tuy_nhiên/C ,/, màu_sắc/N nhìn_chung/X rực/A và/C đậm/A hơn/A so/V với/E bình_thường/A khá/R nhiều/A ,/, gần/R giống/N với/E hiện_tượng/N có_thể/R thấy/V trên/A Galaxy/N S/Ny II/Ny ./.
Khi/N nhìn/V ở/E khoảng_cách/N gần/A thì/C chữ/N và/C các/L đối_tượng/N đồ_họa/N trên/A màn_hình/N của/E Q10/M hơi/Np rỗ/A một_chút/L so/V với/E độ_phân_giải/N Retina/Np của/E iPhone/Ny hay/C Full/Np -/- HD/Ny của/E những/L máy/N Android/N cao_cấp/A ,/, tuy_nhiên/C đó/P không_phải/R là/V vấn_đề/N gì/N to_tát/A bởi/E chúng_ta/P không_phải/R lúc_nào_cũng/N dí/E sát/M mắt/N vào/E màn_hình/N chỉ/R để/E xem/V điểm/Nc ảnh/N ./.
Góc/N nhìn/V cũng/R rất/R ổn/A ,/, không/R có/V vấn_đề/N gì/T cả/T ./.
Có/Np điều_độ/A sáng/N tối_đa/A của/E máy/N không/R cao/A ./.

Trên/E BlackBerry/N 10/M ,/, chúng_ta/P sẽ/R phải/V dựa/V khá/R nhiều/A vào/E các/L thao_tác/V vuốt/Np từ/E cạnh/N màn_hình/N vào/E trong/E màn_hình/N ,/, nhất_là/X việc/Np vuốt/Nc từ/E dưới/A lên/E để_trở_về/M màn_hình/N chính/A hoặc/C hiện/N giao_diện/N chạy/V đa_nhiệm/N ./.
Vấn_đề/N ở/E Q10/M đó/Np là/C phần/N không_gian/N trống/Np bên_dưới/A màn_hình/N quá/T ít/A nên/V lúc/N vuốt/V nhiều/A khả_năng/N bạn/N sẽ/R bị/V vướng/V vào/E dãy/Nc phím/N QWERTY/Ny trên/A cùng/A ,/, khá/N khó_chịu/A ,/, nhất_là/X với/C những/L bạn/N mới/A lần_đầu/V tiếp_cận/V với/E chiếc/Nc điện_thoại/N này/P ./.
Với/E chiếc/Nc Q5/M mình/Np từng/R được/V thử/V cũng_như/C trên/A Z10/M thì/C phần/N cạnh/N này/N rộng/A hơn/A ,/, thao_tác/V vuốt/R thoải_mái/A hơn/A ./.
Ngoài_ra/C ,/, khi/N cần/V phải/V cuộn/V những/L trang/Nc nội_dung/N dài/A như/C Facebook/Np ,/, website/N ,/, mình/N cũng/R hay/C vô_tình/A kích_hoạt/V giao_diện/N multitask/Np trong/E những/L lần_đầu/N dùng/V Q10/N ,/, trong_khi/N dùng/V Z10/Ny thì/C không/R gặp/V tình_trạng/N tương_tự/A vì/E cạnh/N máy/N nằm/V tuốt/A phía_dưới/A và/C không_ai/A lại/R đi/V cuộn/Np từ_vị_trí/N đó/T cả/T ./.

3/M ./.
Bàn_phím/Np

Có_lẽ/R đây/Np là/C phần/N đáng/V được/V quan_tâm/V nhất/A khi/N đánh_giá/V một/M chiếc/Nc BlackBerry/N ,/, và/C đặc_biệt/A lại/R là/C một/M mẫu/N máy/N mang/V tính/N "/" sống_còn/X "/" như/C là/C Q10/M ./.
So/Np với/E các/L dòng/Nc BlackBerry/A đời/N trước/A ,/, bàn_phím/N của/E Q10/M đã/R duỗi/V thẵng/V ra/V ,/, tất_cả/P các/L phím/N đều/Np nằm/V trên/A một/M đường/N ngang/A chứ/C không_còn/C cong_cong/A như_trước/X ./.
Có/V thể/N bạn/N nhìn/V vào/E sẽ/R nói/V rằng/E bàn_phím/N ngang/A thế/T này/P thì/C làm_thế_nào/X mà/C gõ/R ngon/A được/V nhưng/C thật_sự/A thì/C cảm_giác/N không/R khác/A lúc/N gõ/V những/L chiếc/Nc BlackBerry/N cũ/A là/C mấy/X ./.
Tốc_độ/N gõ/Np ,/, độ_chính_xác/N vẫn/R y_như/A vậy/X ./.
Mình/P đặc_biệt/A thích/V bốn/N dải/N kim_loại/N ngăn_cách/V từng/R hàng/N nút/E trên/A bàn_phím/N QWERTY/Ny ,/, nó/P giúp/V làm/V tổng_thể/N chiếc/N Q10/M trở_nên/V nổi_bật/A ,/, không/R nhàm_chán/A và/C sở_hữu/V cái/Nc chất/N rất/R BlackBerry/A ,/, nhất_là/X khi/N bàn_phím/N trải/V dài_ra/X tận/E hai/M mép/N và/C của/E máy/N ./.

Tương_tự/N như/C những/L chiếc/Nc Bold/N trước_đây/N ,/, các/L phím/N của/E Q10/Ny được/V đặt/V sát/A nhau/N ,/, tuy_nhiên/E phía/N trên/A mỗi/M phím/N lại/Np có/V một/M phần/N gờ/N nhô/V lên/E giúp_việc/V nhập_liệu/Np được/V nhanh_chóng/A ,/, hạn_chế/V lỗi/Nb xảy_ra/X do/C nhấn/V nhầm/V nút/N ./.
Cảm_giác/N nhấn/V quen_thuộc/A và/C độ/N nảy/V vừa_phải/A chắc_chắn/A sẽ/R làm/V hài_lòng/V bất/Np kì/V tín_đồ/N BlackBerry/Np nào/Np ./.
Nếu/C cần/V phải/V thao_tác/V nhiều/A với/E những/L công_việc/N cần/V phải/V gõ/V văn_bản/N ,/, ví_dụ/N như/C trả_lời/V email/A ,/, chat/V ,/, facebook/Np ,/, chỉnh/V sửa/V nội_dung/N văn_bản/N ,/, nhắn_tin/V thì/C bàn_phím/N cứng/A sẽ/R mang/V lại/R cho/E bạn/N cảm_giác/N rất/R khác_biệt/A ,/, một/M cảm_giác/N "/" an_toàn/A "/" và/C "/" chính_xác/A "/" hơn/A so/V với/C việc/L gõ/V trên/A màn/N hình/N cảm_ứng/N ./.
Tất_nhiên/Np là/C chúng_ta/P sẽ/R phải/V dùng/V lực/N nhiều/A hơn/A ,/, nhưng/C dù_sao/E thì/C sự_phản_hồi/V vật_lý/N nảy/V lên/N ,/, nhúng/V xuống/V của/E phím/N bao_giờ/P cũng/R thích/V hơn/A là/C khả_năng/N rung/V xúc_giác/N của/E những/L máy/N full/A -/- touch/X ./.

Tuy_nhiên/C ,/, ngoài/E chuyện/N có/T giảm/V giác/V đã/R và/C thích_thú/V ra/R ,/, liệu/V bàn_phím/N QWERTY/Ny vật_lý/N có_thể/R giúp/V Q10/Np cạnh_tranh/V được/V với/E các/L đối_thủ/N full/Np -/- touch/X ?/?
Nếu/C bạn/N đang/R sử_dụng/V một/M chiếc/Nc điện_thoại/N cảm_ứng/N có/V bàn_phím/N tốt/A ,/, ví_dụ/N như/C bàn_phím/N của/E BlackBerry/N Z10/M ,/, của/E iPhone/Nb ,/, thật_sự/A mình/P nghĩ/V rằng/C bạn/N sẽ/R rất/R hài_lòng/V dù_cho/C có_phải/X gõ/V phím/N ảo/A đi/V chăng_nữa/X ./.
Với/E những/L bạn/N nào/A đã/R quen/V dùng/V máy/N full/V -/- touch/Np thì/C việc/R quay/V trở_lại/V sử_dụng/V bàn_phím/N vật_lý/N có_thể/R là/C một/M cực_hình/N bởi/E bạn/N phải/V nhấn_mạnh/V hơn/A ,/, và/C không_phải/R ai/P cũng/R thích/V điều/N này/P ./.
Hiệu_suất/N của/E việc/Ny gõ/E bàn_phím/N cứng/A thật_sự/A không/R cao/A hơn/A mấy/M so/V với/E bàn_phím/N ảo/A trong/E trường_hợp/N bạn/N đã/R dùng/V quen/N cả_hai/X ./.
Ngoài_ra/C ,/, bàn_phím/N ảo/A còn/C hỗ_trợ/V khả_năng/N chọn/V và/C đoán/V từ/E nhanh/A hơn/A ,/, linh_hoạt/A hơn/A khá/R nhiều/A so/V với/E QWERTY/Ny vật_lý/N trên/A Q10/M ,/, vốn/Np chỉ/R hiện/L thị/N ba/Np từ/E gợi_ý/V tại/E một/M thời_điểm/N ./.

4/M ./.
Camera/Np

BlackBerry/Np trang_bị/V cho/E Q10/M máy_ảnh/N độ_phân_giải/N 8/M megapixel/Nu ,/, tương_tự/A như/C nhiều/A smartphone/N cao_cấp/A hiện_nay/N trên/A thị_trường/N và/C rõ_ràng/A là/C cao/A hơn/A nhiều/A so/V với/E những/L chiếc/Nc BlackBerry/N đời/N trước/A ./.
Và/C may_mắn/A là/C hãng/N đã/R mang/V tính_năng/N auto/Np focus/V lên/E lại/R thiết_bị/N của/E mình/P chứ/C không/R tự_nhiên/A loại_bỏ/V tính_năng/N tự_động/A lấy/V nét/N như/C hồi/N 9900/M ./.
Ảnh/Np cho/E ra/E sắc_nét/T ,/, màu_sắc/N ổn/A trong/E điều_kiện/N đủ/A sáng/N ,/, hoàn_toàn/A phù_hợp/V với/C những/L anh_em/N thích/V chụp/V và/C chia_sẻ/V ảnh/N qua/V email/Nb ,/, up/V lên/E Facebook/Np ./.
Và/C dù/C là/C chụp/V ngoài_trời/X hay/C trong/E nhà/N thì/C khả_năng/N cân/Np bằng/E trắng/A của/E Q10/Ny cũng/R khá/V chuẩn/A ./.

TimeShift/Np ,/, một/M tính_năng/N nổi_bật/A trên/A BlackBerry/N 10/M cho_phép/Nu bạn/N chọn/V khuôn_mặt/N của/E từng/R người/Nc trong/E ảnh/N ở/E khoảnh_khắc/N đẹp/A nhất/A ,/, tương_đối/A hữu_ích/A ,/, chỉ/R hơi/R tiếc_rằng/P chúng_ta/P chưa/R thể/N zoom/Np toàn/P bức_ảnh/N khi/N chỉnh/Np TimeShift/Np ./.
Q10/M được/V tích_hợp/V tính_năng/N chụp_ảnh/N HDR/Ny ,/, tuy_nhiên/C kết_quả/N giữa/E ảnh/N HDR/Ny và/C ảnh/N thường/R chưa/R thật/V khác/A biệt/A như/C kì/N vọng/V của/E mình/P ./.


5/M ./.
Pin/Np

Q10/M được/V trang_bị/V viên/Nc pin/N dung_lượng/N 2100mAh/M ,/, một/M mức/N dung_lượng/N khá/Np cao/A so/V với/C một/M chiếc/Nc máy/N dùng/V màn_hình/N chỉ/R 3,1/M "/" độ_phân_giải/N 720/M x/N 720/M ,/, chip/N hai/M nhân/N Snapdragon/N S4/Np Plus/Np ./.
Thời_lượng/N dùng/V pin/N của/E Q10/M ở/E mức/N tốt/A so/V với/E các/L smartphone/N mạnh_mẽ/A hiện_nay/N ./.
Nó/P không/R "/" trâu_bò/N "/" bằng/E những/L máy/N BB/Ny 9700/M ,/, 9780/M mà/Nu mình/P từng/R đắm/V chìm/V trong/E một/M thời_gian/N dài/A nhưng/C cũng/R đủ/A cho/E gần/A một/M ngày/N sử_dụng/V với/E khoảng/N 2/M tiếng/N lướt/Np web/N bằng/E 3G/M ,/, push/Np mail/V liên_tục/A từ/E ba/M hộp_thư/N Gmail/Np ,/, 30/M phút/N chơi/V nhạc/N bằng/E loa/N ngoài/A với/E âm_lượng/N 70/M %/Nu ,/, lướt/Np facebook/M và/C check/Nu mail/V khoảng/N 1/M tiếng/N ,/, gọi/V 5/M cuộc/Nc gọi/V khoảng/N 3/M phút/N /// cuộc/N ./.
Trong/E suốt/A thời_gian/N đó/P ,/, máy/N luôn/R bật/V Wi/Np -/- Fi/Np ,/, NFC/Ny ,/, độ/Np sáng/V màn/N hình/N để/E tự_động/A ./.

Với/E một_viên/N pin/N ,/, bạn/N hoàn_toàn/A có_thể/R an_tâm/A rời_khỏi/R nhà/N lúc/N 7/M giờ/N sáng/N và/C về_nhà/A lúc/N 7/M giờ/N tối/N mà/C máy/N vẫn/R còn/V khoảng/N 25-30/M %/Nu pin/N nếu/C sử_dụng/V với/E cường_độ/N như_trên/X ./.
BlackBerry/Np có/V bán/L một/M phụ_kiện/N sạc/V pin/N rời/V ,/, cộng/V với/C việc/X Q10/M có_thể/R tháo/V nắp/N lưng/N để/E thay/V pin/N một_cách/N nhanh_chóng/A thì/C bạn/N sẽ/R không_phải/A lo_lắng/V trong/E những/L chuyến_đi/N dài/A ./.

Tuy_nhiên/C ,/, thật_sự/A mà/C nói/V thì/C mình/P vẫn/R mong/V thời_lượng/N pin/N của/E Q10/M dài/A hơn/A ,/, khoảng/N 1/M ngày/N rưỡi/N chẳng_hạn/X ,/, bởi_vì/E với/C cùng/C cường_độ/N xài/V như/C trên/A mà/C chiếc/Nc HTC/Ny One/Np của/E mình/P cũng/R có/V thời_gian/N dùng/V tương_tự/A Q10/M trong_khi/Np One/N dùng/V bộ/N xử/V lí/N mạnh/A hơn/A ,/, màn/N hình/N to/A hơn/A ,/, độ_phân_giải/N cao/A hơn/A ./.

6/M ./.
Hiệu_năng/Np

Q10/M sử_dụng/V chip/N Qualcomm/Np Snapdragon/Np S4/Np Plus/Np hai/M nhân/N xung/M nhịp/N 1,5GHz/M ,/, RAM/Ny 2GB/M ,/, thấp/Np so/V với/E các/L máy/N bốn/N nhân/N ,/, tám/N nhân/Np trong/E thế_giới/N Android/Np ,/, tuy_nhiên/C trải/V nghiệm/R rất/R mượt_mà/A và/C nhanh_chóng/A (/( mình/Np đã/R cập_nhật/V chiếc/N Q10/M chính/N hãng/N của/E mình/P lên/E phiên_bản/N phần_mềm/N mới/A nhất/A )/) ./.
Mình/P có/V ghi_nhận/V được/V vài/L lần/Nu máy/N bị/V chậm/A và/C đứng_lại/V trong_lúc/L vuốt/V để/E chuyển/V sang/E màn_hình/N Multitask/Np hoặc/C khi/N khởi/Np chạy/V app/Nc mới/A ,/, cũng/R may/A là/C chuyện/N này/P không/R xảy_ra/X thường_xuyên/A ./.
Trải/Np nghiệm/Np tổng_quan/N là/C tốt/A ./.
Nếu/C bạn/N đã/R sử_dụng/V thuần_thục/A các/L thao_tác/V gesture/Np cũng_như/C phối_hợp/V nhuần_nhuyễn/A hệ_thống/N phím/N tắt/Np thì/C Q10/M càng/Np trở_nên/V tuyệt_vời/A hơn_nữa/C ./.
Những/L tính_năng/N đặc_trưng/A của/E BB10/M như/C BlackBerry/Np Hub/Np ,/, các/L thao_tác/V cơ_bản/A ,/, Universal/Np Search/Np ,/, BBM/Ny ,/, Yahoo/Np Messenger/Np ,/, bản_đồ/N thì/C chúng_ta/P đã/R có/V một/M bài/Nc riêng/A ,/, mời/V các/L bạn/N xem/V qua/E nếu/C thích/V ./.

Tinhte/Np bb/Np blackberry/N q10/Y bbq10-29/M ./.
​/​

Mình/P nghĩ/V rằng/C nếu/C BlackBerry/Np bỏ_bớt/V ,/, hoặc/C cung_cấp/V tùy/V chọn/V để/E người_dùng/V tắt/V bớt/V các/L hiệu_ứng_chuyển/V cảnh/N trên/A BlackBerry/N 10/M thì/C tốc_độ/N truy/V xuất/V app/A sẽ/R nhanh/A hơn/A ./.
Ví_dụ/C ,/, khi/N bạn/N chạy/V một/M ứng_dụng/Nu nào_đó/V lên/A ,/, hệ_điều_hành/N sẽ/R mở/V một/M cửa_sổ/N của/E app/N đó/Ny trong/E giao_diện/N Multitasking/Np rồi/Np mới/A hiện/A app/R ra/E cho/E chúng_ta/P sử_dụng/V ./.
Bước/Np đầu_tiên/A đó/P có_thể/R lược/V bỏ_đi/Nu để/E tiết_kiệm/V được/V khoảng/N nửa/M giây/N ,/, chỉ/R cần/V hiệu_ứng/N phóng/V to/A app/R ra/E đã/R đẹp/A lắm/T rồi/T ./.
Ngoài_ra/C ,/, lúc/N gửi/V tin_nhắn/Nb Facebook/N cũng/R có/V một/M độ/N trễ/A rõ_rệt/A nhưng/C có_lẽ/X là/C do/E phần_mềm/N chứ/Np không_phải/R vấn_đề/N phần_cứng/N ./.

7/M ./.
Kết_luận/V

Vì/E sao/Np bạn/N muốn/V mua/V Q10/M ?/?
Đó/P là/V câu_hỏi/N bạn/N cần/V phải/V đặt_ra/X đầu_tiên/A khi/N cân_nhắc/V việc/N sắm/V chiếc/Nc điện_thoại/N này/P ./.
Nếu/C câu_trả_lời/Np là/C vì/E bạn/N yêu/V bàn_phím/N QWERTY/Ny vật_lý/N thì/C xin/V chúc_mừng/V ,/, Q10/Np là/V một/M sản_phẩm/N đáng/V mua/V nhất/A ở/E thời_điểm/N hiện_tại/N nếu/C bạn/N muốn/V có_được/X trải/V nghiệm/R gõ/V gõ/A ,/, nhấn/V nhấn/V tuyệt_vời/A trên/A bàn_phím/N vật_lý/N ./.
Còn/C nếu/C bạn/N muốn/V mua/V Q10/Np vì/E hiệu_suất/N làm_việc/V cao/A thì/C chiếc/Nc máy/N này/P có_thể/R sẽ/R không/R đáp_ứng/V được/V như/C kì/N vọng/V của/E bạn/N ./.
Máy/Np có/V màn_hình/N nhỏ/N 3,1/M "/" nên/V việc/Np lướt/V web/N ,/, xem/V các/L tập_tin/N văn_phòng/N ,/, ngay_cả/N khi/N chat/V ,/, nhắn_tin/V ,/, email/Nb và/C chơi/V Facebook/Np thì/C Q10/M không/R thật_sự/A thoải_mái/A so/V với/E hàng/R tá/E những/L smartphone/Nc Android/N ,/, iOS/Np ,/, Windows/Np Phone/Np có/V màn_hình/N lớn/A ,/, và/C tất_nhiên/A là/C cũng/R không/R sướng/A như/C lúc/N xài/V Z10/M ./.
Có_thể/R bạn/N tranh_luận/V rằng/C tôi/P đã/R quen_với/A màn_hình/N nhỏ/N của/E BB/Ny từ/E lâu/A rồi/X ,/, nhưng/C nếu/C là/V một/M người/N quan_tâm/V đến/E năng_suất/N mà/C điện_thoại/N mang/V lại/R thì/C bạn/N sẽ/R cần/V cân_nhắc/V nhiều/A trước_khi/N cầm/V tiền/N đi/V mua/V Q10/M ./.

Hệ_điều_hành/N BlackBerry/Np 10/M hiện/Nu còn/V khá/R mới_mẻ/A ,/, nhiều/A anh_em/N sẽ/R chê/V là/C ít/A app/A quá/T ,/, nhưng/C các/L ứng_dụng/N cơ_bản/A thì/C gần_như/R đầy_đủ/A và/C đáp_ứng/V tốt/A nhu_cầu/N thông_thường/A của/E số_đông/N người_dùng/N ./.
Nếu/C đã/R sử_dụng/V thuần_thục/A ,/, BlackBerry/Np 10/M rất/R tốt/A ,/, các/L thao_tác/V gesture/A không_những/C mang/V lại/R trải/V nghiệm/R mới_lạ/A mà/C cũng/R cực/V kì/X hữu_ích/A ,/, có/V điều/N những/L anh_em/N nào/Np mới/A tiếp_cận/V BB10/Np thì/C phải/V bỏ/V khoảng/N 2-3/M ngày/N làm_quen/V mới/A ngon/A ./.

Điểm/N mạnh/A của/E Q10/M :/:
Bàn_phím/N tuyệt_vời/A
Chất_lượng/N hoàn_thiện/Np ổn/A
Thiết_kế/Np mang/V nét/N cổ_điển/A nhưng/C pha/Np các/L điểm/N nhấn/V hiện_đại/A
BlackBerry/Np 10/M với/E hệ_thống/N thao_tác/V điều_khiển/V tốt/A
Điểm_yếu/Np của/E Q10/M :/:
Màn_hình/N chưa/R xuất_sắc/A ,/, bị/V ngả/V vàng/R
Thao_tác/V vuốt/Np để/E quay/V trở_về/V màn_hình/N Multitasking/Np bị_vướng/Np
Pin/Np chưa/R ngon/A bằng/E những/L chiếc/Nc BlackBerry/Np Bold/Np 97xx/M


sao/T con/Nc này/P ra/E lâu/A rùi/N mà/C giờ/N mới/A có/V đánh_giá/V :/: eek/V :/:

Tinh_tế/N lại/Np PR/Ny trá_hình/V cho/E BB/Ny rồi/Np :/: )/)
Bữa_trước/Np có/V một/M bài/Nc đánh_giá/V rồi/A mà/T ./.

Thích/Np BB/Ny nhưng/C không/R có/V tiền/N để/E sở_hữu/V em/N nó/P ./.

Máy/N này/P rất/R tốt/A ,/, thực_sự/A rất/R yêu_thích/A những/L máy/N như_vậy/X .../... có/T điều/N cái/Nc giá/N của/E nó/P quá/T là/C hoang_tưởng/V .../... !/! ;/; )/)

đt/N nhựa/N dành/V cho/E doanh_nhân/N :/: D/Ny
cái/Nc giá/N cũng/R rất/R là/C doanh/E cmn/P nhân/N :/: D/Ny

Năm/N 2013/M mà/Nu còn/C mấy/L cái/Nc dt/P như_vầy/X dành/V cho/E doanh_nhân/N nữa/Np ./.
Tưởng/V mod/Np lấy/V cái/Nc dt/A năm/N 2005/M đem_lên/Nu chớ/R ./.

Đọc/V xong/V review/N về/E màn_hình/N ,/, ham_muốn/V mua/V Q10/M giảm/V quá/N nửa/N :/: D/Ny

Gặp/V lại/R cậu/V sau_vậy/X

pin/N lại/Np là/C điểm_yếu/A

lạ/A ,/, sao/N tự_nhiên/A lại/R lòi/V ra/E cái/Nc đánh_giá/V này/A :/: 3/M

giá/N mấy/L con/Nc bb/P xưa_nay/N nổi_tiếng/A là/C chát./V đây/N cũng/R là/V rảo/V cản/Nb để/E người_dùng/V tiếp_cận/V bb./L hi_vọng/Np có/T dòng/Nc giá/N phải_chăng/A (/( kiểu/N như/C lumia/N 520/M bên/N wp/Np )/) thì/C nhiều/A người/Nc mới/A tiếp_cận/V được/V

niềm/Nc mơ_ước/Nb của/E e/N nhưng/C đành_chịu/Np vì/E không/R có/V lúa/N

Lúc/N trc/Np qua/V chỗ/N Cellphones/Np thấy/V con/Nc này/P đẹp/A thiệt/A !/! men/A -/- lỳ/T !/!

Như_vậy/C mới/A đúng/A chất/N Blackberry/N chứ/Np ./.

Em/N rất/R tốt/A ,/, nhưng/C anh/Np xin_lỗi/V ./.

cố_lên/N Blackberry/V :/: D/Ny

Màn/V hình/N bị/V rỗ/A rất/R rõ_rệt/A ,/, nhất_là/X khi/N thay_đổi/V góc/N nhìn/V ./.
BlackBerry/Np bỏ_mất/Np các/L phím/N shortcut/A khi/N ở/E màn_hình/N Home/Np ,/, giảm/V hiệu_suất/N rõ_rệt/A ./.
Ko/Np hiểu_sao/X bb/V lại/R bỏ_đi/V một/M thao_tác/V rất/R tiện_lợi/A ở/E bb7/M đó/Np là/C nhập/V pass/Nb ngay_cả/L khi/N màn_hình/N đang/R tắt/V ,/, bb10/M phải/V thêm/V động_tác/N mở_màn/V hình/N mới/A nhập/V pass/R đc/X ./.
Tiễn/N em/N sau/N 2/M ngày/N sử_dụng/V ,/, ưu_điểm/N về/E phím/N cứng/A có_vẻ/X nhạt_nhòa/A khi/N z10/M có/V bàn_phím/N quá/T tiện_dụng/A ./.

Vẫn/R dùng/V 9900/M và/C chưa/R hội/V đủ/A yếu_tố/N để/E lên/E Q10/M

cần/V thêm/V phím/N tắt/Np từ/E màn_hình/N chính/Np ./.
Quá/Np quen_với/Np việc/Nc ấn/Nc M/Ny -/- message/N ,/, O/Ny -/- Options/Np ,/, .../... ./.

phím/N vật_lý/N thì/C quá/T ngon/A rồi/T ,/, cảm_ứng/N vẫn/R chỉ_là/V cảm_ứng/N

Giá/N chát/A quá/T so/V với/C hiệu_năng/N

nhìn/V đẹp/A phết/N

làm_gì/X đã/R có/V hả/T bác.dẫn/A ra/E hộ/T em/N cái/Nc

còn/C này/P không_có/R gì/C nỗi/C bật/V ngoài/A trừ/V cái/Nc giá/N trời_ơi/A của/E nó/P

Giờ/N thì/C đám/Nc trẻ/A trâu/N chỉ/R biết/V màn_hình/N càng/N to/A càng/R ngon/A chứ/R biết/V gì/N về/E BB/Ny ./.
Đây/P không_phải/V đt/N 2005/M mà/C chỉ/R những/L thằng/Nc đã/R ngu/A còn/R cố/V tỏ_ra/X nguy_hiểm/Nc mới/A nghĩ/V thế_thôi/N :/: )/) )/)

Cái/Nc giá/N là/C điểm_yếu/A nhất/A của/E con/Nc này/P :/: p/X

sexy/A quá/T @/@ @/@

Cấu_hình/N cao/Np so/V với/E hiện_nay/N ,/, hình_thức/N quá/T đẹp/A so/V với/E 5/M năm/N trước/A

[/[ quot/X "/" c.bony/A ,/, post/V :/: 38682114/M ,/, member/V :/: 726865/M "/" ]/] lạ/A ,/, sao/N tự_nhiên/A lại/R lòi/V ra/E cái/Nc đánh_giá/V này/A :/: 3/M [/[ /quote/M ]/]
Hôm/N bữa/N là/C Z10/M mà/C :/: D/Ny

Phần/N lớn/A BB/Ny bán/V theo/V hợp_đồng/N nên/V giá/N thực_ra/X không_phải/Np là/C vấn_đề/N lớn/A khi/N so/V với/E các/L smartphone/N khác/A

Bác/Nc nào/Np nói/V pin/N yếu/A ?/?
:/: D/Ny

đã/R "/" chạy/V "/" từ/E 9900/M sang/E note/N 2/M ,/, dưng/Np nếu/C mua/V chiếc/Nc đt/N thứ/N 2/M (/( dùng/V song_song/A )/) thì/C chắc_chắn/A lại/R quay/V về/R 9900/M hoặc/C e/N Q10/M này/Np (/( nếu_có/V điều_kiện/N :/: D/Ny )/)

BB/Ny vẫn/R có/V sự_hấp_dẫn/N rất/R khó/A tả/V ,/, chỉ/R những/L người_dùng/N rồi/C mới/A cảm_nhận/V đuợc/R :/: D/Ny

Mình/P thích/V Z10/M hơn/A ./.
Dùng/Nc màn_hình/N to/A quen/A rồi/A ,/, 3,1/M '/' '/' thấy/V cứ/Nc nhỏ/Nc nhỏ/N

Thực_sự/Np là/C với/E 9900/M thì/C vẫn/R có_thể/R dùng/V pin/N với/E cường_độ/N và/C thời_gian/N như/C trên/A ,/, và/C trên/A Q10/M được/V đánh_giá/V là/C tốt_hơn/A ,/, có_lẽ/X DL/Ny nên/V test/N lại/R phần/N pin/N xem_sao/X ./.

Giá/N chát/A quá../V :/: D/Ny

Bữa_trước/Np là/C bữa/N nào/C vậy/N cụ_non/N :/: D/Ny

Chưa/R gì/Np đã/R "/" phán/V "/" người_khác/N ,/, nặng/A quá/T vậy/T Bạn/N ./.

Thôi/I ,/, yêu/V 9930/M đây/N :/: D/Ny

ngon/A

Chú/Nc vịt_quay/Np trên/A đĩa/N không/R thích/V điều/N này/P

Hài_hòa/Np hài_hòa/Np đừng_nên/C nặng_lời/A vậy/T chứ/C bác/V :/: D/Ny

mình/P ghét/E cái/Nc kiểu/N icon/Np với/E các/L bóng/N mờ/A ô_vuông/N trên/A BB/Ny 10/M ,/, nhìn/V xấu/A quá/T ./.

Chịu/Np cái/Np ông/P đánh_giá/V này/P ,/, màn/N hình/N độ_phân_giải/N 353/M dpi/Nu còn/C cao/A hơn_cả/L Iphone/N 5/M mà/Nu kêu/V rỗ/A không/R nét/N bằng_Ip/Np

Đọc/V xong/V mình/N dí/E sát/M mắt/N mà/C vẫn/R chẳng/R thấy/V gì/N ./.

Về/V thao_tác/V web/N ;/; facebook/Np thì/C trên/A Q10/M thật/Np tuyệt_vời/A bởi/E các/L hệ_thống/N phím/N tắt/Np I/M :/: O/Ny :/: B/N :/: T/Ny .../... .rất/N tiện_lợi/A nhanh_chóng/A ,/, các/L đt/N cảm_ứng/N khác/A mà/C zoom/V ,/, vuốt/Np chạm/V còn/R chậm/A hơn/A vài/N phần/N ./.

Thích/Np thiết_kế/V cổ_điển/A như/C vây/Np của/E BB/Ny ,/, to/A hơn/A chút/N là/C đẹp/A ./.

Mình/P đang/R sài/N Q/Y 10/M nè/T ,/, máy/N rút/V sạc/V từ/E sáng/Nc qua/E đến/E giờ/N vẫn/R còn/V 50/M %/Nu Pin/N ,/, hôm_qua/N ngày/N nghỉ/V gọi/V hơi/R ít/A ,/, sáng/Np có/T FB/M ,/, lướt/V web/N ./.
Hôm_nay/N cũng/R FB/Ny lướt/X web/N ,/, mình/Np để/E 3G/M ,/, nếu/C 2G/M chắc/Nu là/C pin/N sẽ/R trâu/N hơn/A ./.
9900/M ngày_xưa/N để/E 3G/M thì/C ngày/N làm_việc/V đến/V 4h/M chiều/Nu đã/R phải/V cắm/V xạc/V rồi/T ./.

Vừa/R cứng/A vừa/R mềm/A :/: D/Ny
Bác/Np cuhiep/Np chốt/P câu/Nc hay/C quá/T :/: D/Ny

Thích/Np quá/N đam_mê/V thì/C có/V quyết_tâm/V thì/C k./R phải/V có/T cái/Nc máy_tính/N bảng/N ngon/A rồi/V sài/N với/C em/N này/P thì/C được/V ./.

Rom/N 10.2/M hỗ_trợ/V Android/Np runtime/N 4.2.2/M cài/V game/N ,/, appstore/Np vô_tư/A

Không_biết/R phải/V nói/V gì/N ,/, thời_lượng/N Q10/Np cỡ/Np 9900/M ?/?

Thằng/V xác_nhận/V là/C thằng/Nc nào/C vậy/A ,/, chuyên/A dìm/V hàng/N BB/Ny à/X ./.

Có_lẽ/X là/C ếch/N ngồi/V đáy_giếng/A nhưng/C mình/V thấy/V BB/Ny bỏ/V hàng/N phím/N nghe_gọi/M menu/N quả_là/X ý_kiến/N tồi/A ./.
Bàn_phím/Np BB/Ny nổi_tiếng/A vì/E sự_tiện_dụng/N ./.
Việc/Nc cho/E cảm_ứng/N vào/E cái/Nc màn/N 3/M "/" bé_tí/N đã/R là/C nhảm/A (/( BB9900/M )/) ,/, giờ/N lại/Np bỏ_đi/E hàng/N phím/N nghe_gọi/X ./.
9000/M mà/C chạy/V được/V BB10/Ny thì/C tuyệt/R ^/^ ^/^ ./.

Nếu/C BB/Ny cho/E tự/P build/V cấu/V hình/N mình/Np sẽ/R chọn/V layout/Nb của/E 9900/M (/( bỏ/V cảm_ứng/N )/) +/+ trackball/A thay/Nb nóng/A được/V của/E 8700/M +/+ OS/Ny BB10/Ny ./.

Thực_tế/N thì/C dùng/V Q10/Np có/V cái/Nc hay/C hơn_hẳn/T em/N 9900/M là/C tốc_độ/N ./.
Nhận/Np mail/Np chưa_chắc/Nc bằng/E 9900/M nhưng/C hơn_hẳn/Np ở/E cái/T mở/V attack/N đính_kèm/A ,/, chỉnh/V sửa/V ./.
Tốc_độ/N vào/E web/N thì/C kém/A Z10/M +/+ màn_hình/N nhỏ/N nên/V xem/V không/R sướng/A :/: (/( ./.
Giờ/N vẫn/R bực_mình/V vì/E cái/Nc quả/Nc gọi_nhanh/Np từ/E bàn_phím/N và/C quản_lý/V danh_bạ/N tuy/C tích_hợp/V nhiều/A hơn/A OS7/M nhưng/C có_vẻ/X ngu/A hơn/A !/!

Q10/M gọi/V app/E ví_dụ/N SMS/Ny có/T phím/N tắt/Np ko_ạ/R

Con/Nc này/Np có/V một/M số/N ưu_điểm/N nếu/C so/V với/C mấy/L em/N Q/Ny -/- mobile/Np vì/E thương_hiệu/N của/E RIM/Ny trước_đây/N ,/, nếu/C giá/N tầm/N 2/M triệu/M đổ/V lại/R thì/C nên/V mua/V Q10/M này/Np hơn/A là/C mấy/L con_dòng/N E/Ny cua/Np Nokia/A hoặc/C mấy/L đt/N bàn_phím/N qwerty/Np của/E Q/Ny -/- mobile/X .../... Ý_kiến/N cá_nhân/N :/: D/Ny

mấy/L cái/Nc messaging/Nc app/A thì/C sao/R nhỉ/I ?/? whatsapp/A ,/, line/Np với_lại/C viber/L hiện_tại/N có/V chạy/V ổn_định/A không/R vì/E theo/V tôi/P biết/V thì/C chỉ/R có/T mỗi/M whatsapp/N là/C cho/E bb10/M ,/, còn_lại/C toàn/P phải/N dùng/V của/E android/Ny

Mới/R mua/V Q10/M tối/Np qua/V ở/E Vincom/Np ,/, cảm_giác/N bây_giờ/P vẫn/R lâng_lâng/A ./.

Ai_thấy/V phù_hợp/V với/C mình/P thì/C cứ/R mua/V đi/V ,/, Kg/Np thất_vọng/V đâu/T ./.

Với/E mình/Np thì/C Blackberry/R phải/Np là/C bàn_phím/N cơ/Np /// vật_lý/N ,/, mình/Np không/R thích/V Blackberry/Np cảm_ứng/N =/= =/= >/> như_vậy/X mới/A là/C Blackberry/A ./.

Nhân_viên/N kinh_doanh/V thích/V BB/Ny ,/, còn/C các/L ông_chủ/N thật_sự/A lại/R thích/V những/L cái/Nc phone/N càng/R đơn_giản/A càng/R tốt/A

đánh_giá/V sẽ/R chuẩn_xác/A hơn/A khi/N có/T 1/M khoảng/N thời_gian/N kha_khá/A trải/V nghiệm/N đó/P mà/C

trackball/N của/E 8700/M @/@ @/@ !/!
và/C thao_tác/V nghe/V gọi/V trên/A BB10/M cũng_như/Np os/X 7.1/M có/V touch/M screen/N thì/C hầu_như/R chẳng_cần/R nút/N call/M vật_lý/N nữa/A ./.

BB/Ny Hub/Np đó/Np =/= ./. =/= !/! vuốt/V 1c/M từ/E bất_kỳ/A màn/N hình/N nào/V luôn/R ./.

Mời/Np bác/Np tham_khảo/V 9220/M brand/Nu new/A ,/, 9320/M brand/Nu new/A ,/, 8900/M 2nd/Nu

con/Nc smartphone/P bàn_phím/N qwerty/Np tốt/A nhất/R thị_trường/N .../... đợi/V vài/N năm/N giảm_giá/V nâng/V từ/E e6/M lên/E em/N này/P .../... ./.
Kết/Np cái/Nc màn_hình/N với/E camera/N .../... thèm/V quá/T

Đọc/V tới/Np đoạn/Nc độc_nhất/A vô/E độc_nhất/A vô/E nhị_cái/N màn_hình/N mà/C buồn_cười/A ,/, mấy/L anh/P Palm/Np chắc/V bùn/N lắm/T ./.
Chưa/R nói_đến/V mấy/L anh/P LG/Ny VU/Ny sắc/Np nữa/P chứ/T :/: )/) )/)

Có/V dùng/V thử/V mấy/L phút/N thấy/V phần_mềm/N vẫn/R chưa/R hoàn_chỉnh/A ,/, thân_thiện/A với/E người_dùng/T !/! ./. BB/Ny ra/E con/Nc này/P vội_vàng/A quá/T hay_sao/X ấy/T !/!

Thấy/V con/Nc này/P cũng/R không_có/R gì_đăc/X biệt/A so/V với/E các/L dòng/N 9900/M ,/, 9970/M .../... ./. trước_đó/N ,/, có_chăng/X hơn/A một_chút/L về/Nu HDH/Ny và/C nâng_cấp/V một/M chút/N về/V cấu/V hình/N phần_cứng/N ./.

Chính/T vì_thế/C nên/V mới/A trên_bờ/A vực/N phá_sản/V đấy/N ,/, cứ/R bình_dân/A mà/C dùng/V thôi/R
