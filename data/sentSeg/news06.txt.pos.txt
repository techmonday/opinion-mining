Ra_mắt/N sau/N iPhone/N 6/M Plus/Np khoảng/N nửa_năm/A ,/, Galaxy/Np S6/Np sẽ/R được/V trang_bị/V những/L gì/N để/E nổi_bật/A hơn/A đối_thủ/N lớn/A iPhone/N 6/M Plus/Np ./.
Truyền_thông/N Hàn_Quốc/Np gần_đây/Np có/T dẫn/V lời/N một/M nhân_sự/N cấp/N cao/M đang/R làm_việc/V cho/E Samsung/Np xác_nhận/V về/E việc/Nc Samsung/Np Galaxy/Np S6/Np sẽ/R được/V trình_làng/V vào/E ngày/N 2/M tháng/N 3/M tới/Nu ,/, trong/E khuôn_khổ/N sự_kiện/N MWC/Ny 2015/M ./.
Cận/Np kề/V thời_điểm/N S6/M được/V giới_thiệu/V ,/, không/R ít/A thông_tin/N rò/V rỉ/N về/E thiết_bị/N này/Np đã/R xuất_hiện/V mang/V tới/T cho/E cộng_đồng/N quan_tâm/V đến/E sản_phẩm/N của/E Samsung/Np những/L hình_dung/V đầu_tiên/A về/E một/M trong/E những/L smartphone/Nc Android/N đáng/V chú_ý/V nhất/N ./.
Dĩ_nhiên/A ,/, khi/N Galaxy/Np S6/Np ra_mắt/V ,/, người_ta/N sẽ/R nói/V rất/R nhiều/A đến/E cuộc/Nc đối_đầu/V giữa/N sản_phẩm/N này/Np với/E sản_phẩm/N iPhone/Nc mới/A của/E Apple/N ./.
Vậy/P nếu/C những/L tin_đồn/N gần_đây/P là/C xác_thực/A ,/, chúng_ta/P sẽ/R có/V một/M Galaxy/N S6/Np như_thế_nào/X so/V với/E iPhone/T 6/M Plus/Np ?/?
1/M ./.
Màn_hình/N
Hình_dung/Np Galaxy/Np S6/Np dựa_theo/Np tin/N đồn/C đọ/V sức/N cùng/C iPhone/X 6/M PlusSamsung/Np Galaxy/Np Note/Np 4/M đã/R có/V màn_hình/N QHD/Ny nên/V khả_năng/N S6/Np cũng/R có/V độ_phân_giải/N là/C rất/R cao/A ./.
Samsung/Np Galaxy/Np S6/Np được/V cho/E là/C sẽ/R có/V màn_hình/N Super/Np AMOLED/Ny 5,2/M inch/N cùng/C độ_phân_giải/N 2.560/M x/N 1.440/M pixel/Nu (/( cho/E mật_độ/N điểm/Nc ảnh/N 554PPI/M )/) ./.
Trong_khi_đó/Np ,/, iPhone/N 6S/M Plus/Np sử_dụng/V công_nghệ/N màn_hình/N IPS/Ny LCD/Ny ,/, kích_thước/N 5,5/M inch/N và/C độ_phân_giải/N 1.920/M x/N 1.080/M pixel/Nu (/( cho/E mật_độ/N điểm/Nc ảnh/N 401PPI/M )/) ./.
Như_vậy/X ,/, siêu_phẩm/N của/E Samsung/N có_thể/R sẽ/R có/V màn_hình/N nhỏ/A hơn/A đồng_thời/R mang/V đến/E cho/E người_dùng/N mật_độ/N điểm/Nc ảnh/N cao/A hơn/A ./.
Thực_tế/N ,/, độ_phân_giải/N QHD/Ny đã/R được/V kỳ_vọng/V từ/E thời/N Samsung/Np Galaxy/Np S5/Np ,/, dù/C vậy/C năm_ngoái/N Samsung/Np đã/R không/R hiện_thực_hóa/V điều/N này/P ./.
2/M ./.
Vi/N xử_lý/V và/C hệ_điều_hành/N
Hình_dung/Np Galaxy/Np S6/Np dựa_theo/Np tin/N đồn/C đọ/V sức/N cùng/C iPhone/X 6/M Plus/Np -/- 1Thời/M gian/Nu gần_đây/X rộ/A lên/E thông_tin/N cho_rằng/X chip/N Snapdragon/Np 810/M gặp/V vấn_đề/N về/E xử_lý/V lượng/N nhiệt/N sản_sinh/V trong/E quá_trình/N sử_dụng/V ./.
Một_số_báo_cáo/N đăng_tải/V gần_đây/X cho_biết/R có_thể/R Samsung/Np sẽ/R hoàn_toàn/A sử_dụng/V chip/N xử_lý/V Exynos/Np trên/A Samsung/Np Galaxy/Np S6/Np ,/, tuy_nhiên/C nhiều/A khả_năng/N ông_lớn/L công_nghệ/N Hàn_Quốc/Np vẫn/R sẽ/R sử_dụng/V chip/N đến/V từ/E Qualcomm/Np và/C tất_nhiên/A sẽ/R là/V dòng/P chip/N mới/A nhất/A Qualcomm/Np Snapdragon/Np 810/M (/( với/E tốc_độ/N được/V cho/E là/C sự_kết_hợp/N của/E nhân/C Cortex/Np -/- A53/M tốc_độ/N 1,3/M GHz/Np và/C nhân/C Cortex/Np -/- A57/M tốc_độ/N 1,9GHz/M )/) ./.
Hiện_nay/N ,/, Apple/Np iPhone/Nb 6S/M được/V trang_bị/V chip/N lõi/N đôi/M Apple/N A8/Np ,/, tốc_độ/N 1,4GHz/M ,/, tuy_nhiên/C thông_số/N khiêm_tốn/A này/P chưa_bao_giờ/P là/C một/M bất_lợi/Nb của/E Apple/N khi/N nền_tảng/N iOS/A 8/M luôn/R có/V khả_năng/N tận_dụng/V tốt/A nhất/R tài_nguyên/N máy/N ./.
Về/E hệ_điều_hành/N của/E Samsung/Np Galaxy/Np S6/Np ,/, máy/N có/V khả_năng/N cao/Np được/V cài/V sẵn/Ny Android/N 5.0/M khi/N lên/E kệ/Nb ./.
Một/M thông_tin/N vui/A nữa/R liên_quan/V đến/E phần_mềm/N của/E siêu_phẩm/Nb này/P cũng/R phải/V kể/V đến/E việc/E gần_đây/X Samsung/Np được/V cho/E là/C đang/R tiết_chế/V mạnh/A tính_năng/N cũng_như/C giao_diện/N của/E TouchWiz/Np UI/Ny ./.
Hãng/N này/P theo/V đó/Np sẽ/R nỗ_lực/V mang/V đến/E một/M trải/Nb nghiệm/A "/" ngang_tầm/V Nexus/N 6/M "/" cho/E người_dùng/N ./.
3/M ./.
RAM/Ny ,/, bộ_nhớ/N và/C pin/N
Hình_dung/Np Galaxy/Np S6/Np dựa_theo/Np tin/N đồn/C đọ/V sức/N cùng/C iPhone/X 6/M Plus/Np -/- 2Samsung/M đã/R sẵn_sàng/A để/E đưa/V Samsung/Np Galaxy/Np S/Ny trở/V lại/R cuộc_đua/N cấu/V hình/N ?/?
Samsung/Np Galaxy/Np S6/Ny có_thể/R sẽ/R được/V trang_bị/V 3GB/M RAM/Ny và/C 32GB/M bộ_nhớ/N trong/E cùng_viên/N pin/N dung_lượng/N 3.300/M mAh/Nu ./.
Một_số/L thông_tin/N gần_đây/N về/R thiết_kế/V mới/A của/E S6/M cho_rằng/V đây/N sẽ/R là/V dòng/P Galaxy/Np S/Ny đầu_tiên/Np không/R hỗ_trợ/V thẻ/N nhớ/V microSD/X ./.
iPhone/A 6/M Plus/Np có/T 1GB/M RAM/Ny cùng/C ba/X lựa_chọn/V về/E bộ_nhớ/N 16/64/128GB/M không_thể/R mở_rộng/V thêm/V ./.
Ngoài_ra/C ,/, phablet/N đầu_tiên/A của/E Apple/Np có/V pin/N dung_lượng/N 2.915/M mAh/Nu ./.
4/M ./.
Camera/Np
Hình_dung/Np Galaxy/Np S6/Np dựa_theo/Np tin/N đồn/C đọ/V sức/N cùng/C iPhone/X 6/M Plus/Np -/- 3Camera/M sau/N khoảng/N 20MP/M đang/R là/V thông_số/N cao/A nhất/A trong/E phân_khúc/L smartphone/I Android/Np ./.
Các/L thông_tin/N gần_đây/P đều/R cho_rằng/V Galaxy/N S6/Np sẽ/R có/V camera/N 20MP/M cùng/C camera/N trước/A 5MP/M ./.
Đây/P là/V mức/N nâng_cấp/V ấn_tượng/N từ_thông_số/L 16MP/M và/C 2MP/M trên/A S5/M ./.
Về/E phía/N smartphone/Np của/E Apple/Np ,/, mặc_dù/C vẫn/R mang/V lại/R trải/V nghiệm/R chụp_hình/Nc được/V người_dùng/N đánh_giá/V cao/A ,/, iPhone/N 6/M Plus/Np chỉ/Np có/V camera/N 8MP/M ./.
Dù/C vậy/Nb ,/, đây/Np là/C dòng/Nc iPhone/A duy_nhất/A hiện_nay/N có/V khả_năng/N chống/V rung/V quang_học/N ./.
Những/L thay_đổi/V ấn_tượng/N về/E camera/N trên/A iPhone/N nhiều/A khả_năng/N sẽ/R chỉ/V xuất_hiện/V trên/A thế_hệ/N iPhone/M năm/N 2015/M ./.
(/( Tổng_hợp/Np )/)
View/Np less/Np
